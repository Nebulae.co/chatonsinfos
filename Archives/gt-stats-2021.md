# Quinzième réunion du groupe de travail

### jeudi 7 janvier 2021 à 11h

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes :  Cpm, Angie



   * divers précédents :
       * création d'un schéma explicitant les subs
           * TODO Angie
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * TODO Cpm


   * ONTOLOGIE
       * software.modules (services.properties)
           * ajout d'un champs "modules" dans la section Software où on indiquerait les modules du logiciel activé
           * rajouter dans l'ontologie le nouveau champ + docs      FAIT
           * TODO Cpm : coder et valoriser le champ dans StatoolInfos   FAIT
           * TODO Angie  : créer modèles des différents cas
               * nextcloud.properties : le nextcloud de base, avec ses modules actifs OK
               * nextcloud-calendar.properties: le nextcloud bridé (custom?) avec un seul module d'actif (ici les agendas, comme sur framagenda.org) OK


       * propriétés socialnetworks :
           * proposition de les passer à optionnel
           * TODO Cpm propager dans federation.properties (c'est mis en recommandé) FAIT
           * TODO Cpm propager dans organization.properties (c'est mis en recommandé) FAIT
           * TODO Cpm propager dans statoolinfos (c'est plus en erreur) FAIT
           * TODO Cpm mettre à jour le CHANGELOG  FAIT


       * organization.chatrooms.{xmpp / irc / rocketchat / mattermost / matrix …} 
               * TODO Angie : ajouter dans le fichier modèle organization.properties OK
               * TODO Cpm ajouter dans l'outil statoolinfos FAIT
                   * **question : type URL ?**
                   * STRING
                   * TODO mettre à jour organization.properties + changelog + moulinette
                       * Angie, Cpm : FAIT


       * informations de membre du collectif « memberof »
           * organization.memberof.chatons.startdate
           * TODO Angie ajouter dans le fichier modèle organization.properties OK
           * TODO Cpm ajouter dans l'outil statoolinfos FAIT
           * TODO Cpm ajouter dans le CHANGELOG FAIT


       * service.guide.user : WISHED ?
           * oui : pas grave si jaune dans la page de check, c'est tout à fait normal


       * nouveau champ :
           * service.package.type :
            # Nature de l'installation du service, une valeur parmi {DISTRIB, CLONEREPO, ARCHIVE, SOURCES, CONTAINER}, obligatoire.

            # DISTRIB : installation via le gestionnaire d'une distribution (apt, yum, etc.)

            # EDITOR : installation via le paquet d'un éditeur (.deb via source externe)

            # CLONEREPO : clone d'un dépôt de source (git clone…)

            # ARCHIVE : code compilé récupéré dans un tgz ou un zip

            # SOURCES : recompilation du code source

            # CONTAINER : déploiement par containeur (Docker, Snap, Flatpak, etc.)

            # L'installation d'un service via un paquet Snap avec apt sous Ubuntu doit être renseigné CONTAINER

            # L'installation d'une application ArchLinux doit être renseignée DISTRIB

            # L'installation d'une application Yunohost doit être renseignée DISTRIB

service.package.type = 

           * TODO Cpm : solliciter l'avis de Flo
               * Flo :
                   * le caractère obligatoire m'embête un peu
                   * je ne suis pas sûr que cette transparence là, n'est pas une faille de sécurité : si une publication de sécurité annonce une faille dans la version packagées de tel outil, il sera très facile de retrouver les serveurs chatons avec cette faille exposée (je sais que la protection par l'omission d'info n'est pas la meilleure, mais concretement, je pense que les hackeurs exploitent les failles de cette maniere là)
                   * je ne suis pas contre l'ajout de ces champs mais ya un truc qui me travaille un peu, bref l'avoir en non obligatoire m'arrangerais
               * Cpm :
                   * comme tu le rappelles, la sécurité par l'omission n'est pas de la sécurité
                   * ici, on indique uniquement le mode d'installation, pas la version, ni la distribution, ni le dépôt… Tu écris « ajout de **ces** champs » mais en fait y en a qu'un, type et les valeurs sont super abstraites : DISTRIB, CLONEREPO, ARCHIVE, SOURCES, CONTAINER
                   * j'ai reformaté les lignes, DISTRIB et autres sont des valeur du champ type, y a pas de valeur pour DISTRIB et autre. Ça serait trop riche et trop précis ;-)
               * Flo : OK
           * TODO propagation
           * TODO ajout changelog
           * TODO ajout moulinette




   * revue de [https://stats.chatons.org/](https://stats.chatons.org/) 😍
       * intégration d'une page édito : [https://stats.chatons.org/edito.xhtml](https://stats.chatons.org/edito.xhtml)
           * à modifier sur [https://framagit.org/chatons/chatonsinfos/-/tree/master/StatoolInfos/edito](https://framagit.org/chatons/chatonsinfos/-/tree/master/StatoolInfos/edito) (liens relatifs)
           * texte pour les internautes explicitant où on est et comment on trouve des infos
           * TODO Angie FAIT !
           * TODO Cpm déployé en prod  FAIT
           * TODO Angie : **revoir la taille d'affichage de l'image**  :o)
       * ajout à page Statistiques (fédération)
           * demande : quels autres statistiques mettre ? 
           * TODO Angie et Flo : prendre un temps pour y réfléchir
               * ajout d'un camembert par catégories de services (47 entrées à ce jour) ? 
                   * les types de services les plus proposés / les moins proposés
               * ajout d'une courbe de progression du nombre de services ?
               * ajout d'une courbe de progression du nombre de membres de la fédération ?
               * les entrées des chatons dans le collectif (nb de chatons a l'année)
                   * metrics.members.count.2016
                   * TODO Angie sur fiche chatons.properties
           * utiliser les pourcentages plutôt que les chiffres ?
               * ca dépend des metrics
               * les ajustements, la mise en forme pourra etre faite plutot à la fin
           * on s'occupera de la mise en forme une fois qu'on aura tous les types de stats
       * ajout de statistiques au niveau des organisations (dans la ligne lien, bouton vers une page dédiée)
           * affichage chronologique du nombre de services par structure
           * nombre d'utilisateurs de l'organisation


   * revue de fichiers properties de membres  :
       * Framasoft
       * Colibri
       * Chapril


   * revue des merge requests :
       * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/19](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/19) :
           * Update chatons.properties for 3hg  
           * organization.memberof.chatons.status.level = IDLE ???
           * Cpm : FAIT
       * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/20](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/20) :
           * ImmaeEu properties file
           * Cpm : FAIT
       * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/21](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/21)
           * Ajout du fichier chatons.properties pour Ethibox :D
           * Cpm : FAIT
       * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/22](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/22)
           * Update chatons.properties for Vulpecula 
           * Cpm : FAIT


   * revue du forum :
       * [https://forum.chatons.org/t/fiche-service-et-stats-chatons-org/1875](https://forum.chatons.org/t/fiche-service-et-stats-chatons-org/1875)
           * Cpm : répondu FAIT


   * avancer avec le collectif sur la complétion des metrics ?
       * metrics génériques à renommer : http / visitors
       * metrics spécifiques à chaque service à penser
           * TODO Angie : création d'un fichier metrics.properties à placer sous MODELES
           * besoin de repasser dessus pour le nommage avant de propager
           * besoin de coder leur affichage pour stats.chatons.org
           * besoin de paramétrer des moulinettes pour les récupérations automatisées de moulinettes




# Seizième réunion du groupe de travail

### jeudi 14 janvier 2021 à 11h

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes :   Cpm, MrFlo, Angie



   * proposition : ajouter les url pour les dons
       * ajouter les url pour les dons type tip (Liberapay) organization.donation.liberapay / organization.donation.paypal / organization.donation.bitcoin ? aux organisations cf. discussion sur les dons pour entraide sur le forum (cela permettrait de faire des stats sur les systemes utilisés pour les dons, et éventuellement proposer une répartition s'il y a une convergence sur l'outil)
       * Cpm :
           * par défaut ajoutable sans problème, principe des properties
           * aucun système n'est exemplaire, même pas Liberapay qui tourne sur AWS :-/
           * leur faire de la pub ? Affichage de l'échec de la situation ?
       * information de type ?
           * organization.donations.type : un ou plusieurs parmi {BANK, SITE, ???}
           * non pertinent
       * information de réseu à liste fermée ?
           * organization.donate.providers={LIBERAPAY, BANK, ???}
           * non pertinent
       * information de réseau ?
           * organization.~~donate~~.* = URL
           * organization.funding.* = URL
           * * : liberapay, tipee, helloasso, paypal (beurk) etc.
           * own, self, home, url, **custom**, other, nomdelastructure
           * ~~obligatoire, attendu,~~ **optionnel**
           * TODO FLO


Ajouter à la doc un commentaire pour expliquer les champs non fermés * qui permettent d'ajouter de nouvelles valeurs possible

# Liens vers les comptes sociaux de l'organisation

# Si vous utilisez un réseau social différent de ceux-là alors ajouter un nouveau champ.

A décliner sur organization.chatroom et pour organization.socialnetwork

TODO Angie : FAIT



   * divers précédents :
       * création d'un schéma explicitant les subs
           * TODO Angie
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * TODO Cpm


   * ONTOLOGIE
       * service.install.type : type d'installation du service, une valeur parmi {DISTRIBUTION, PROVIDER, PACKAGE, CLONEREPO, ARCHIVE, SOURCES, CONTAINER} obligatoire.
           * extrait du fichier service.properties :
            # Type d'installation du service, une valeur parmi {DISTRIBUTION, PROVIDER, PACKAGE, CLONEREPO, ARCHIVE, SOURCES, CONTAINER}, obligatoire.

            # DISTRIBUTION : installation via le gestionnaire d'une distribution (apt, yum, etc.).

            # PROVIDER : installation via le gestionnaire d'une distribution configuré avec une source externe (ex. /etc/apt/source.list.d/foo.list).

            # PACKAGE : installation manuelle d'un paquet compatible distribution (ex. dpkg -i foo.deb).# CLONEREPO : clone manuel d'un dépôt (git clone…).

            # ARCHIVE : application récupérée dans un tgz ou un zip ou un bzip2…

            # SOURCES : compilation manuelle à partir des sources de l'application.

            # CONTAINER : installation par containeur (Docker, Snap, Flatpak, etc.).# L'installation d'un service via un paquet Snap avec apt sous Ubuntu doit être renseigné CONTAINER.

            # L'installation d'une application ArchLinux doit être renseignée DISTRIBUTION.

            # L'installation d'une application Yunohost doit être renseignée DISTRIBUTION.service.install.type =

           * TODO Cpm : solliciter l'avis de Flo
               * Fait : retour « go »
           * TODO propagation : FAIT
           * TODO ajout changelog : FAIT
           * TODO Cpm  ajout moulinette : FAIT
           * TODO Cpm annonce : FAIT
               * [https://forum.chatons.org/t/version-0-2-de-chatonsinfos-mettez-a-jour-vos-fichiers-properties-o/1902](https://forum.chatons.org/t/version-0-2-de-chatonsinfos-mettez-a-jour-vos-fichiers-properties-o/1902)
       * métriques HTTP :
           * rappel convention métriques :
               * metrics.foo.2020 =
               * metrics.foo.2020.months =
               * metrics.foo.2020.weeks =
               * metrics.foo.2020.days =
           * questions existentielles :
               * bots vs monitoring :
                   * requêtes = { bots } + { monitoring }
                   * requêtes = { bots + monitoring } , un bot de monitoring est un bot
                   * les deux
               * bots vs page
                   * pages = { bots } + { human }
                   * pages = { bots + human } 
                   * les deux
               * ip vs visitors
                   * metrics.http.visitors  vs  metrics.http.ip.visitors  vs  metrics.http.ip.human 
           * metrics.http.hits : nombre de requêtes HTTP(S) reçues
               * metrics.http.hits.ipv4
               * metrics.http.hits.ipv6
               * metrics.http.hits.bots
               * metrics.http.hits.bots.ipv4
               * metrics.http.hits.bots.ipv6
               * metrics.http.hits.bots.monitoring
               * metrics.http.hits.bots.monitoring.ipv4
               * metrics.http.hits.bots.monitoring.ipv6
               * metrics.http.hits.monitoring
               * metrics.http.hits.monitoring.ipv4
               * metrics.http.hits.monitoring.ipv6
               * metrics.http.hits.human
               * metrics.http.hits.human.ipv4
               * metrics.http.hits.human.ipv6
           * metrics.http.pages : nombre de pages servies
               * ipv4, ipv6, ~~bots, monitoring, human~~
           * metrics.http.file : nombre de fichiers servis (un fichier n'est pas une page)
               * ipv4, ipv6, ~~bots, monitoring, human~~
           * metrics.http.bytes : nombre d'octets envoyés
               * ipv4, ipv6, ~~bots, monitoring, human~~
           * metrics.http.errors : nombre d'erreurs dans les log erreurs (?)
               * ipv4 ?, ipv6 ?, bots/monitoring, human
           * metrics.http.errors.php : nombre d'erreurs PHP mentionnées dans les logs erreurs (?)
               * ipv4 ?, ipv6 ?, bots/monitoring, human
           * metrics.http.ip : nombre d'adresses IP différentes
               * ipv4, ipv6, bots/monitoring, human
           * metrics.http.ip.visitors : nombre d'adresses IP
           * metrics.http.status.* :
               * metrics.http.status = metrics.http.hits
               * ipv4, ipv6, bots, monitoring, human
               * metrics.http.status.1xx : nombre de réponses de type information
               * metrics.http.status.2xx : nombre de réponses de type succès
               * metrics.http.status.3xx : nombre de réponses de type redirection
               * metrics.http.status.4xx : nombre de réponses de type erreur du client
               * metrics.http.status.5xx : nombre de réponses de type erreur du serveur


   * revue de [https://stats.chatons.org/](https://stats.chatons.org/) 😍
       * ajout à page Statistiques (fédération)
           * demande : quels autres statistiques mettre ? 
           * TODO Angie et Flo : prendre un temps pour y réfléchir
               * ajout d'un camembert par catégories de services (47 entrées à ce jour) ? 
                   * les types de services les plus proposés / les moins proposés
               * ajout d'une courbe de progression du nombre de services ?
               * ajout d'une courbe de progression du nombre de membres de la fédération ?
               * les entrées des chatons dans le collectif (nb de chatons a l'année)
                   * metrics.members.count.2016
                   * TODO Angie sur fiche chatons.properties
               * ajouter un donuts sur les pays
               * ajouter un donuts sur les services de paiement
               * ajouter un donuts sur les méthodes d'installation
           * utiliser les pourcentages plutôt que les chiffres ?
               * ça dépend des metrics
               * les ajustements, la mise en forme pourra être faite plutôt à la fin
           * on s'occupera de la mise en forme une fois qu'on aura tous les types de stats
       * ajout de statistiques au niveau des organisations (dans la ligne lien, bouton vers une page dédiée)
           * affichage chronologique du nombre de services par structure
           * nombre d'utilisateurs de l'organisation
       * prévoir bouton export CSV et JSON pour les fiches d'organisation


   * revue de fichiers properties de membres  :
       * RAS


   * revue des merge requests : [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * RAS


   * revue du forum :
       * RAS


   * avancer avec le collectif sur la complétion des metrics ?
       * metrics génériques à renommer : http / visitors
       * metrics spécifiques à chaque service à penser
           * TODO Angie : création d'un fichier metrics.properties à placer sous MODELES
           * supprimer les lignes concernant les metrics des fiches service.properties sous MODELES
           * besoin de repasser dessus pour le nommage avant de propager
           * besoin de coder leur affichage pour stats.chatons.org
           * besoin de paramétrer des moulinettes pour les récupérations automatisées de moulinettes




# Dix-septième réunion du groupe de travail

### jeudi 21 janvier 2021 à 11h

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes :  cpm / Angie, mrflos avec du retard



Question soumise à la réunion chatons du 19 janvier : avoir un flag BETA pour les services (ljf)

   * **service.status.level** et **service.status.description** peuvent-ils suffire ?
       * OK -> vert, WARNING -> jaune, ALERT -> orange, ERROR -> rouge,  OVER -> noir, VOID -> bleu
   * intérêt de faire apparaître les services BETA dans le collectif ? faible
       * réponse à valider par le collectif plutôt que le groupe de travail stats.chatons.org
       * TODO Angie : ouvrir le sujet aux autres membres sur le forum


   * divers précédents :
       * création d'un schéma explicitant les subs
           * TODO Angie
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * TODO Cpm


   * ONTOLOGIE
       * organization.funding.* = URL, optionnel
           * * : custom, liberapay, tipee, helloasso, paypal (beurk) etc.
           * TODO FLO : à intégrer sur le fichier modèle FAIT
           * TODO StatoolInfos :  Cpm FAIT


       * doc foo.* :
           * TODO Angie : Ajouter à la doc un commentaire pour expliquer les champs non fermés * qui permettent d'ajouter de nouvelles valeurs possible
           * # Liens vers les comptes sociaux de l'organisation
           * # Si vous utilisez un réseau social différent de ceux-là alors ajouter un nouveau champ.
           * A décliner sur organization.chatroom et pour organization.socialnetwork
           * TODO Angie : FAIT


       * host.country.code :
           * STRING -> COUNTRY\_CODE
            # Code pays de l'hébergeur (type COUNTRY\_CODE sur 2 caractères, obligatoire, ex. FR ou BE ou CH ou DE ou GB).

            # Table ISO 3166-1 alpha-2 : [https://fr.wikipedia.org/wiki/ISO\_3166-1#Table\_de\_codage](https://fr.wikipedia.org/wiki/ISO\_3166-1#Table\_de\_codage)

host.country.code =

           * décision pour : Mrflo, Angie, Cpm
           * TODO Cpm propagation service.properties  : FAIT
           * TODO Cpm propagation StatoolInfos : FAIT
           * TODO Cpm propagation CHANGELOG.md : FAIT


       * host.country.* :
           * recommandé -> obligatoire ?
           * décision  pour : Mrflo, Angie, Cpm
           * TODO Cpm propagation organization.properties : FAIT
           * TODO Cpm propagation StatoolInfos : FAIT
           * TODO Cpm propagation CHANGELOG.md : FAIT


       * organization.country
            # Pays de l'organisation (type STRING, recommandé, ex. France).

            organization.country.name =

            # Code pays de l'organisation (type COUNTRY\_CODE sur 2 caractères, obligatoire, ex.ex. FR ou BE ou CH ou DE ou GB).

            # Table ISO 3166-1 alpha-2 : [https://fr.wikipedia.org/wiki/ISO\_3166-1#Table\_de\_codage](https://fr.wikipedia.org/wiki/ISO\_3166-1#Table\_de\_codage)

organization.country.code =

           * décision pour : Mrflo, Angie, Cpm
           * TODO Cpm propagation organization.properties : FAIT
           * TODO Cpm propagation CHANGELOG.md : FAIT


       * métriques HTTP :
           * rappel convention métriques :
               * metrics.foo.2020 =
               * metrics.foo.2020.months =
               * metrics.foo.2020.weeks =
               * metrics.foo.2020.days =
               * Conventions indiquées dans le fichier CONCEPTS
           * questions existentielles :
               * bots vs monitoring :
                   * ~~requêtes = { bots } + { monitoring }~~
                   * **requêtes = { bots + monitoring } , un bot de monitoring est un bot**
                   * ~~les deux~~
               * bots vs page  <=>  charge serveur  vs  pages utiles
                   * ~~pages = { bots } + { human }  =>  pages utiles~~
                   * **pages = { bots + human }  => charge serveur, 1 page demandée par un bot est une page utile aussi**
                   * ~~les deux~~
                   * donc : 
                       * ~~metrics.http.hits.monitoring~~
                       * ~~metrics.http.hits.bots.monitoring~~
               * human vs nonbot
                   * metrics.http.hits.bots
                   * metrics.http.hits.humans
                   * metrics.http.hits.nonbots
                   * metrics.http.hits.normal
                   * metrics.http.hits.nominal
               * ip vs visitors
                   * metrics.http.visitors  vs  metrics.http.ip.visitors  vs  metrics.http.ip.human 
           * metrics.http.* : voir le fichier créé par Angie dans le dépôt 
           * hit vs file vs page vs bytes
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
           * metrics.http.hits : nombre de requêtes HTTP(S) reçues
               * metrics.http.hits.ipv4
               * metrics.http.hits.ipv6
               * metrics.http.hits.bots
               * ~~metrics.http.hits.bots.ipv4 ?~~
               * ~~metrics.http.hits.bots.ipv6 ?~~
               * metrics.http.hits.browser
               * ~~metrics.http.hits.browser.ipv4 ?~~
               * ~~metrics.http.hits.browser.ipv6 ?~~
           * metrics.http.files : nombre de fichiers servis (fréquemment, une requête ne renvoie rien, ex. page déjà e cache, une erreur, etc.)
               * ipv4 ?, ipv6 ?, ~~bots, human~~
           * metrics.http.pages : nombre de pages servies (html, xhtml, dossier)
               * ipv4 ?, ipv6 ?, ~~bots, human~~
           * metrics.http.bytes : nombre d'octets envoyés
               * ipv4, ipv6, ~~bots, human~~
           * metrics.http.errors : nombre d'erreurs dans les log erreurs (?)
               * ipv4 ?, ipv6 ?, bots, human
           * metrics.http.errors.php : nombre d'erreurs PHP mentionnées dans les logs erreurs (?)
               * ipv4 ?, ipv6 ?, bots, human
           * metrics.http.ip : nombre d'adresses IP différentes
               * ipv4, ipv6, bots ?, human ?
               * metrics.http.ip.visitors : nombre d'adresses IP
           * metrics.http.status.* :
               * metrics.http.status.* : nombre du nombre de code HTTP répondu
               * ~~ipv4, ipv6, ~~bots, human
           * metrics.http.navigators.* : répartition du nombre de hits entre navigateurs
               * fournir une liste de référence
           * metrics.http.os.* : répartition des hits entre systèmes d'exploitation
               * autres nommages possibles :
                   * metrics.http.operatingsystems.*
                   * metrics.http.systems.*
               * fournir une liste de référence
           * metrics.http.machines :
               * metrics.http.machines.phones
               * metrics.http.machines.other
           * metrics.http.country.* : répartition du nombre de hits entre pays, le suffix générique est le code internationale ISO\_3166 sur 2 caractères




   * revue de [https://stats.chatons.org/](https://stats.chatons.org/) 😍
       * ajout à page Statistiques (fédération)
           * demande : quels autres statistiques mettre ? 
           * TODO Angie et Flo : prendre un temps pour y réfléchir
               * ajout d'un camembert par catégories de services (47 entrées à ce jour) ? 
                   * trop de données pour un camembert
                   * un camembert des services les plus proposés (en cumulant les autres en "autres")
                   * un graph avec lïes moins proposés ? pas pertinent car il suffit de trier le tableau
                   * TODO Cpm tenter de faire des graphiques
               * ajout d'une courbe de progression du nombre de services ?
               * ajout d'une courbe de progression du nombre de membres de la fédération ?
                   * Cpm : FAIT / modifier le libellé dans ce graph
                   * TODO Cpm : rajouter graphique nombre de in/out
               * les entrées des chatons dans le collectif (nb de chatons a l'année)
                   * metrics.members.count.2016
                   * TODO Angie sur fiche chatons.properties : FAIT
               * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
                   * par exemple pour les types d'inscription (à un service)
               * ajouter un donuts sur les pays
                   * TODO Cpm x2 (organizations et services) : FAIT
                   * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays
                   * TODO : ajouter un graph de distribution des hébergeurs
               * ajouter un donuts sur les services de paiement
                   * TODO Cpm
               * ajouter un donuts sur les méthodes d'installation
                   * Cpm : FAIT
                   * besoin d'ajouter un nouveau type d'installation (cf : [https://forum.chatons.org/t/version-0-2-de-chatonsinfos-mettez-a-jour-vos-fichiers-properties-o/1902/6](https://forum.chatons.org/t/version-0-2-de-chatonsinfos-mettez-a-jour-vos-fichiers-properties-o/1902/6) )
           * utiliser les pourcentages plutôt que les chiffres ?
               * ça dépend des metrics
               * les ajustements, la mise en forme pourra être faite plutôt à la fin
           * on s'occupera de la mise en forme une fois qu'on aura tous les types de stats
       * ajout de statistiques au niveau des organisations (dans la ligne lien, bouton vers une page dédiée)
           * affichage chronologique du nombre de services par structure
           * nombre d'utilisateurs de l'organisation
       * ajout d'un page d'export :
           * bouton export de l'arbre de la fédération :
               * JSON : Cpm FAIT <3
           * bouton export de la liste des organisations
               * CSV :
               * JSON : Cpm FAIT <3
               * ODS : 
           * bouton export de la liste des services
               * CSV :
               * JSON : Cpm FAIT <3
               * ODS : 
           * logo JSON à rendre plus compréhensible en ajoutant le nom ?
               * cf [https://innocreate.com/quick-introduction-apis/](https://innocreate.com/quick-introduction-apis/)


   * revue de fichiers properties de membres  :
       * ajout des fichiers organization.properties par défaut des chatons de la dernière portée (avec ajout du lien dans chatons.properties) Angie FAIT
       * Katzei : avait une coquillette dans l'URL
       * Framasoft :
           * chatons.properties : prendre le raw à la place du blob, Cpm FAIT (Angie reFAIT)
           * logos : [https://framagit.org/framasoft/chatons/-/tree/main/Logos](https://framagit.org/framasoft/chatons/-/tree/main/Logos)
           * framasoft.properties : pour les liens subs, prendre le raw à la place du blob Angie FAIT


   * revue des merge requests : [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * RAS


   * revue du forum :
       * [https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28](https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28)
           * ajouter un page pour lister les problèmes de crawling
               * TODO Cpm
       * [https://forum.chatons.org/t/version-0-2-de-chatonsinfos-mettez-a-jour-vos-fichiers-properties-o/1902/6](https://forum.chatons.org/t/version-0-2-de-chatonsinfos-mettez-a-jour-vos-fichiers-properties-o/1902/6)
           * service.install.type : type d'installation du service, une valeur parmi {DISTRIBUTION, PROVIDER, PACKAGE, CLONEREPO, ARCHIVE, SOURCES, CONTAINER}
           * cas du gestionaire de paquets alternatif (en plus de celui de la distribution)
           * proposition : TOOLING


   * avancer avec le collectif sur la complétion des metrics ?
       * metrics génériques à renommer : http / visitors
       * metrics spécifiques à chaque service à penser
           * TODO Angie : création d'un fichier metrics.properties à placer sous MODELES
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
           * supprimer les lignes concernant les metrics des fiches service.properties sous MODELES
           * besoin de repasser dessus pour le nommage avant de propager
           * besoin de coder leur affichage pour stats.chatons.org
           * besoin de paramétrer des moulinettes pour les récupérations automatisées de moulinettes




# Dix-huitième réunion du groupe de travail

### jeudi 28 janvier 2021 à 11h

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes :  Cpm, Angie, Florian à la bourre



Question soumise à la réunion chatons du 19 janvier : avoir un flag BETA pour les services (ljf)

   * **service.status.level** et **service.status.description** peuvent-ils suffire ?
       * OK -> vert, WARNING -> jaune, ALERT -> orange, ERROR -> rouge,  OVER -> noir, VOID -> bleu
   * intérêt de faire apparaître les services BETA dans le collectif ? faible
       * réponse à valider par le collectif plutôt que le groupe de travail stats.chatons.org
       * TODO Angie : ouvrir le sujet aux autres membres sur le forum


   * divers précédents :
       * création d'un schéma explicitant les subs
           * TODO Angie
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * TODO Cpm


   * ONTOLOGIE
       * métriques HTTP :
           * rappel convention métriques :
               * metrics.foo.name =
               * metrics.foo.description = 
               * metrics.foo.2020 =
               * metrics.foo.2020.months =
               * metrics.foo.2020.weeks =
               * metrics.foo.2020.days =
               * Conventions indiquées dans le fichier CONCEPTS
               * ip vs visitors
                   * metrics.http.visitors  vs  metrics.http.ip.visitors  vs  metrics.http.ip.human 
           * metrics.http.* : voir le fichier créé par Angie dans le dépôt 
           * hit vs file vs page vs bytes
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
           * nombre de requêtes HTTP(S) reçues
               * metrics.http.hits
               * metrics.http.hits.ipv4
               * metrics.http.hits.ipv6
               * metrics.http.hits.bots
               * ~~metrics.http.hits.browser~~
               * validés
           * nombre de fichiers servis (fréquemment, une requête ne renvoie rien, ex. page déjà en cache, une erreur, etc.)
               * metrics.http.files :
               * ~~ipv4, ipv6, bots, human~~
               * validé
           * nombre de pages servies (html, xhtml, dossier)
               * metrics.http.pages
               * ~~ipv4, ipv6, bots, human~~
               * validé
           * nombre d'octets envoyés :
               * metrics.http.bytes
               * ~~ipv4, ipv6, bots, human~~
               * validé
           * nombre d'erreurs dans les log erreurs (?) :
               * metrics.http.errors
               * ~~ipv4 ?, ipv6 ?, bots, human~~
               * metrics.http.errors.php :  nombre d'erreurs PHP mentionnées dans les logs erreurs (?)
           * nombre d'adresses IP visiteuses :
               * metrics.http.ip
               * metrics.http.ip.ipv4
               * metrics.http.ip.ipv6
               * ~~metrics.http.ip.bots~~
               * ~~metrics.http.ip.browser~~
               * ~~metrics.http.ip.visitors ? : nombre d'adresses IP~~
           * répartition des codes HTTP répondus, le suffix générique est le code à 3 chiffres :
               * metrics.http.status.*
           * répartition du nombre de hits entre navigateurs :
               * metrics.http.navigators.*
               * fournir une liste de référence pour le regroupement
           * répartition des hits entre systèmes d'exploitation
               * metrics.http.os.*
               * ~~metrics.http.operatingsystems.*~~
               * ~~metrics.http.systems.*~~
               * fournir une liste de référence pour le regroupement
           * metrics.http.machines :
               * metrics.http.machines.phones
               * metrics.http.machines.other
           * répartition du nombre de hits entre pays, le suffix générique est le code ISO\_3166 sur 2 caractères
               * metrics.http.country.*




   * revue de [https://stats.chatons.org/](https://stats.chatons.org/) 😍
       * page d'export :
           * bouton export de la liste des organisations
               * CSV : TODO Cpm FAIT
               * ODS : TODO Cpm FAIT
           * bouton export de la liste des services
               * CSV : TODO Cpm FAIT
               * ODS : TODO Cpm FAIT
           * logo JSON à rendre plus compréhensible en ajoutant le nom ?
               * cf [https://innocreate.com/quick-introduction-apis/](https://innocreate.com/quick-introduction-apis/)
               * TODO Cpm FAIT
       * page organization :
           * ajout d'un indicateur statistiques des erreurs du fichier properties de l'organiation
           * bouton cliquable menant vers une page dédiées aux lignes en erreur
           * TODO Cpm : FAIT
       * page organization : 
           * refonte de l'affichage des dates :
               * Date d'entrée : 12/10/2016 (4 ans et 3 mois)
               * Date de création : 01/02/2011 (9 ans et 11 mois)
               * **décision d'afficher par défault les organizations et services « actifs », sans enddate future ;) ; **plus tard éventuellement, ajout d'un fonction pour voir les autres aussi
                   * ne pas se contenter de regarder si le enddate est vide, comparer à la date du jour
               * libellés pour le memberof.startdate/enddate :
                   * Si pas de date de fin future: 
                       * Membre <nom du collectif> depuis <organization.memberof.chatons.startdate> (<durée>)
                   * Si date de fin : 
                       * Membre <nom du collectif> : <organization.memberof.chatons.startdate>-<organization.memberof.chatons.enddate> (<durée>)
               * libellés pour le organization.startdate/enddate :
                   * Création : <organization.startdate>
               * libellés pour le service :
                   * si date de fin :
                       * on supprime les libellés : 
                       * <service.startdate> – <service.enddate> (<durée>)
                   * si pas date de fin :
                       * Depuis <service.startdate> (durée)
               * ordre des libellés de date :
                   * dates orga avant date créa
               * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
                   * TODO Cpm
       * ajout à page Statistiques (fédération)
           * demande : quels autres statistiques mettre ? 
           * TODO Angie et Flo : prendre un temps pour y réfléchir
               * ajout d'un camembert par catégories de services (47 entrées à ce jour) ? 
                   * trop de données pour un camembert
                   * un camembert des services les plus proposés (en cumulant les autres en "autres")
                   * un graph avec lïes moins proposés ? pas pertinent car il suffit de trier le tableau
                   * TODO Cpm tenter de faire des graphiques
               * ajout d'une courbe de progression du nombre de services ?
               * modifier le libellé dans les stats avec graphique bar :
                   * TODO Cpm : FAIT
               * ajout graphique nombre de in/out
                   * TODO Cpm : FAIT
               * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
                   * par exemple pour les types d'inscription (à un service)
               * donuts sur les pays
                   * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays
               * ajouter un graphique de distribution des hébergeurs
                   * TODO Cpm
               * ajouter un donuts sur les services de paiement
                   * TODO Cpm
               * donuts sur les méthodes d'installation
                   * besoin d'ajouter un nouveau type d'installation (cf : [https://forum.chatons.org/t/version-0-2-de-chatonsinfos-mettez-a-jour-vos-fichiers-properties-o/1902/6](https://forum.chatons.org/t/version-0-2-de-chatonsinfos-mettez-a-jour-vos-fichiers-properties-o/1902/6) )
                   * TODO Cpm
           * utiliser les pourcentages plutôt que les chiffres ?
               * ça dépend des metrics
               * les ajustements, la mise en forme pourra être faite plutôt à la fin
           * on s'occupera de la mise en forme une fois qu'on aura tous les types de stats
       * ajout de statistiques au niveau des organisations (dans la ligne lien, bouton vers une page dédiée)
           * affichage chronologique du nombre de services par structure
           * nombre d'utilisateurs de l'organisation


   * revue des catégories :
       * Autres : 
           * DokuWiki -> est-ce un service ? NON. Demande de suppression en cours.
           * Dovecot + postfix
           * ejabberd -> Chat
           * Hedgedoc -> Traitement de texte collaboratif
           * Keycloak : est-ce un service ? NON. Demande de suppression en cours.
           * Movim -> "Movim is a social and chat platform" cf. [https://movim.eu/](https://movim.eu/)
           * Mypads => Traitement de texte collaboratif ?
           * Nextcloud\&OnlyOffice -> Nextcloud + module OnlyOffice =>  
           * Prosody -> serveur XMPP -> Chat
       * Générateur de QR code
               * [https://fr.wikipedia.org/wiki/Code\_QR](https://fr.wikipedia.org/wiki/Code\_QR) => code-barre ?
       * Diffusion en direct de flux audio et vidéo :
           * rajouter Peertube ?
           * rajouter Galene (à vérifier) ?


   * revue de fichiers properties de membres  :
       * Ajout de Open-door


   * revue des merge requests : [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * RAS


   * revue du forum :
       * [https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28](https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28)
           * ajouter un page pour lister les problèmes de crawling
               * TODO Cpm
       * [https://forum.chatons.org/t/version-0-2-de-chatonsinfos-mettez-a-jour-vos-fichiers-properties-o/1902/6](https://forum.chatons.org/t/version-0-2-de-chatonsinfos-mettez-a-jour-vos-fichiers-properties-o/1902/6)
           * service.install.type : type d'installation du service, une valeur parmi {DISTRIBUTION, PROVIDER, PACKAGE, CLONEREPO, ARCHIVE, SOURCES, CONTAINER}
           * cas du gestionaire de paquets alternatif (en plus de celui de la distribution)
           * proposition : TOOLING
               * TODO Cpm propager dans service*.properties : FAIT
               * TODO Cpm propager dans l'outil StatoolInfos : FAIT
               * TODO Cpm répondre dans le forum : FAIT


   * avancer avec le collectif sur la complétion des metrics ?
       * metrics génériques à renommer : http / visitors
       * metrics spécifiques à chaque service à penser
           * FAIT Angie : création d'un fichier metrics.properties à placer sous MODELES
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
           * EN COURS supprimer les lignes concernant les metrics des fiches service.properties sous MODELES
           * AJOUTER #================== entre les différentes catégories de metrics
           * besoin de repasser dessus pour le nommage avant de propager
           * besoin de coder leur affichage pour stats.chatons.org
           * besoin de paramétrer des moulinettes pour les récupérations automatisées de moulinettes




# Dix-neuvième réunion du groupe de travail

### jeudi 04 février 2021 à 11h15

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes :  Cpm / Flo / Angie



Question soumise à la réunion chatons du 19 janvier : avoir un flag BETA pour les services (ljf)

   * **service.status.level** et **service.status.description** peuvent-ils suffire ?
       * OK -> vert, WARNING -> jaune, ALERT -> orange, ERROR -> rouge,  OVER -> noir, VOID -> bleu
   * intérêt de faire apparaître les services BETA dans le collectif ? faible
       * réponse à valider par le collectif plutôt que le groupe de travail stats.chatons.org
       * FAIT Angie : ouvrir le sujet aux autres membres sur le forum
           * [https://forum.chatons.org/t/rendre-publics-les-services-des-chatons-en-version-beta-sur-les-sites-du-collectif/](https://forum.chatons.org/t/rendre-publics-les-services-des-chatons-en-version-beta-sur-les-sites-du-collectif/)


   * divers précédents :
       * création d'un schéma explicitant les subs
           * TODO Angie
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * TODO Cpm


   * revue de [https://stats.chatons.org/](https://stats.chatons.org/) 😍
       * page d'export : RAS
       * page fédération : RAS
       * page organization : 
           * refonte de l'affichage des dates :
               * Date d'entrée : 12/10/2016 (4 ans et 3 mois)
               * Date de création : 01/02/2011 (9 ans et 11 mois)
               * **décision d'afficher par défault les organizations et services « actifs », sans enddate future ;) ; **plus tard éventuellement, ajout d'un fonction pour voir les autres aussi
                   * ne pas se contenter de regarder si le enddate est vide, comparer à la date du jour
               * libellés pour le memberof.startdate/enddate :
                   * Si pas de date de fin future: 
                       * Membre <nom du collectif> depuis <organization.memberof.chatons.startdate> (<durée>)
                   * Si date de fin : 
                       * Membre <nom du collectif> : <organization.memberof.chatons.startdate>-<organization.memberof.chatons.enddate> (<durée>)
               * libellés pour le organization.startdate/enddate :
                   * Création : <organization.startdate>
               * libellés pour le service :
                   * si date de fin :
                       * on supprime les libellés : 
                       * <service.startdate> – <service.enddate> (<durée>)
                   * si pas date de fin :
                       * Depuis <service.startdate> (durée)
               * ordre des libellés de date :
                   * dates orga avant date créa
               * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
                   * TODO Cpm
                   * question Mrflo : n'est-ce pas suffisant déjà là ?
       * ajout à page Statistiques (fédération)
           * demande : quels autres statistiques mettre ? 
           * TODO Angie et Flo : prendre un temps pour y réfléchir
               * ajout d'un camembert par catégories de services (47 entrées à ce jour) ? 
                   * trop de données pour un camembert
                   * un camembert des services les plus proposés (en cumulant les autres en "autres")
                   * un graph avec lïes moins proposés ? pas pertinent car il suffit de trier le tableau
                   * TODO Cpm tenter de faire des graphiques
               * ajout d'une courbe de progression du nombre de services ?
                   * TODO Cpm
               * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
                   * par exemple pour les types d'inscription (à un service)
               * donuts sur les pays
                   * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays
               * ajouter un graphique de distribution des hébergeurs
                   * TODO Cpm
               * ajouter un donuts sur les services de paiement
                   * TODO Cpm
               * donuts sur les méthodes d'installation
                   * besoin d'ajouter un nouveau type d'installation (cf : [https://forum.chatons.org/t/version-0-2-de-chatonsinfos-mettez-a-jour-vos-fichiers-properties-o/1902/6](https://forum.chatons.org/t/version-0-2-de-chatonsinfos-mettez-a-jour-vos-fichiers-properties-o/1902/6) )
                   * TODO Cpm
           * utiliser les pourcentages plutôt que les chiffres ?
               * ça dépend des metrics
               * les ajustements, la mise en forme pourra être faite plutôt à la fin
           * on s'occupera de la mise en forme une fois qu'on aura tous les types de stats
       * ajout de statistiques au niveau des organisations (dans la ligne lien, bouton vers une page dédiée)
           * affichage chronologique du nombre de services par structure
           * nombre d'utilisateurs de l'organisation


   * revue des catégories :
       * [https://stats.chatons.org/category-autres.xhtml](https://stats.chatons.org/category-autres.xhtml)
           * DokuWiki -> est-ce un service ? NON. Demande de suppression en cours.
           * Dovecot + postfix
           * Hedgedoc -> Traitement de texte collaboratif
           * Keycloak : est-ce un service ? NON. Demande de suppression en cours.
           * Mypads => Traitement de texte collaboratif
           * Nextcloud\&OnlyOffice -> Nextcloud + module OnlyOffice =>  
           * Prosody -> serveur XMPP -> Chat
       * Générateur de QR code
               * [https://fr.wikipedia.org/wiki/Code\_QR](https://fr.wikipedia.org/wiki/Code\_QR) => code-barre ?
       * Diffusion en direct de flux audio et vidéo :
           * rajouter Peertube ?
               * TODO Angie
           * rajouter Galene (à vérifier) ?
       * Nouvelle(s) catégorie(s) :
           * Jeux :  Trivabble, Minetest
           * TODO Angie : ajouter au fichier et trouver une icône


   * revue de fichiers properties de membres  :
       * Ajout du fichier du chaton Anancus


   * revue des merge requests : [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * RAS


   * revue du forum :
       * [https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28](https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28)
           * ajouter un page pour lister les problèmes de crawling
               * TODO Cpm


   * avancer avec le collectif sur la complétion des metrics ?
       * metrics spécifiques à chaque service à penser
           * FAIT supprimer les lignes concernant les metrics des fiches service.properties sous MODELES
           * besoin de repasser dessus pour le nommage avant de propager
           * besoin de coder leur affichage pour stats.chatons.org
           * besoin de paramétrer des moulinettes pour les récupérations automatisées de moulinettes


   * ONTOLOGIE
       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
           * nombre de visites
               * voir la définition d'une visite dans [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
               * As long as the same site keeps making requests within a given timeout period, they will all be considered part of the same **Visit**.  If the site makes a request to your server, and the length of time since the last request is greater than the specified timeout period (*default is 30 minutes*), a new **Visit** is started and counted, and the sequence repeats.
               * Visites : nombre de « visites » du site. Une visite est un groupe de requêtes en provenance d'une même adresse IP sur une même période (avec moins de 30 minutes entre chaque requête). Si une personne se connecte plusieurs fois dans un délai inférieur à 30 minutes, on ne comptabilise qu'une seule visite.
               * # bla
               * metrics.http.visits.name = 
               * metrics.http.visits.description = 
               * metrics.http.visits.* = 
               * FAIT
           * répartition des codes HTTP de réponse  :
               * [https://fr.wikipedia.org/wiki/Liste\_des\_codes\_HTTP](https://fr.wikipedia.org/wiki/Liste\_des\_codes\_HTTP)
               * # Répartition des codes HTTP de réponse.
               * metrics.http.status.xxx.name = nombre de réponses pour le code xxx
               * ~~metrics.http.status.xxx.description = le suffix générique est le code à 3 chiffres :~~
               * metrics.http.status.xxx =
               * FAIT
           * répartition des requêtes HTTP entre systèmes d'exploitation
               * metrics.http.os.XXXXX.name =
               * ~~metrics.http.os.XXXXX.description =~~
               * metrics.http.os.XXXXX =
               * ~~metrics.http.operatingsystems.*~~
               * ~~metrics.http.systems.*~~
               * ~~fournir une liste de référence pour le regroupement~~
               * FAIT
           * répartition des requêtes HTTP entre navigateurs :  USER AGENT ?
               * ~~fournir une liste de référence pour le regroupement~~
               *  metrics.http.browsers.XXXXX.name = 
               *  metrics.http.browsers.XXXXX =
               * FAIT
           * metrics.http.devices :
               * metrics.http.devices.phones
               * metrics.http.devices.other
               * FAIT
           * répartition du nombre des requêtes HTTP entre pays, le suffix générique est le code ISO\_3166 sur 2 caractères
               * metrics.http.countries.XX =
               * FAIT
       * métriques spécifiques aux services
           * users = visitors ayant réalisé une action sur le service








# Vingtième réunion du groupe de travail

### jeudi 11 février 2021 à 11h15

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes :  Angie / Flo / cpm



Question soumise à la réunion chatons du 19 janvier : avoir un flag BETA pour les services (ljf)

   * **service.status.level** et **service.status.description** peuvent-ils suffire ?
       * OK -> vert, WARNING -> jaune, ALERT -> orange, ERROR -> rouge,  OVER -> noir, VOID -> bleu
   * intérêt de faire apparaître les services BETA dans le collectif ? faible
       * réponse à valider par le collectif plutôt que le groupe de travail stats.chatons.org
       * FAIT Angie : ouvrir le sujet aux autres membres sur le forum
           * [https://forum.chatons.org/t/rendre-publics-les-services-des-chatons-en-version-beta-sur-les-sites-du-collectif/](https://forum.chatons.org/t/rendre-publics-les-services-des-chatons-en-version-beta-sur-les-sites-du-collectif/)
       * résultat vote : **Le collectif acte donc que les services en version bêta proposés par les chatons ne doivent pas être listés sur les outils du collectif (chatons.org, entraide.chatons.org et stats.chatons.org).**


   * divers précédents :
       * création d'un schéma explicitant les subs
           * TODO Angie
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * TODO Cpm


   * revue de [https://stats.chatons.org/](https://stats.chatons.org/) 😍
       * page d'export : RAS
       * page fédération : RAS
       * page organization : 
           * refonte de l'affichage des dates :
               * Date d'entrée : 12/10/2016 (4 ans et 3 mois)
               * Date de création : 01/02/2011 (9 ans et 11 mois)
               * **décision d'afficher par défault les organizations et services « actifs », sans enddate future ;) ; **plus tard éventuellement, ajout d'un fonction pour voir les autres aussi
                   * ne pas se contenter de regarder si le enddate est vide, comparer à la date du jour
               * libellés pour le memberof.startdate/enddate :
                   * Si pas de date de fin future : Membre <nom du collectif> depuis <organization.memberof.chatons.startdate> (<durée>)
                   * Si date de fin :  Membre <nom du collectif> : <organization.memberof.chatons.startdate>-<organization.memberof.chatons.enddate> (<durée>)
                   * Cpm : FAIT (sans traitement date future)
               * libellés pour le organization.startdate/enddate :
                   * Depuis <organization.startdate>
                   * FAIT
               * libellés pour le service :
                   * si date de fin : on supprime les libellés : <service.startdate> – <service.enddate> (<durée>)
                   * si pas date de fin : Depuis <service.startdate> (durée)
                   * Cpm : FAIT
               * ordre des libellés de date :
                   * date créa avant membre avant date membre
                   * Cpm : à refaire 
               * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
                   * TODO Cpm
                   * question Mrflo : n'est-ce pas suffisant déjà là ?
                   * Mrflo : balises encadrantes 
       * ajout à page Statistiques (fédération)
           * demande : quels autres statistiques mettre ? 
           * TODO Angie et Flo : prendre un temps pour y réfléchir
               * ajout d'un camembert par catégories de services (47 entrées à ce jour) ? 
                   * trop de données pour un camembert
                   * un camembert des services les plus proposés (en cumulant les autres en "autres")
                   * un graph avec lïes moins proposés ? pas pertinent car il suffit de trier le tableau
                   * TODO Cpm tenter de faire des graphiques
               * ajout d'une courbe de progression du nombre de services ?
                   * TODO Cpm
               * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
                   * par exemple pour les types d'inscription (à un service)
               * donuts sur les pays
                   * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays
               * ajouter un graphique de distribution des hébergeurs
                   * TODO Cpm
               * ajouter un donuts sur les services de paiement
                   * TODO Cpm
               * donuts sur les méthodes d'installation
                   * besoin d'ajouter un nouveau type d'installation (cf : [https://forum.chatons.org/t/version-0-2-de-chatonsinfos-mettez-a-jour-vos-fichiers-properties-o/1902/6](https://forum.chatons.org/t/version-0-2-de-chatonsinfos-mettez-a-jour-vos-fichiers-properties-o/1902/6) )
                   * TODO Cpm
           * utiliser les pourcentages plutôt que les chiffres ?
               * ça dépend des metrics
               * les ajustements, la mise en forme pourra être faite plutôt à la fin
           * on s'occupera de la mise en forme une fois qu'on aura tous les types de stats
       * ajout de statistiques au niveau des organisations (dans la ligne lien, bouton vers une page dédiée)
           * affichage chronologique du nombre de services par structure
           * nombre d'utilisateurs de l'organisation


   * revue des catégories :
       * [https://stats.chatons.org/category-autres.xhtml](https://stats.chatons.org/category-autres.xhtml)
           * DokuWiki -> est-ce un service ? NON. Demande de suppression en cours.
           * Keycloak : est-ce un service ? NON. Demande de suppression en cours.
               * FAIT
           * Nextcloud\&OnlyOffice -> Nextcloud + module OnlyOffice
           * Matrix-Synapse
               * cas déjà vu, recommandation de mettre un espace entre les deux mots
           * Element-web
               * pareil
       * Générateur de QR code
               * [https://fr.wikipedia.org/wiki/Code\_QR](https://fr.wikipedia.org/wiki/Code\_QR) => code-barre ?
               * Angie : FAIT
       * Diffusion en direct de flux audio et vidéo :
           * rajouter Peertube ?
               * FAIT Angie
           * rajouter Galene (à vérifier) 
               * FAIT Angie dans la catégorie Visioconférence
       * Nouvelle(s) catégorie(s) :
           * Jeux :  Trivabble, Minetest
           * FAIT Angie : ajouter au fichier et trouver une icône
               * super l'icône
               *  **coquillette** : Sauvegarde de contenus web         Minetest, Trivabble


   * revue de fichiers properties de membres  :
       * RAS


   * revue des merge requests : [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * JabberFR :
           * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/23](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/23)
           * Cpm : FAIT
       * UNDERWORLD :
           * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/24](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/24)
           * Cpm : FAIT


   * revue du forum :
       * [https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28](https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28)
           * ajouter un page pour lister les problèmes de crawling
               * TODO Cpm


   * avancer avec le collectif sur la complétion des metrics ?
       * metrics spécifiques à chaque service à penser
           * besoin de repasser dessus pour le nommage avant de propager
           * besoin de coder leur affichage pour stats.chatons.org
           * besoin de paramétrer des moulinettes pour les récupérations automatisées de moulinettes


   * ONTOLOGIE
       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)


       * métriques génériques aux services :
           * users = visitors ayant réalisé une action sur le service
           * Nombre total -> Nombre
           * prefix vs leaf
               * Toute donnée est associé à à une feuille 
               * un préfixe racine par famille de données (feuilles ou sous-feuilles)
               * un préfixe racine ne peut pas être une feuille
           * ## vs #


       * ŀ# [Metrics spécifiques aux services de transfert de fichiers] .
           * metrics.temporaryfilesharing.files                  Nombre fichiers
           * metrics.temporaryfilesharing.bytes                Espace disque total
           * metrics.temporaryfilesharing.bytes.used      Espace disque utilisé
           * metrics.temporaryfilesharing.bytes.free       Espace disque disponible
           * metrics.temporaryfilesharing.uploads            Nombre de téléversements
           * metrics.temporaryfilesharing.downloads       Nombre de téléchargements


       * # [Metrics spécifiques aux  services de sondage de dates].
           * metrics.surveys.count                                          Nombre de sondages dans la base :
           * metrics.surveys.new/creations/created.*    Nombre de sondages créés (durant la période) :
           * metrics.surveys.expired.*                                  Nombre de sondages expirés (durant la période)
           * metrics.surveys.deleted.*                                  Nombre de sondages supprimés (durant la période) :
           * metrics.surveys.purged.*                                   Nombre de sondages purgés
           * metrics.surveys.emails.*                                    Nombre d'adresses mail utilisatrices différentes
           * metrics.surveys.bytes.*                                      Taille de la base de données


       * # [Metrics spécifiques aux services de formulaires en ligne].
           * metrics.forms.count              
           * metrics.forms.submissions
           * metrics.forms.database.bytes




       * # [Metrics spécifiques aux services de forges logicielles]
           * metrics.forge.projects
           * metrics.forge.groups
           * mertics.forge.groups.publics
           * metrics.forge.accounts
           * metrics.forge.repositories
           * metrics.forge.repositories.publics
           * metrics.forge.database.bytes
           * metrics.forge.spacefile.bytes.used
           * metrics.forge.spacefile.files
           * metrics.forge.spacefile.directories
           * metrics.forge.spacefile.inodes


       * # [Metrics spécifiques aux services de cartographie].
           * metrics.maps.count
           * metrics.maps.markers


       * # [Metrics spécifiques aux réseaux sociaux]
           * metrics.socialnetwork.toots
           * metrics.socialnetwork.accounts
           * metrics.socialnetwork.accounts.active




# Vingt et unième réunion du groupe de travail

### jeudi 18 février 2021 à 11h15

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes :  CPM, mrflos



   * divers précédents :
       * création d'un schéma explicitant les subs
           * TODO Angie
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * TODO Cpm


   * revue de [https://stats.chatons.org/](https://stats.chatons.org/) 😍
       * page « organization » : 
           * **décision d'afficher par défault les organizations et services « actifs », sans enddate future ;) ; **plus tard éventuellement, ajout d'un fonction pour voir les autres aussi
               * ne pas se contenter de regarder si le enddate est vide, comparer à la date du jour
               * TODO Cpm
           * date créa avant membre avant date membre
               * Cpm : FAIT 
               * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
                   * TODO Cpm
                   * question Mrflo : n'est-ce pas suffisant déjà là ?
                   * Mrflo : balises encadrantes 
       * page « Statistiques » (fédération) :
           * demande : quels autres statistiques mettre ? 
           * TODO Angie et Flo : prendre un temps pour y réfléchir
               * ajout d'un camembert par catégories de services (47 entrées à ce jour) ? 
                   * trop de données pour un camembert
                   * un camembert des services les plus proposés (en cumulant les autres en "autres")
                   *  un graph avec lïes moins proposés ? pas pertinent car il suffit de trier le tableau
                   * TODO Cpm tenter de faire des graphiques
               * ajout d'une courbe de progression du nombre de services ?
                   * TODO Cpm
               * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
                   * par exemple pour les types d'inscription (à un service)
               * donuts sur les pays
                   * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays
               * ajouter un graphique de distribution des hébergeurs
                   * TODO Cpm
               * ajouter un donuts sur les services de paiement
                   * TODO Cpm
               * donuts sur les méthodes d'installation
                   * besoin d'ajouter un nouveau type d'installation (cf : [https://forum.chatons.org/t/version-0-2-de-chatonsinfos-mettez-a-jour-vos-fichiers-properties-o/1902/6](https://forum.chatons.org/t/version-0-2-de-chatonsinfos-mettez-a-jour-vos-fichiers-properties-o/1902/6) )
                   * TODO Cpm
           * utiliser les pourcentages plutôt que les chiffres ?
               * ça dépend des metrics
               * les ajustements, la mise en forme pourra être faite plutôt à la fin
           * on s'occupera de la mise en forme une fois qu'on aura tous les types de stats
       * ajout de statistiques au niveau des organisations (dans la ligne lien, bouton vers une page dédiée)
           * affichage chronologique du nombre de services par structure
           * nombre d'utilisateurs de l'organisation


   * revue des catégories :
       * [https://stats.chatons.org/category-autres.xhtml](https://stats.chatons.org/category-autres.xhtml)
       * DokuWiki -> est-ce un service ? NON. Demande de suppression en cours.
       * Nextcloud\&OnlyOffice -> Nextcloud + module OnlyOffice
       * Element-web
           * pareil
       *  **coquillette** : Sauvegarde de contenus web         Minetest, Trivabble
           * TODO Angie  FAIT
       * Vger :
           * « service d’hébergement de capsules Gemini proposé par Automario ! »
           * c'est bien le nom d'un produit de service
           * rajouter Vger dans la catégorie « Hébergement de sites web / blogs »


   * revue de fichiers properties de membres  :
       * RAS
       * Colibris Outils libres n'a plus de cartons rouges! (passage a chatoolsinfo-0.2)


   * revue des merge requests : [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * RAS


   * revue du forum :
       * [https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28](https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28)
           * ajouter un page pour lister les problèmes de crawling
               * TODO Cpm
       * [https://forum.chatons.org/t/service-properties-registration-status/2068](https://forum.chatons.org/t/service-properties-registration-status/2068)
           * service.registration : étude de cas
           * TODO Cpm : FAIT
           * besoin d'avis complémentaires ?
               * a priori la réponse est ok, on se donne une semaine de plus pour affirmer la réponse :D


   * avancer avec le collectif sur la complétion des metrics ?
       * metrics spécifiques à chaque service à penser
           * besoin de repasser dessus pour le nommage avant de propager
           * besoin de coder leur affichage pour stats.chatons.org
           * besoin de paramétrer des moulinettes pour les récupérations automatisées de moulinettes


   * ONTOLOGIE
       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)


       * valeurs par défaut :
           * metrics.*.name
           * metrics.*.description
           * encombrant dans les fichiers properties
           * en fait obligation d'obliger une liste par défaut !
           * soit dans la moulinette, soit dans le code valorisation
           * si dans la moulinette alors pas besoin de les mettre dans les fichiers .properties
               * nécessaire pour surcharge ou métriques inconnues
               * => concept de valeur conventionnelle par défaut 
               * comment indiquer dans le fichier modèle metrics.properties ?
           * Mrflos : si la moulinette les ajoute alors d'autres utilisations des fichiers properties pourront en profiter
           * compromission/perversion de l'universalisme fondamental de la notion de fichier properties
           * a priori décision de laisser comme c'est actuellement :o)


       * les propriétés metrics.*.name et metrics.*.description sont-elles optionnelles dans les fichiers properties ?
           * réponse : non


       * ŀ# [Metrics spécifiques aux services de transfert de fichiers] .
           * ~~metrics.temporaryfilesharing.bytes.total                             Espace disque total~~
               * information peu utile et déductible de used+free


       * # [Metrics spécifiques aux  services de sondage de dates].
           * préfixe : poll vs survey ?
               * pour poll :  Cpm
               * pour survey :  Cpm
           * metrics.surveys.count                                              Nombre de sondages dans la base :
           * metrics.surveys.new/creations/created.*        Nombre de sondages créés (durant la période) :
               * pour new : 
               * pour creations :
               * pour created : mrflos, Cpm
           * metrics.surveys.expired.*                                      Nombre de sondages expirés (durant la période)
           * metrics.surveys.purged.*                                       Nombre de sondages purgés
           * metrics.surveys.deleted.*                                      Nombre de sondages supprimés (durant la période) :
           * metrics.surveys.authors.*                                       Nombre d'auteurs de sondage
               * ~~metrics.surveys.emails.*                                         Nombre d'adresses mail utilisatrices différentes~~
           * metrics.surveys.voters.*                                          Nombre de votants
               * pertinence ?
           * metrics.surveys.votes.*                                          Nombre de votes effectués
               * pertinence ?
           * metrics.surveys.bytes.*                                           Taille de la base de données
               * ~~metrics.surveys.database.bytes.*                        Taille de la base de données~~


       * [Metrics spécifiques aux services de formulaires en ligne].
           * metrics.forms.count              
           * metrics.forms.submissions
           * metrics.forms.authors
           * metrics.forms.created
           * metrics.forms.expired
           * metrics.forms.purged
           * metrics.forms.deleted
           * metrics.forms.~~database.~~bytes


       * [Metrics spécifiques aux services de forges logicielles]
           * metrics.forge.projects
           * metrics.forge.groups
           * metrics.forge.groups.private
           * metrics.forge.groups.publics
           * metrics.forge.accounts
           * metrics.forge.repositories
           * metrics.forge.repositories.private
           * metrics.forge.repositories.publics
           * metrics.forge.database.bytes
           * metrics.forge.files.bytes
           * metrics.forge.files.count
           * ~~metrics.forge.directories.count~~  
           * metrics.forge.issues
           * metrics.forge.commiters
           * metrics.forge.roles.owners
           * metrics.forge.roles.role1
           * metrics.forge.roles.role2
           * metrics.forge.wikis.count
           * metrics.forge.wikis.pages
           * metrics.forge.ci vs metrics.ci.* ?


       * [Metrics spécifiques aux services de cartographie].
           * metrics.maps.count
           * metrics.maps.markers


       *  [Metrics spécifiques aux réseaux sociaux]
           * metrics.socialnetwork.toots
           * metrics.socialnetwork.accounts
           * metrics.socialnetwork.accounts.active
           * metrics.socialnetwork.database.bytes
           * metrics.socialnetwork.files.bytes
           * metrics.socialnetwork.files.count
           * metrics.socialnetwork.moderation.???
           * metrics.socialnetwork.instancesfollowed
           * metrics.socialnetwork.instancesfollowers




# Vingt deuxième réunion du groupe de travail

### jeudi 25 février 2021 à 11h15

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes :  Cpm / Angie



   * divers précédents :
       * création d'un schéma explicitant les subs
           * TODO Angie
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * TODO Cpm


   * revue de [https://stats.chatons.org/](https://stats.chatons.org/) 😍
       * page CHATONS : 
           * **décision d'afficher par défaut les organisations et services « actifs » (sans enddate ou avec enddate future)**
               * ne pas se contenter de regarder si le enddate est vide, comparer à la date du jour
               * plus tard éventuellement, ajout d'un fonction pour voir les autres aussi "le cimetière des chatons" 😆
               * TODO Cpm
       * page générique chaton : 
           * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
               * TODO Cpm
       * page « Statistiques » (fédération) :
           * demande : quels autres statistiques mettre ? 
               * TODO Angie et Flo : prendre un temps pour y réfléchir
           * utiliser les pourcentages plutôt que les chiffres ?
               * ça dépend des metrics
               * les ajustements, la mise en forme pourra être faite plutôt à la fin
               * on s'occupera de la mise en forme une fois qu'on aura tous les types de stats
           * graphiques de données déclarés vs graphiques de données complètes
               * à distinguer ? comment ?
           * catégories de services (47 entrées à ce jour) ? 
               * un camembert des catégories les plus proposés (en cumulant les autres en "autres")
               * TODO Cpm distribution décroissante FAIT
               * TODO Cpm  donuts : FAIT
                   * Titre à renommer : répartition des services par catégorie (de logiciels)
                       * TODO Cpm
                   * mettre Autres en gris
                       * TODO Cpm
           * les logiciels
               * un camembert des logiciels les plus proposés (en cumulant les autres en "autres")
               * TODO Cpm distribution décroissante FAIT
               * TODO Cpm  donuts : FAIT
                   * Titre à renommer : répartition des services par (type de) logiciel
                       * TODO Cpm
                   * mettre Autres en gris
                       * TODO Cpm
           * évolution du nombre de services :
               * TODO Cpm : FAIT
               * choix entre graphique par mois et par années
                   * supprimer le graphique par mois 
                       * pour : Angie, Cpm
                       * contre : 
               * renommer titre : retirer le « déclarés » et ajouter en entête un texte de contextualisation
           * ajouter un texte de contextualisation en début de page :
               * les graphiques ci-dessous sont générés à partir de données déclarées et peuvent être partiels
           * camembert répartition des services avec ou sans dates remplies
               * TODO Cpm : FAIT
               * renommer titre : 
                   * répartition des services avec ou son date
                   * services avec ou sans date
           * ajouter un graphique de distribution des hébergeurs
               * TODO Cpm : FAIT
           * ajouter un donuts sur les services de paiement
               * TODO Cpm
       * page « Statistiques chatons » (organization) :
           * évolution du nombre de services :
               * TODO Cpm : FAIT
               * faire pareil que page statistiques fédération
           * camembert répartition des services avec ou sans dates remplies
               * TODO Cpm : FAIT
               * choix entre graphique par mois et par années
               * faire pareil que page statistiques fédération
           * nombre d'utilisateurs de l'organisation
               * TODO Cpm
           * un jour peut-être :
               * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
                   * par exemple pour les types d'inscription (à un service)
               * donuts sur les pays
                   * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays


   * revue des catégories :
       * [https://stats.chatons.org/category-autres.xhtml](https://stats.chatons.org/category-autres.xhtml)
       * DokuWiki -> est-ce un service ? NON. Demande de suppression en cours.
       * Nextcloud\&OnlyOffice -> Nextcloud + module OnlyOffice
       * Element-web FAIT
       * Vger :
           * « service d’hébergement de capsules Gemini proposé par Automario ! »
           * c'est bien le nom d'un produit de service
           * rajouter Vger dans la catégorie « Hébergement de sites web / blogs » FAIT
       * [https://libretranslate.com/](https://libretranslate.com/)
           * dans quelle catégorie le ranger ? Service de traduction
       * Vosk (transcription automatique)
           * dans quelle catégorie le ranger ? Service de transcription


   * revue de fichiers properties de membres  :
       * Colibris Outils libres : et le service de forge ? ;-)
       * [https://chatons.org/fr/find-by-services?field\_revision\_type\_services\_tid%5B%5D=131](https://chatons.org/fr/find-by-services?field\_revision\_type\_services\_tid%5B%5D=131)


   * revue des merge requests : [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * RAS


   * revue du forum :
       * [https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28](https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28)
           * ajouter un page pour lister les problèmes de crawling
               * TODO Cpm
       * [https://forum.chatons.org/t/service-properties-registration-status/2068](https://forum.chatons.org/t/service-properties-registration-status/2068)
           * service.registration : étude de cas
           * TODO Cpm : FAIT
           * besoin d'avis complémentaires ?
               * a priori la réponse est ok, on se donne une semaine de plus pour affirmer la réponse :D
               * TODO Angie rajouter  une interrogation sur le fait que ce n'est pas un service


   * avancer avec le collectif sur la complétion des metrics ?
       * metrics spécifiques à chaque service à penser
           * besoin de repasser dessus pour le nommage avant de propager
           * besoin de coder leur affichage pour stats.chatons.org
           * besoin de paramétrer des moulinettes pour les récupérations automatisées de moulinettes


   * ONTOLOGIE
       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
           * ordre des questions à se poser : préfixe, sous-préfixe


       * les propriétés metrics.*.name et metrics.*.description sont-elles optionnelles dans les fichiers properties ?
           * réponse : non


       * # [Metrics spécifiques aux  services de sondage de dates].
           * préfixe : polls vs surveys ?
               * pour polls :  Cpm
               * pour surveys :  Cpm
               * utilisation du préfixe *date* dans le fichier categories.properties (survey est utilisé pour les outils de sondage non spécifiques aux sondages de dates)
           * metrics.surveys.count                                              Nombre de sondages dans la base :
           * metrics.surveys.new/creations/created.*        Nombre de sondages créés (durant la période) :
               * pour new : 
               * pour creations :
               * pour created : mrflos, Cpm
           * metrics.surveys.expired.*                                      Nombre de sondages expirés (durant la période)
           * metrics.surveys.purged.*                                       Nombre de sondages purgés
           * metrics.surveys.deleted.*                                      Nombre de sondages supprimés (durant la période) :
           * metrics.surveys.authors.*                                       Nombre d'auteurs de sondage
               * ~~metrics.surveys.emails.*                                         Nombre d'adresses mail utilisatrices différentes~~
           * metrics.surveys.voters.*                                          Nombre de votants
               * pertinence ?
           * metrics.surveys.votes.*                                          Nombre de votes effectués
               * pertinence ?
           * metrics.surveys.bytes.*                                           Taille de la base de données
               * ~~metrics.surveys.database.bytes.*                        Taille de la base de données~~


       * [Metrics spécifiques aux services de formulaires en ligne].
           * metrics.forms.count              
           * metrics.forms.submissions
           * metrics.forms.authors
           * metrics.forms.created
           * metrics.forms.expired
           * metrics.forms.purged
           * metrics.forms.deleted
           * metrics.forms.~~database.~~bytes


       * [Metrics spécifiques aux services de forges logicielles]
           * utilisation du préfixe git dans le fichier categories.properties
           * metrics.forge.projects
           * metrics.forge.groups
           * metrics.forge.groups.private
           * metrics.forge.groups.publics
           * metrics.forge.accounts
           * metrics.forge.repositories
           * metrics.forge.repositories.private
           * metrics.forge.repositories.publics
           * metrics.forge.database.bytes
           * metrics.forge.files.bytes
           * metrics.forge.files.count
           * ~~metrics.forge.directories.count~~  
           * metrics.forge.issues
           * metrics.forge.commiters
           * metrics.forge.roles.owners
           * metrics.forge.roles.role1
           * metrics.forge.roles.role2
           * metrics.forge.wikis.count
           * metrics.forge.wikis.pages
           * metrics.forge.ci vs metrics.ci.* ?


       * [Metrics spécifiques aux services de cartographie].
           * metrics.maps.count
           * metrics.maps.markers


       * [Metrics spécifiques aux services de stockage/partage d'images]
           * metrics.pics.count
           * metrics.pics.unlimited
           * metrics.pics.annual
           * metrics.pics.monthly
           * metrics.pics.weekly
           * metrics.pics.daily


       *  [Metrics spécifiques aux réseaux sociaux]
           * metrics.socialnetwork.toots
           * metrics.socialnetwork.accounts
           * metrics.socialnetwork.accounts.active
           * metrics.socialnetwork.database.bytes
           * metrics.socialnetwork.files.bytes
           * metrics.socialnetwork.files.count
           * metrics.socialnetwork.moderation.???
           * metrics.socialnetwork.instancesfollowed
           * metrics.socialnetwork.instancesfollowers










# Vingt troisième réunion du groupe de travail

### jeudi 04 mars 2021 à 11h15

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes :  Cpm, Angie, MrFlo



   * divers précédents :
       * création d'un schéma explicitant les subs
           * TODO Angie
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * TODO Cpm


   * revue de [https://stats.chatons.org/](https://stats.chatons.org/) 😍
       * page CHATONS : 
           * **décision d'afficher par défaut les organisations et services « actifs » (sans enddate ou avec enddate future)**
               * ne pas se contenter de regarder si le enddate est vide, comparer à la date du jour
               * plus tard éventuellement, ajout d'un fonction pour voir les autres aussi "le cimetière des chatons" 😆
               * TODO Cpm
       * page générique chaton : 
           * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
               * TODO Cpm
       * page « Statistiques » (fédération) :
           * demande : quels autres statistiques mettre ? 
               * TODO Angie et Flo : prendre un temps pour y réfléchir
           * utiliser les pourcentages plutôt que les chiffres ?
               * ça dépend des metrics
               * les ajustements, la mise en forme pourra être faite plutôt à la fin
               * on s'occupera de la mise en forme une fois qu'on aura tous les types de stats
           * graphiques de données déclarés vs graphiques de données complètes
               * à distinguer ? comment ?
           * catégories de services (47 entrées à ce jour) ? 
               * un camembert des catégories les plus proposés (en cumulant les autres en "autres")
               * TODO Cpm  donuts : FAIT
                   * Titre à renommer : répartition des services par catégorie ~~(de logiciels)~~
                       * TODO Cpm : FAIT
                   * mettre Autres en gris
                       * TODO Cpm : nan, c'est bon comme ça
           * les logiciels
               * un camembert des logiciels les plus proposés (en cumulant les autres en "autres")
               * TODO Cpm distribution décroissante FAIT
               * TODO Cpm  donuts : FAIT
                   * Titre à renommer : répartition des services par (type de) logiciel
                       * TODO Cpm : FAIT
                   * mettre Autres en gris
                       * TODO Cpm nan, c'est bon comme ça
                   * 2 fois autres : remplacer Autres en Autres catégories
                   * TODO Cpm : TYPO "Services avec ou son date" >  "Services avec ou SANS date"
           * évolution du nombre de services :
               * TODO Cpm : FAIT
               * choix entre graphique par mois et par années
                   * supprimer le graphique par mois 
                       * pour : Angie, Cpm
                       * contre : 
               * renommer titre : retirer le « déclarés » et ajouter en entête un texte de contextualisation
           * ajouter un texte de contextualisation en début de page :
               * les graphiques ci-dessous sont générés à partir de données déclarées et peuvent être partiels
               * TODO Cpm : FAIT
           * camembert répartition des services avec ou sans dates remplies
               * TODO Cpm : FAIT
               * renommer titre : 
                   * ~~répartition des services avec ou son date~~
                   * services avec ou sans date
                       * TODO Cpm : FAIT
           * ajouter un donuts sur les services de paiement
               * TODO Cpm
       * page « Statistiques chatons » (organization) :
           * évolution du nombre de services :
               * TODO Cpm : FAIT
               * faire pareil que page statistiques fédération
               * TODO Cpm : FAIT
           * camembert répartition des services avec ou sans dates remplies
               * TODO Cpm : FAIT
               * choix entre graphique par mois et par années
               * faire pareil que page statistiques fédération
               * TODO Cpm : FAIT
           * nombre d'utilisateurs de l'organisation
               * TODO Cpm
           * un jour peut-être :
               * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
                   * par exemple pour les types d'inscription (à un service)
               * donuts sur les pays
                   * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays


   * revue des catégories :
       * [https://stats.chatons.org/category-autres.xhtml](https://stats.chatons.org/category-autres.xhtml)
       * DokuWiki -> est-ce un service ? NON. Demande de suppression en cours.
       * ISPConfig -> ok pour la catégorie Hébergement site web
           * « ISPConfig fournit différentes interfaces de gestion pour les fournisseurs de services Internet et les clients. » [https://fr.wikipedia.org/wiki/ISPConfig](https://fr.wikipedia.org/wiki/ISPConfig)
       * Nextcloud\&OnlyOffice -> Nextcloud + module OnlyOffice
           * en attente de ljf
       * Nextcloud Calendar :
           * Angie FAIT
       * Nextcloud Passman :
           * Angie FAIT
       * uMap :
           *  Angie : FAITŀ
       * [https://libretranslate.com/](https://libretranslate.com/)
           * dans quelle catégorie le ranger ? Service de traduction
       * Vosk (transcription automatique)
           * dans quelle catégorie le ranger ? Service de transcription


   * revue de fichiers properties de membres  :
       * Colibris Outils libres : et le service de forge ? ;-)
           * [https://chatons.org/fr/find-by-services?field\_revision\_type\_services\_tid%5B%5D=131](https://chatons.org/fr/find-by-services?field\_revision\_type\_services\_tid%5B%5D=131)
               * TODO FLO : officialiser le service et mettre un fichier properties
       * Le Cloud Girofle
           * URL dans la fiche par défaut :     [https://retzo.net/](https://retzo.net/)
           *  Angie FAIT


   * revue des merge requests : [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * Infini :
           * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/29](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/29)
           * Cpm : FAIT
       * Retzo :
           * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/28](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/28)
           * Cpm : FAIT
       * proposition d'un modèle pour Nextcloud-passman :
           * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/27](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/27)
           * TODO validation collective
           * MrFlo : merge + message : FAIT
       * proposition d'améliorations pour service-nextcloud-calendar.properties 
           * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/26](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/26)
           * TODO validation collective
           * MrFlo : merge + message : FAIT
       * Zici mise à jour URL :
           * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/25](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/25)
           * Cpm : FAIT


   * revue du forum :
       * [https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28](https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28)
           * ajouter un page pour lister les problèmes de crawling
               * TODO Cpm
       * [https://forum.chatons.org/t/service-properties-registration-status/2068](https://forum.chatons.org/t/service-properties-registration-status/2068)
           * service.registration : étude de cas
           * TODO Cpm : FAIT
           * besoin d'avis complémentaires ?
               * a priori la réponse est ok, on se donne une semaine de plus pour affirmer la réponse :D
               * TODO Angie rajouter  une interrogation sur le fait que ce n'est pas un service  : FAIT


   * avancer avec le collectif sur la complétion des metrics ?
       * metrics spécifiques à chaque service à penser
           * besoin de repasser dessus pour le nommage avant de propager
           * besoin de coder leur affichage pour stats.chatons.org
           * besoin de paramétrer des moulinettes pour les récupérations automatisées de moulinettes


   * ONTOLOGIE
       * service.properties :
           * service.startdate # Date d'ouverture du service (type DATE, recommandé).
           * Pourquoi recommandé ? Proposition de passer en « obligatoire »
           * Pour : Cpm, Angie, Flo
           * Contre : 
           * TODO Cpm fiches modèles
           * TODO Cpm outil check
       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
           * ordre des questions à se poser : préfixe, sous-préfixe


       * les propriétés metrics.*.name et metrics.*.description sont-elles optionnelles dans les fichiers properties ?
           * réponse : non


       * # [Metrics spécifiques aux  services de sondage, dont les sondages de dates].
           * préfixe : polls vs surveys ?
               * pour polls :  Cpm
               * pour surveys :  Cpm, Angie, mrflos
               * utilisation du préfixe *date* dans le fichier categories.properties (survey est utilisé pour les outils de sondage non spécifiques aux sondages de dates)
           * metrics.surveys.count                                              Nombre de sondages dans la base :
           * metrics.surveys.new/creations/created.*        Nombre de sondages créés (durant la période) :
               * pour new : 
               * pour creations :
               * pour created : mrflos, Cpm
           * metrics.surveys.voters.*                                          Nombre de votants
               * pertinence ?
           * metrics.surveys.votes.*                                          Nombre de votes effectués
               * pertinence ?
           * metrics.surveys.bytes.* 
               * question des cas où on a besoin à la fois de la taille de la bdd + de la taille des fichiers
               * créer :
               * metrics.surveys.database.bytes.*              *# Taille de la base de données*
               * metrics.surveys.files.bytes.*                        *#Taille de l'espace disque occupé par les fichiers*




       * [Metrics spécifiques aux services de formulaires en ligne].
           * metrics.forms.count              
           * metrics.forms.submissions
           * metrics.forms.authors
           * metrics.forms.created
           * metrics.forms.expired
           * metrics.forms.purged
           * metrics.forms.deleted
           * metrics.forms.~~database.~~bytes


       * [Metrics spécifiques aux services de forges logicielles]
           * utilisation du préfixe git dans le fichier categories.properties
           * metrics.forge.projects
           * metrics.forge.groups
           * metrics.forge.groups.private
           * metrics.forge.groups.public
           * metrics.forge.accounts
           * metrics.forge.repositories
           * metrics.forge.repositories.private
           * metrics.forge.repositories.public
           * metrics.forge.commits
           * metrics.forge.commiters
           * metrics.forge.mergerequests
           * metrics.forge.mergerequesters
           * ~~metrics.forge.forks~~
           * ~~metrics.forge.pushs~~
           * (metrics.forge.roles.owners)
           * (metrics.forge.roles.role1)
           * (metrics.forge.roles.role2)
           * metrics.forge.database.bytes
           * metrics.forge.files.bytes
           * metrics.forge.files.count
           * ~~metrics.forge.directories.count~~  
Que faire des métriques liés aux fonctionnalités secondaires ?

   * ne pas les mettre
   * les mettre en tant que sous-préfixe
   * les mettre en tant que préfixe dédié +++


           * metrics.forge.issues   vs metrics.issues.xxx
           * metrics.forge.wikis.count
           * metrics.forge.wikis.pages
           * metrics.forge.ci vs metrics.ci.* ?
           * metrics.ci.xxx
           * comment gerer les modules externes Gitlab pages, gestionnaires de tickets, CI...


metrics.issues.count

metrics.issues.opened

metrics.issues.closed



metrics.wikis.pages



metrics.ci

metrics.hostedstatic.websites

metrics.hostedstatic.pages TODO Flo : comment appeler ca?





       * [Metrics spécifiques aux services de cartographie].
           * metrics.maps.count
           * metrics.maps.markers


       * [Metrics spécifiques aux services de stockage/partage d'images]
           * metrics.pics.count
           * metrics.pics.unlimited
           * metrics.pics.annual
           * metrics.pics.monthly
           * metrics.pics.weekly
           * metrics.pics.daily


       *  [Metrics spécifiques aux réseaux sociaux]
           * metrics.socialnetwork.toots
           * metrics.socialnetwork.accounts
           * metrics.socialnetwork.accounts.active
           * metrics.socialnetwork.database.bytes
           * metrics.socialnetwork.files.bytes
           * metrics.socialnetwork.files.count
           * metrics.socialnetwork.moderation.???
           * metrics.socialnetwork.instancesfollowed
           * metrics.socialnetwork.instancesfollowers




# Vingt quatrième réunion du groupe de travail

### jeudi 11 mars 2021 à 11h15

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes :  Angie / Cpm / MrFlos



   * divers précédents :
       * création d'un schéma explicitant les subs
           * TODO Angie
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * TODO Cpm


   * revue de [https://stats.chatons.org/](https://stats.chatons.org/) 😍
       * page CHATONS : 
           * **décision d'afficher par défaut les organisations et services « actifs » (sans enddate ou avec enddate future)**
               * ne pas se contenter de regarder si le enddate est vide, comparer à la date du jour
               * plus tard éventuellement, ajout d'un fonction pour voir les autres aussi "le cimetière des chatons" 😆
               * TODO Cpm
       * page générique chaton : 
           * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
               * TODO Cpm
       * page « Statistiques » (fédération) :
           * demande : quels autres statistiques mettre ? 
               * TODO Angie et Flo : prendre un temps pour y réfléchir
           * utiliser les pourcentages plutôt que les chiffres ?
               * ça dépend des metrics
               * on s'occupera de la mise en forme une fois qu'on aura tous les types de stats
           * graphiques de données déclarés vs graphiques de données complètes
               * à distinguer ? comment ?
           * les logiciels
               * un camembert des logiciels les plus proposés (en cumulant les autres en "autres")
                   * 2 fois autres : remplacer Autres en Autres catégories
                       * TODO Cpm : FAIT
                   * TYPO "Services avec ou son date" >  "Services avec ou SANS date"
                       * TODO Cpm : FAIT
           * ajouter un donuts sur les services de paiement
               * TODO Cpm
       * page « Statistiques chatons » (organization) :
           * nombre d'utilisateurs de l'organisation
               * TODO Cpm
           * un jour peut-être :
               * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
                   * par exemple pour les types d'inscription (à un service)
               * donuts sur les pays
                   * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays


   * revue des catégories :
       * [https://stats.chatons.org/category-autres.xhtml](https://stats.chatons.org/category-autres.xhtml)
       * DokuWiki -> est-ce un service ? NON. Demande de suppression en cours.
       * Nextcloud\&OnlyOffice -> Nextcloud + module OnlyOffice
           * en attente de ljf
       * [https://libretranslate.com/](https://libretranslate.com/)
           * dans quelle catégorie le ranger ? Service de traduction
           * à voir quand le premier cas se présentera
       * Vosk (transcription automatique)
           * dans quelle catégorie le ranger ? Service de transcription
           * à voir quand le premier cas se présentera


   * revue de fichiers properties de membres  :
       * RAS


   * revue des merge requests : [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * RAS


   * revue du forum :
       * [https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28](https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28)
           * ajouter un page pour lister les problèmes de crawling
               * TODO Cpm
       * [https://forum.chatons.org/t/service-properties-registration-status/2068](https://forum.chatons.org/t/service-properties-registration-status/2068)
           * continuité du sujet
           * Angie : précisions ajoutées
           * faut-il distinguer les inscriptions restreintes en nombre ?
               * au niveau du chaton ?
               * service par service ?
           * nouvelle valeur service.registration vs nouvelle propriété service.status.full
               * la notion de full serait applicable aux autres valeurs de service.registration (none, free, member, client)
               * donc c'est forcément une autre propriété
           * proposition de nouvelle propriété :
               * pourcentage vs drapeau
                   * a priori drapeau est suffisant, plus simple et répond au besoin
               * termes disponibles : full ~~overflow~~ ~~overcrowded fullness closed~~ ~~saturation~~ available
               * filling saturation
               * service.status.saturation : ~~open~~ available|full
               * service.status.load :
               * service.registration.load  :
                   * valeurs possibles : available|full (obligatoire)
                   * modetruc : obligatoire
                   * commentaire : 
                       * # Capacité à accueillir de nouveaux utilisateurs (un parmi {open|full}, obligatoire).
                   * TODO Cpm : propager dans service.properties et fichiers exemples demo
                   * TODO Cpm ajouter dans l'outil statoolinfos
                   * TODO page service signalisation par picto cadenas
                   * TODO vue liste services signalisation par picto cadenas
                   * TODO Cpm prévenir flo pour com quand c'est propagé
                   * TODO Flo informer sur le forum une fois le commit fait


   * avancer avec le collectif sur la complétion des metrics ?
       * metrics spécifiques à chaque service à penser
           * besoin de repasser dessus pour le nommage avant de propager
           * besoin de coder leur affichage pour stats.chatons.org
           * besoin de paramétrer des moulinettes pour les récupérations automatisées de moulinettes


   * ONTOLOGIE
       * service.properties :
           * service.startdate # Date d'ouverture du service (type DATE, recommandé).
           * Pourquoi recommandé ? Proposition de passer en « obligatoire »
               * Pour : Cpm, Angie, Flo
               * Contre : 
           * TODO Cpm fiches modèles
           * TODO Cpm outil check
       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
           * ordre des questions à se poser : préfixe, sous-préfixe


       * [Metrics spécifiques aux services de forges logicielles]
Que faire des métriques liés aux fonctionnalités secondaires ?

   * ne pas les mettre
   * les mettre en tant que sous-préfixe
   * les mettre en tant que préfixe dédié +++
           * metrics.forge.issues   vs metrics.issues.xxx
           * metrics.forge.wikis.count
           * metrics.forge.wikis.pages
           * metrics.forge.ci vs metrics.ci.* ?
           * metrics.ci.xxx
           * comment gerer les modules externes Gitlab pages, gestionnaires de tickets, CI...
       * pour : Cpm
       * contre :
       * pas d'avis : Angie, MrFlo (mais consent)
       * décision : on essaie  les préfixes dédiés


       * metrics.issues :
           * metrics.issues.count
           * metrics.issues.created
           * ~~metrics.issues.status.open~~
           * ~~metrics.issues.status.close~~
           * metrics.issues.closed
           * metrics.issues.database.bytes
           * metrics.issues.files.bytes


       * metrics.wikis
           * metrics.wikis.pages
           * metrics.wikis.account


       * metrics.ci
           * à réfléchir


       * metrics.hostedstatic.websites
       * metrics.hostedstatic.pages TODO Flo : comment appeler ca ?
           * ~~ HostedPage~~
           * ~~websitehost~~
           * ~~websitehosting~~
           * webhosting : +++


<-- on en est là :-)

       * [Metrics spécifiques aux services de cartographie].
           * metrics.maps.count
           * metrics.maps.markers


       * [Metrics spécifiques aux services de stockage/partage d'images]
           * metrics.pics.count
           * metrics.pics.unlimited
           * metrics.pics.annual
           * metrics.pics.monthly
           * metrics.pics.weekly
           * metrics.pics.daily


       *  [Metrics spécifiques aux réseaux sociaux]
           * metrics.socialnetwork.toots
           * metrics.socialnetwork.accounts
           * metrics.socialnetwork.accounts.active
           * metrics.socialnetwork.database.bytes
           * metrics.socialnetwork.files.bytes
           * metrics.socialnetwork.files.count
           * metrics.socialnetwork.moderation.???
           * metrics.socialnetwork.instancesfollowed
           * metrics.socialnetwork.instancesfollowers




# Vingt cinquième réunion du groupe de travail

### jeudi 18 mars 2021 à 11h15

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes :  Angie, Flo, Cpm



   * divers précédents :
       * création d'un schéma explicitant les subs
           * TODO Angie
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * TODO Cpm


   * revue de [https://stats.chatons.org/](https://stats.chatons.org/) 😍
       * page CHATONS : 
           * **décision d'afficher par défaut les organisations et services « actifs » (sans enddate ou avec enddate future)**
               * ne pas se contenter de regarder si le enddate est vide, comparer à la date du jour
               * plus tard éventuellement, ajout d'un fonction pour voir les autres aussi "le cimetière des chatons" 😆
               * TODO Cpm
       * page générique chaton : 
           * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
               * TODO Cpm
       * page « Statistiques » (fédération) :
           * ajouter un donuts sur les services de paiement
               * TODO Cpm
       * page « Statistiques chatons » (organization) :
           * nombre d'utilisateurs de l'organisation
               * TODO Cpm
           * un jour peut-être :
               * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
                   * par exemple pour les types d'inscription (à un service)
               * donuts sur les pays
                   * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays


       * Cas d'usage : **quand un service repose sur plusieurs logiciels**
           * ex : [https://stats.chatons.org/opendoor.xhtml](https://stats.chatons.org/opendoor.xhtml) (un service MAIL qui fonctionne avec dovcot + postfix)
           * ce n'est pas une histoire de module... donc lui demander de revoir sa fiche en changeant son logiciel par Rainloop et de supprimer la fiche du service "SMTP"


   * revue des catégories : RAS


   * revue de fichiers properties de membres  :
       * Ajout de [https://stats.chatons.org/siicksservices.xhtml](https://stats.chatons.org/siicksservices.xhtml)


   * revue des merge requests : [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * RAS


   * revue du forum :
       * [https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28](https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28)
           * ajouter un page pour lister les problèmes de crawling
               * TODO Cpm
       * [https://forum.chatons.org/t/service-properties-registration-status/2068](https://forum.chatons.org/t/service-properties-registration-status/2068)
           * continuité du sujet
           * Angie : précisions ajoutées
           * faut-il distinguer les inscriptions restreintes en nombre ?
               * au niveau du chaton ?
               * service par service ?
           * nouvelle valeur service.registration vs nouvelle propriété service.status.full
               * la notion de full serait applicable aux autres valeurs de service.registration (none, free, member, client)
               * donc c'est forcément une autre propriété
           * proposition de nouvelle propriété :
               * pourcentage vs drapeau
                   * a priori drapeau est suffisant, plus simple et répond au besoin
               * termes disponibles : full ~~overflow~~ ~~overcrowded fullness closed~~ ~~saturation~~ available
               * filling saturation
               * service.status.saturation : ~~open~~ available|full
               * service.status.load :
               * service.registration.load  :
                   * valeurs possibles : available|full (obligatoire)
                   * modetruc : obligatoire
                   * commentaire : 
                       * # Capacité à accueillir de nouveaux utilisateurs (un parmi {open|full}, obligatoire).
                   * TODO Cpm : propager dans service.properties et fichiers exemples demo
                   * TODO Cpm ajouter dans l'outil statoolinfos
                   * TODO page service signalisation par picto cadenas
                   * TODO vue liste services signalisation par picto cadenas
                   * TODO Cpm prévenir flo pour com quand c'est propagé
                   * TODO Flo informer sur le forum une fois le commit fait


   * avancer avec le collectif sur la complétion des metrics ?
       * metrics spécifiques à chaque service à penser
           * besoin de repasser dessus pour le nommage avant de propager
           * besoin de coder leur affichage pour stats.chatons.org
           * besoin de paramétrer des moulinettes pour les récupérations automatisées de moulinettes


   * ONTOLOGIE
       * service.properties :
           * service.startdate # Date d'ouverture du service (type DATE, recommandé).
           * Pourquoi recommandé ? Proposition de passer en « obligatoire »
               * Pour : Cpm, Angie, Flo
               * Contre : 
           * TODO Cpm fiches modèles
           * TODO Cpm outil check
       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
           * ordre des questions à se poser : préfixe, sous-préfixe


       * Un jour peut-être : 
           * metrics.ci


       * [Metrics spécifiques aux services de cartographie]
           * metrics.maps.count
           * metrics.maps.markers
           * metrics.maps.accounts
           * metrics.maps.database.bytes
           * metrics.maps.files.bytes


       * [Metrics spécifiques aux services de stockage/partage d'images]
           * metrics.pics.count
           * metrics.pics.unlimited
           * metrics.pics.annual
           * metrics.pics.monthly
           * metrics.pics.weekly
           * metrics.pics.daily
           * metrics.pics.created
           * metrics.pics.expired
           * metrics.pics.purged
           * metrics.pics.deleted
           * metrics.pics.database.bytes
           * metrics.pics.files.bytes


       *  [Metrics spécifiques aux réseaux sociaux]
           * metrics.socialnetwork.posts
           * metrics.socialnetwork.~~repost~~sharedposts
           * metrics.socialnetwork.likedposts
           * metrics.socialnetwork.privatemessages
           * metrics.socialnetwork.accounts
           * metrics.socialnetwork.accounts.active
           * metrics.socialnetwork.database.bytes
           * metrics.socialnetwork.files.bytes
           * metrics.socialnetwork.sharedfiles
           * metrics.socialnetwork.moderation.abuses
           * metrics.socialnetwork.moderation.bans
           * metrics.socialnetwork.moderation.blocks|mutes ?
           * metrics.socialnetwork.instancesfollowed
           * metrics.socialnetwork.instancesfollowers
TODO Angie : aller voir les vocabulaires des outils de modération sur Mastodon



       * [Metrics spécifiques aux messageries instantanées]
           * metrics.im.teams
           * metrics.im.chans
           * metrics.im.accounts
           * metrics.im.database.bytes
           * metrics.im.files.bytes


       * [Metrics spécifiques aux services de diffusion de fichiers vidéo]
           * metrics.videos.count
           * metrics.videos.views
           * metrics.videos.comments
           * metrics.videos.accounts
           * metrics.videos.channels
           * metrics.videos.database.bytes
           * metrics.videos.files.bytes
           * metrics.videos.federated.count
           * metrics.videos.federated.comments
           * metrics.videos.instancesfollowed
           * metrics.videos.instancesfollowers


       * [Metrics spécifiques aux raccourcisseurs de liens]
           * metrics.shortenlinks.count
           * metrics.shortenlinks.database.bytes
           * metrics.shortenlinks.files.bytes


       * [Metrics spécifiques aux services de transfert de messages chiffrés]
           * metrics.bins.created
           * metrics.bins.consulted
           * metrics.bins.database.bytes
           * metrics.bins.files.bytes


       * [Metrics spécifiques aux tableaux blancs collaboratifs]
           * metrics.boards.count
           * metrics.boards.notes
           * metrics.boards.database.bytes
           * metrics.boards.files.bytes




# 26ème réunion du groupe de travail

### jeudi 1er avril 2021 à 11h15

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes :  Christian (Cpm) , Florian, Angie, Antoine



   * divers précédents :
       * création d'un schéma explicitant les subs
           * TODO Angie
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * TODO Cpm


   * revue de [https://stats.chatons.org/](https://stats.chatons.org/) 😍
       * page CHATONS : 
           * **décision d'afficher par défaut les organisations et services « actifs » (sans enddate ou avec enddate future)**
               * ne pas se contenter de regarder si le enddate est vide, comparer à la date du jour
               * plus tard éventuellement, ajout d'un fonction pour voir les autres aussi "le cimetière des chatons" 😆
               * TODO Cpm
       * page générique chaton : 
           * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
               * TODO Cpm
       * page « Statistiques » (fédération) :
           * ajouter un donuts sur les services de paiement
               * TODO Cpm
       * page « Statistiques chatons » (organization) :
           * nombre d'utilisateurs de l'organisation
               * TODO Cpm
           * un jour peut-être :
               * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
                   * par exemple pour les types d'inscription (à un service)
               * donuts sur les pays
                   * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays


       * Cas d'usage : **quand un service repose sur plusieurs logiciels**
           * ex : [https://stats.chatons.org/opendoor.xhtml](https://stats.chatons.org/opendoor.xhtml) (un service MAIL qui fonctionne avec dovcot + postfix)
           * ce n'est pas une histoire de module... donc lui demander de revoir sa fiche en changeant son logiciel par Rainloop et de supprimer la fiche du service "SMTP"
               * FAIT Angie : OpenDoor a modifié


   * revue des catégories : RAS


   * revue de fichiers properties de membres  :
       * RAS


   * revue des merge requests : [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * RAS


   * revue du forum :
       * [https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28](https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28)
           * ajouter un page pour lister les problèmes de crawling
               * TODO Cpm
       * [https://forum.chatons.org/t/service-properties-registration-status/2068](https://forum.chatons.org/t/service-properties-registration-status/2068)
           * continuité du sujet
           * Angie : précisions ajoutées
           * faut-il distinguer les inscriptions restreintes en nombre ?
               * au niveau du chaton ?
               * service par service ?
           * nouvelle valeur service.registration vs nouvelle propriété service.status.full
               * la notion de full serait applicable aux autres valeurs de service.registration (none, free, member, client)
               * donc c'est forcément une autre propriété
           * proposition de nouvelle propriété :
               * pourcentage vs drapeau
                   * a priori drapeau est suffisant, plus simple et répond au besoin
               * service.registration.load  :
                   * valeurs possibles : available|full (obligatoire)
                   * modetruc : obligatoire
                   * commentaire : 
                       * # Capacité à accueillir de nouveaux utilisateurs (un parmi {open|full}, obligatoire).
                   * TODO Cpm : propager dans service.properties et fichiers exemples demo
                   * TODO Cpm ajouter dans l'outil statoolinfos
                   * TODO page service signalisation par picto cadenas
                   * TODO vue liste services signalisation par picto cadenas
                   * TODO Cpm prévenir flo pour com quand c'est propagé
                   * TODO Flo informer sur le forum une fois le commit fait
       * message interrogation directement matomo
           * TODO Cpm comp


   * avancer avec le collectif sur la complétion des metrics ?
       * metrics spécifiques à chaque service à penser
           * besoin de repasser dessus pour le nommage avant de propager
           * besoin de coder leur affichage pour stats.chatons.org
           * besoin de paramétrer des moulinettes pour les récupérations automatisées de moulinettes


   * ONTOLOGIE
       * service.properties :
           * service.startdate # Date d'ouverture du service (type DATE, recommandé).
           * Pourquoi recommandé ? Proposition de passer en « obligatoire »
               * Pour : Cpm, Angie, Flo
               * Contre : 
           * TODO Cpm fiches modèles
           * TODO Cpm outil check
       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
           * ordre des questions à se poser : préfixe, sous-préfixe


       * Un jour peut-être : 
           * metrics.ci


       *  [Metrics spécifiques aux réseaux sociaux]
           * Reste à définir si on met : 
               * metrics.socialnetworks.moderation.reports
               * metrics.socialnetworks.moderation.disabledaccounts
               * metrics.socialnetworks.moderation.silencedaccounts
               * metrics.socialnetworks.moderation.cancelledaccounts
source : [https://docs.joinmastodon.org/admin/moderation/](https://docs.joinmastodon.org/admin/moderation/)



       * [Metrics spécifiques aux messageries instantanées]
           * metrics.instantmessaging.teams
           * metrics.instantmessaging.rooms # nb de rooms / channels / salons
           * metrics.instantmessaging.messages
           * metrics.instantmessaging.accounts
           * metrics.instantmessaging.database.bytes
           * metrics.instantmessaging.files.bytes


       * [Metrics spécifiques aux services de diffusion de fichiers vidéo]
           * metrics.videos.count
           * metrics.videos.count.lives
           * metrics.videos.views
           * metrics.videos.comments
           * metrics.videos.accounts
           * metrics.videos.accounts.monthlyactive
           * metrics.videos.channels
           * metrics.videos.database.bytes
           * metrics.videos.files.bytes
           * metrics.videos.federated.count
           * metrics.videos.federated.comments
           * metrics.videos.instances.followed
           * metrics.videos.instances.followers
           * metrics.videos.moderation.reports
           * metrics.videos.moderation.disabledaccounts
           * metrics.videos.moderation.silencedaccounts
           * metrics.videos.moderation.cancelledaccounts


       * [Metrics spécifiques aux raccourcisseurs de liens]
           * urlshorteners vs shortenurls
           * metrics.urlshorteners.count
           * metrics.urlshorteners.clicks # Nombre total de clics
           * metrics.urlshorteners.accounts
           * metrics.urlshorteners.accounts.monthlyactive
           * metrics.urlshorteners.database.bytes
           * metrics.urlshorteners.files.bytes


       * [Metrics spécifiques aux services de transfert de messages chiffrés]
           * pastebin  vs sharedclipboard => pastebin
           * metrics.pastebin.count
           * metrics.pastebin.created
           * metrics.pastebin.deleted
           * metrics.pastebin.purged
           * ~~metrics.pastebin.consulted~~
           * metrics.pastebin.database.bytes
           * metrics.pastebin.files.bytes


       * [Metrics spécifiques aux tableaux blancs collaboratifs]
           * metrics.pinboards.count
           * metrics.pinboards.notes
           * metrics.pinboards.database.bytes
           * metrics.pinboards.files.bytes


       * [Metrics spécifiques aux listes de diffusion].
           * metrics.mailinglists.count = Nombre total de listes hébergées
           * metrics.mailinglists.subscribers = Nombre total d'abonné·es
           * metrics.mailinglists.mailsend = Nombre total de mails envoyés
           * metrics.mailinglists.database.bytes
           * metrics.mailinglists.files.bytes




# 27ème réunion du groupe de travail

### jeudi 8 avril 2021 à 11h15

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes :  Christian (Cpm) Angie, Antoine



   * divers précédents :
       * création d'un schéma explicitant les subs
           * TODO Angie
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * TODO Cpm


   * revue de [https://stats.chatons.org/](https://stats.chatons.org/) 😍
       * page CHATONS : 
           * **décision d'afficher par défaut les organisations et services « actifs » (sans enddate ou avec enddate future)**
               * ne pas se contenter de regarder si le enddate est vide, comparer à la date du jour
               * plus tard éventuellement, ajout d'un fonction pour voir les autres aussi "le cimetière des chatons" 😆
               * TODO Cpm
       * page générique chaton : 
           * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
               * TODO Cpm
       * page « Statistiques » (fédération) :
           * ajouter un donuts sur les services de paiement
               * TODO Cpm
       * page « Statistiques chatons » (organization) :
           * nombre d'utilisateurs de l'organisation
               * TODO Cpm
           * un jour peut-être :
               * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
                   * par exemple pour les types d'inscription (à un service)
               * donuts sur les pays
                   * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays




   * revue des catégories : 
       * RAS


   * revue de fichiers properties de membres  :
       * RAS


   * revue des merge requests : [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * RAS


   * revue du forum :
       * [https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28](https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28)
           * ajouter un page pour lister les problèmes de crawling
               * TODO Cpm
       * [https://forum.chatons.org/t/service-properties-registration-status/2068](https://forum.chatons.org/t/service-properties-registration-status/2068)
           * continuité du sujet
           * Angie : précisions ajoutées
           * faut-il distinguer les inscriptions restreintes en nombre ?
               * au niveau du chaton ?
               * service par service ?
           * nouvelle valeur service.registration vs nouvelle propriété service.status.full
               * la notion de full serait applicable aux autres valeurs de service.registration (none, free, member, client)
               * donc c'est forcément une autre propriété
           * proposition de nouvelle propriété :
               * pourcentage vs drapeau
                   * a priori drapeau est suffisant, plus simple et répond au besoin
               * service.registration.load  :
                   * valeurs possibles : available|full (obligatoire)
                   * modetruc : obligatoire
                   * commentaire : 
                       * # Capacité à accueillir de nouveaux utilisateurs (un parmi {open|full}, obligatoire).
                   * TODO Cpm : propager dans service.properties et fichiers exemples demo
                   * TODO Cpm ajouter dans l'outil statoolinfos
                   * TODO page service signalisation par picto cadenas
                   * TODO vue liste services signalisation par picto cadenas
                   * TODO Cpm prévenir flo pour com quand c'est propagé
                   * TODO Flo informer sur le forum une fois le commit fait
       * message interrogation directement matomo
           * [https://forum.chatons.org/t/metrics-automatiques-pour-les-outils-connus/2213](https://forum.chatons.org/t/metrics-automatiques-pour-les-outils-connus/2213)
           * TODO Cpm répondre FAIT


   * avancer avec le collectif sur la complétion des metrics ?
       * metrics spécifiques à chaque service à penser
           * besoin de repasser dessus pour le nommage avant de propager
           * besoin de coder leur affichage pour stats.chatons.org
           * besoin de paramétrer des moulinettes pour les récupérations automatisées de moulinettes


   * ONTOLOGIE
       * service.properties :
           * service.startdate # Date d'ouverture du service (type DATE, recommandé).
           * Pourquoi recommandé ? Proposition de passer en « obligatoire »
               * Pour : Cpm, Angie, Flo
               * Contre : 
           * TODO Cpm fiches modèles
           * TODO Cpm outil check
       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
           * ordre des questions à se poser : préfixe, sous-préfixe


       * Un jour peut-être : 
           * metrics.ci


       * question existentielle à propos de xxxx.monthlyactive :
           * constat que :
               * xxxx.accounts.monthlyactive.2021 : la moyenne ?
               * xxxx.accounts.monthlyactive.2021.months : pléonasme
               * xxxx.accounts.monthlyactive.2021.weeks : n'a pas de sens
               * xxxx.accounts.monthlyactive.2021.days : n'a pas de sens
           * proposition :
               * xxxx.accounts.active
               * pour : Angie, Cpm, MrFlo
               * TODO Cpm : propager dans metrics.properties et autres fichiers


       * [Metrics spécifiques aux services de cartes mentales]
           * metrics.mindmaps.count
           * metrics.mindmaps.accounts
           * metrics.mindmaps.accounts.active
           * metrics.mindmaps.database.bytes
           * metrics.mindmaps.files.bytes


       * [Metrics spécifiques aux services d'agrégation de flux RSS]
           * choix du préfixe :
               * metrics.rss vs metrics.rssagregator
               * à différencier de rssgenerator
               * rssagregator est plus explicite
           * metrics.rssreaders.feeds
           * metrics.rssreaders.accounts
           * metrics.rssreaders.accounts. active
           * metrics.rssreaders.database.bytes
           * metrics.rssreaders.files.bytes


       * [Metrics spécifiques aux générateurs de flux RSS]
           * metrics.rssgenerators.feeds
           * metrics.rssgenerators.database.bytes
           * metrics.rssgenerators.files.bytes


       * [Metrics spécifiques aux services de mails]
           * metrics.mails.sent
           * metrics.mails.received
           * metrics.mails.accounts
           * metrics.mails.accounts.active
           * metrics.mails.aliases
           * metrics.mails.database.bytes
           * metrics.mails.files.bytes


       * [Metrics spécifiques aux carnets d'adresses]
           * metrics.addressbooks.count
           * metrics.addressbooks.contacts
           * metrics.addressbooks.groups
           * metrics.addressbooks.accounts
           * metrics.addressbooks.database.bytes
           * metrics.addressbooks.files.bytes


       * [Metrics spécifiques aux services de stockage/partage de documents]
           * metrics.filesharing.files.count
           * metrics.filesharing.shares
           * metrics.filesharing.accounts
           * metrics.filesharing.database.bytes
           * metrics.filesharing.files.bytes


       * [Metrics spécifiques aux services d'agendas]
           * metrics.calendars.count
           * metrics.calendars.events
           * metrics.calendars.accounts
           * metrics.calendars.database.bytes
           * metrics.calendars.files.bytes


       * [Metrics spécifiques aux services de gestion de groupes]
           * metrics.groups.count
           * metrics.groups.accounts
           * metrics.groups.database.bytes
           * metrics.groups.files.bytes


       * [Metrics spécifiques aux services d'organisation d'événements]
           * metrics.events.count
           * metrics.events.accounts




Nombre d'évènements                           = 1146

Nombre d'évènements locaux                    = 34

Nombre de groupes                             = 113

Nombre de groupes locaux                      = 9

Nombre de commentaires                        = 342

Nombre de commentaires locaux                 = 8

Nombre d'instances abonnées                   = 12

Nombre d'instances suivies                    = 10





       * [Metrics spécifiques aux forums]
           * metrics.forums.posts
           * metrics.forums.likedposts
           * metrics.forums.topics
           * metrics.forums.privatemessages
           * metrics.forums.accounts
           * metrics.forums.accounts.active


       * [Metrics spécifiques aux services de traitement de texte collaboratif]
           * metrics.?.files
           * 





# 28e réunion du groupe de travail

### jeudi 15 avril 2021 à 11h15

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes :  Christian (Cpm) / Angie / MrFlos / Antoine



   * divers précédents :
       * création d'un schéma explicitant les subs
           * TODO Angie
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * TODO Cpm


   * revue de [https://stats.chatons.org/](https://stats.chatons.org/) 😍
       * page CHATONS : 
           * **décision d'afficher par défaut les organisations et services « actifs » (sans enddate ou avec enddate future)**
               * ne pas se contenter de regarder si le enddate est vide, comparer à la date du jour
               * plus tard éventuellement, ajout d'un fonction pour voir les autres aussi "le cimetière des chatons" 😆
               * TODO Cpm
       * page générique chaton : 
           * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
               * TODO Cpm
       * page « Statistiques » (fédération) :
           * ajouter un donuts sur les services de paiement
               * TODO Cpm
       * page « Statistiques chatons » (organization) :
           * nombre d'utilisateurs de l'organisation
               * TODO Cpm
           * un jour peut-être :
               * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
                   * par exemple pour les types d'inscription (à un service)
               * donuts sur les pays
                   * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays


   * revue des catégories : 
       * sur [https://stats.chatons.org/softwares.xhtml](https://stats.chatons.org/softwares.xhtml) : le logiciel jitsimeet indique comme catégorie n/a
           * FAIT Angie : ajout au fichier categories.properties


   * revue de fichiers properties de membres  :
       * Infini :
           * erreur crawling d'un fichier properties qui n'existe pas mais dont l'URL semble bien
           * un %20 qui traine, facile à corriger
           * [https://framagit.org/infini/chatonsinfos/-/issues/1](https://framagit.org/infini/chatonsinfos/-/issues/1)
           * TODO Infini corriger


   * revue des merge requests : [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * RAS


   * revue du forum :
       * [https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28](https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28)
           * ajouter un page pour lister les problèmes de crawling
               * TODO Cpm
       * [https://forum.chatons.org/t/service-properties-registration-status/2068](https://forum.chatons.org/t/service-properties-registration-status/2068)
           * service.registration.load  :
               * # Capacité à accueillir de nouveaux utilisateurs (un parmi {open|full}, obligatoire).
                   * TODO Cpm : propager dans service.properties et fichiers exemples demo
                       * FAIT
                   * TODO Cpm ajouter dans l'outil check de statoolinfos
                       * FAIT
                   * TODO page service signalisation par picto cadenas
                       * FAIT
                   * TODO vue liste services signalisation par picto cadenas
                       * FAIT
                   * TODO Cpm prévenir flo pour com quand c'est propagé
                       * FAIT
                   * TODO Flo informer sur le forum une fois le commit disponible
                       * FAIT : [https://forum.chatons.org/t/service-properties-registration-status/2068/8](https://forum.chatons.org/t/service-properties-registration-status/2068/8)
           * Ajout d'une colonne sur [https://stats.chatons.org/services.xhtml](https://stats.chatons.org/services.xhtml) avec un picto "cadenas"
               * TODO Cpm changer la légende par "actuellement fermé"


   * avancer avec le collectif sur la complétion des metrics ?
       * metrics spécifiques à chaque service à penser
           * besoin de repasser dessus pour le nommage avant de propager
           * besoin de coder leur affichage pour stats.chatons.org
           * besoin de paramétrer des moulinettes pour les récupérations automatisées de moulinettes


   * ONTOLOGIE
       * service.startdate à passer en obligatoire :
           * TODO Cpm fiches modèles
               * FAIT
           * TODO Cpm outil check
               * FAIT
       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
           * ordre des questions à se poser : préfixe, sous-préfixe


       * Un jour peut-être : 
           * metrics.ci


       * renommage de monthlyactive :
           * – xxxx.monthlyactive
           * + xxxx.accounts.active
           * TODO Cpm : propager dans metrics.properties et autres fichiers
               * FAIT


       * MAJ de [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties) avec les éléments de la dernière réunion
           * Angie : FAIT


       * [Metrics spécifiques aux forums]
           * metrics.forums.count
           * metrics.forums.posts
           * metrics.forums.likedposts
           * metrics.forums.topics
           * metrics.forums.categories
           * metrics.forums.privatemessages
           * metrics.forums.accounts
           * metrics.forums.accounts.active
           * metrics.forums.database.bytes
           * metrics.forums.files.bytes


       * [Metrics spécifiques aux services de traitement de texte collaboratif]
           * pads vs wordprocessors vs textprocessors vs onlineeditors vs texteditors vs wordeditors
           * metrics.textprocessors.files.count
           * metrics.textprocessors.characters
           * metrics.textprocessors.words
           * metrics.textprocessors.accounts
           * metrics.textprocessors.accounts.active
           * metrics.textprocessors.database.bytes
           * metrics.textprocessors.files.bytes


       * [Metrics spécifiques aux services de tableur collaboratif]
           * calcs vs spreadsheets
           * metrics.spreadsheets.files.count
           * metrics.spreadsheets.sheets
           * metrics.spreadsheets.cells
           * metrics.spreadsheets.accounts
           * metrics.spreadsheets.accounts.active
           * metrics.spreadsheets.database.bytes
           * metrics.spreadsheets.files.bytes


       * [Metrics spécifiques aux services de visioconférence]
           * metrics.videoconferencing.rooms (nombre de salons)
           * metrics.videoconferencing.conferences (nombre de sessions)
           * metrics.videoconferencing.hours
           * metrics.videoconferencing.joiners
           * metrics.videoconferencing.accounts
           * metrics.videoconferencing.accounts.active
           * metrics.videoconferencing.traffic.received.bytes
           * metrics.videoconferencing.traffic.sent.bytes
           * metrics.videoconferencing.database.bytes
           * metrics.videoconferencing.files.bytes


       * [Metrics spécifiques aux services d'audioconférence]
           * metrics.audioconferencing.rooms
           * metrics.audioconferencing.conferences
           * metrics.audioconferencing.hours
           * metrics.audioconferencing.joiners
           * metrics.audioconferencing.accounts
           * metrics.audioconferencing.accounts.active
           * metrics.audioconferencing.traffic.received.bytes
           * metrics.audioconferencing.traffic.sent.bytes
           * metrics.audioconferencing.database.bytes
           * metrics.audioconferencing.files.bytes


       * [Metrics spécifiques aux générateurs de code-barres]
           * metrics.barcodes.count (nombre de code-barres générés)
           * metrics.barcodes.database.bytes
           * metrics.barcodes.files.bytes


       * [Metrics spécifiques aux progiciels de Gestion Intégré]
           * metrics.erp.count
           * metrics.erp.accounts
           * metrics.erp.accounts.active
           * metrics.erp.database.bytes
           * metrics.erp.files.bytes


       * [Metrics spécifiques aux services de création de schémas et diagrammes]
           * metrics.diagrams.count
           * metrics.diagrams.accounts
           * metrics.diagrams.accounts.active
           * metrics.diagrams.database.bytes
           * metrics.diagrams.files.bytes


       * [Metrics spécifiques aux gestionnaires de projets]
           * metrics.projectmanagement
           * 

       * [Metrics spécifiques aux gestionnaires de mots de passe]


       * [Metrics spécifiques aux services de diffusion de fichiers audio]






# 29e réunion du groupe de travail

### jeudi 22 avril 2021 à 11h15

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes :  Antoine (Framasoft - stagiaire CHATONS), Christian (Cpm), 



   * divers précédents :
       * création d'un schéma explicitant les subs
           * TODO Angie
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * TODO Cpm


   * revue de [https://stats.chatons.org/](https://stats.chatons.org/) 😍
       * page CHATONS : 
           * **décision d'afficher par défaut les organisations et services « actifs » (sans enddate ou avec enddate future)**
               * ne pas se contenter de regarder si le enddate est vide, comparer à la date du jour
               * plus tard éventuellement, ajout d'un fonction pour voir les autres aussi "le cimetière des chatons" 😆
               * TODO Cpm
       * page générique chaton : 
           * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
               * TODO Cpm
       * page « Statistiques » (fédération) :
           * ajouter un donuts sur les services de paiement
               * TODO Cpm
       * page « Statistiques chatons » (organization) :
           * nombre d'utilisateurs de l'organisation
               * TODO Cpm
           * un jour peut-être :
               * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
                   * par exemple pour les types d'inscription (à un service)
               * donuts sur les pays
                   * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays


   * revue des catégories : 
       * RAS


   * revue de fichiers properties de membres  :
       * Infini :
           * erreur crawling d'un fichier properties qui n'existe pas mais dont l'URL semble bien
           * un %20 qui traine, facile à corriger
           * [https://framagit.org/infini/chatonsinfos/-/issues/1](https://framagit.org/infini/chatonsinfos/-/issues/1)
           * TODO Infini corriger
               * relancé une fois


   * revue des merge requests : [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * RAS


   * revue du forum :
       * [https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28](https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28)
           * ajouter un page pour lister les problèmes de crawling
               * TODO Cpm


   * avancer avec le collectif sur la complétion des metrics ?
       * metrics spécifiques à chaque service à penser
           * besoin de repasser dessus pour le nommage avant de propager
           * besoin de coder leur affichage pour stats.chatons.org
           * besoin de paramétrer des moulinettes pour les récupérations automatisées de moulinettes


   * ONTOLOGIE
       * campagne de communication 
           * sortie de la version 3 de l'ontologie
               * post précédent : [https://forum.chatons.org/t/version-0-2-de-chatonsinfos-mettez-a-jour-vos-fichiers-properties-o/1902](https://forum.chatons.org/t/version-0-2-de-chatonsinfos-mettez-a-jour-vos-fichiers-properties-o/1902)
           * TODO : répondre question du mandatory sur service.registration.load
               * le rendre optionnel et considérer une valeur par défaut  open
                   * Antoine : Angie OK
           * TODO Cpm : fichier CHANGELOG.md
           * TODO Antoine : préparer post pour le forum


       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
           * ordre des questions à se poser : préfixe, sous-préfixe


       * Un jour peut-être : 
           * metrics.ci


       * MAJ de [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties) avec les éléments de la dernière réunion
           * FAIT : Antoine




       * [Metrics spécifiques aux gestionnaires de projets de type kanban] (~~tracim?~~, kanboard, wekan, Nextcloud Boards)
           * metrics.kanbanboard.count
           * metrics.kanbanboard.lists (columns)
           * metrics.kanbanboard.cards
           * metrics.kanbanboard.members
           * metrics.kanbanboard.accounts
           * metrics.kanbanboard.accounts.active
           * metrics.kanbanboard.database.bytes
           * metrics.kanbanboard.files.bytes


Metrics a prévoir pour un outil de gestion de projet (à composer avec les metrics issues et/ou kanbanboards si besoin)

           * metrics.projectmanagement.projects
           * metrics.projectmanagement.accounts
           * metrics.projectmanagement.accounts.active
           * metrics.projectmanagement.database.bytes
           * metrics.projectmanagement.files.bytes


       * [Metrics spécifiques aux gestionnaires de mots de passe] (bitwarden)
           * préfixe : ~~passwordmanager~~ vs ~~passwordmanagement~~ vs ~~passwords~~ vs ~~safes~~ vs **vault**
           * metrics.vault.passwords
           * metrics.vault.passwords.generated
           * ( metrics.vault.passwords.logins)
           * (metrics.vault.passwords.cards)
           * (metrics.vault.passwords.id)
           * (metrics.vault.passwords.notes)
           * metrics.vault.safes ~~vaults~~
           * metrics.vault.folders
           * (metrics.vault.collections)
           * (metrics.vault.sends)
           * metrics.vault.accounts
           * metrics.vault.accounts.active
           * metrics.vault.database.bytes
           * metrics.vault.files.bytes


       * [Metrics spécifiques aux services de diffusion de fichiers audio] (funkwhale)
           * metrics.audios.count
           * metrics.audios.count.lives
           * metrics.audios.listenings
           * metrics.audios.comments
           * metrics.audios.accounts
           * metrics.audios.accounts.active
           * metrics.audios.channels
           * metrics.audios.database.bytes
           * metrics.audios.files.bytes
           * metrics.audios.federated.count
           * metrics.audios.federated.comments
           * metrics.audios.instances.followed
           * metrics.audios.instances.followers


           * metrics.audios.moderation.reports
           * metrics.audios.moderation.disabledaccounts
           * metrics.audios.moderation.silencedaccounts
           * metrics.audios.moderation.cancelledaccounts
       * 

       * metrics génériques pour la modération ?
       * 

       * 

       * [Metrics spécifiques aux services de partage temporaire de fichiers] 
           * Antoine : ça a été déjà fait ligne #2522 mais j'ai fait un mauvais control+f sur le git
               * [https://mypads.framapad.org/p/gt-stats-chatons-org-8h6ly7dn#L2522](https://mypads.framapad.org/p/gt-stats-chatons-org-8h6ly7dn#L2522)
               * ligne 125 du git
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties#L125](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties#L125)
               * // pendant la période
           * ~~metrics.drop.files.count~~
           * metrics.drop.shares
           * metrics.drop.secured   password
           * metrics.drop.singledownload
           * metrics.drop.duration.unlimited
           * metrics.drop.duration.annual
           * metrics.drop.duration.monthly
           * metrics.drop.duration.weekly
           * metrics.drop.duration.daily
           * metrics.drop.created
           * metrics.drop.expired
           * metrics.drop.purged
           * metrics.drop.deleted
           * metrics.drop.accounts
           * ~~metrics.drop.database.bytes~~
           * ~~metrics.drop.files.bytes~~
           * questions :
               * temporaryfilesharing vs drop ?
                   * drop fait penser à temporaire
                   * drop fait penser à dropbox donc pas si temporaire que ça
               * rajouter dans temporaryfilesharing le sous-prefixe duration ?
               * rajouter de nouveaux métriques 




**Catégories de Metrics restant à faire (ordre alphabétique)**

Administration de machines virtuelles // virtual machine administrator

Gestionnaire de finances personnelles (IHateMoney, Kresus) *

Mesure de statistiques (Dolomon, Matomo, Open Web Analytics)

~~Microblogging (Mastodon, Pleroma)~~ = compris dans réseaux sociaux / metrics.socialnetworks

Outils de monitoring (Healthchecks, Monitorix)

Progiciel de Gestion associative (Bénévalibre, Galette, Structura) *

Présentation en ligne (Strut)

VPN (OpenVPN)



***Catégories de Metrics restant mais qui n'ont pas de services sur stats (ordre alphabétique)***

*Gestionnaire de facturation / paiement*

*Gestionnaire de marques-pages (Shaarli)*

*Gestionnaire de tâches*

*Lettres d'informations (PHPList, wassup)*

*Outils de prise de décision (Loomio, VotAR)*

*Prise de note (Turtl)*

*Sauvegarde de contenus web (Wallabag)*

*Serveurs de jeux vidéos (Minetest, Trivabble)*

*Stockage et partage d'albums photos (Piwigo)  // Il me semble qu'on avait dit non*

*Autres (Nextcloud\&OnlyOffice)*

///

gestionnaires de projet (tracim)





# 30e réunion du groupe de travail

### jeudi 29 avril 2021 à 11h15

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes :  Antoine (Framasoft - stagiaire CHATONS), Christian (Cpm)



   * divers précédents :
       * création d'un schéma explicitant les subs
           * TODO Angie
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * TODO Cpm


   * revue de [https://stats.chatons.org/](https://stats.chatons.org/) 😍
       * page CHATONS : 
           * **décision d'afficher par défaut les organisations et services « actifs » (sans enddate ou avec enddate future)**
               * ne pas se contenter de regarder si le enddate est vide, comparer à la date du jour
               * plus tard éventuellement, ajout d'un fonction pour voir les autres aussi "le cimetière des chatons" 😆
               * TODO Cpm
       * page générique chaton : 
           * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
               * TODO Cpm
       * page « Statistiques » (fédération) :
           * ajouter un donuts sur les services de paiement
               * TODO Cpm
       * page « Statistiques chatons » (organization) :
           * nombre d'utilisateurs de l'organisation
               * TODO Cpm
           * un jour peut-être :
               * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
                   * par exemple pour les types d'inscription (à un service)
               * donuts sur les pays
                   * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays


   * revue des catégories ([https://stats.chatons.org/category-autres.xhtml)](https://stats.chatons.org/category-autres.xhtml)) : 
       * Icecast :
           * [https://stats.chatons.org/infini-radiosinfini.xhtml](https://stats.chatons.org/infini-radiosinfini.xhtml)
           * logiciel  [https://icecast.org/](https://icecast.org/)
           * est-ce un logiciel libre ? OUI
           * est-ce un vrai service aux utilisateurs ? OUI. Infini héberge des flux provenant d'autres entités, peut-être des membres ou autre. Par contre, absence de gestion de compte ou d'information sur comment ajouter son flux.
           * dans quelle(s) catégorie(s) l'ajouter ?
               * Diffusion de fichiers audio
               * Diffusion de fichiers vidéo
               * TODO Antoine : enrichir le fichier categories.properties


   * revue de fichiers properties de membres  :
       * Infini :
           * erreur crawling d'un fichier properties qui n'existe pas mais dont l'URL semble bien
           * un %20 qui traine, facile à corriger
           * [https://framagit.org/infini/chatonsinfos/-/issues/1](https://framagit.org/infini/chatonsinfos/-/issues/1)
           * TODO Infini corriger
               * relancé une fois
               * FAIT Infini a corrigé


   * revue des merge requests : [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * RAS


   * revue du forum :
       * [https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28](https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28)
           * ajouter un page pour lister les problèmes de crawling
               * TODO Cpm


   * avancer avec le collectif sur la complétion des metrics ?
       * metrics spécifiques à chaque service à penser
           * besoin de repasser dessus pour le nommage avant de propager
           * besoin de coder leur affichage pour stats.chatons.org
           * besoin de paramétrer des moulinettes pour les récupérations automatisées de moulinettes


   * ONTOLOGIE
       * campagne de communication 
           * sortie de la version 3 de l'ontologie
               * post précédent : [https://forum.chatons.org/t/version-0-2-de-chatonsinfos-mettez-a-jour-vos-fichiers-properties-o/1902](https://forum.chatons.org/t/version-0-2-de-chatonsinfos-mettez-a-jour-vos-fichiers-properties-o/1902)
           * TODO : répondre question du mandatory sur service.registration.load
               * le rendre optionnel et considérer une valeur par défaut open
                   * Antoine + Angie OK
                   * Cpm : OK
                   * Flo : ?
           * TODO Cpm : fichier CHANGELOG.md
           * TODO Antoine : préparer post pour le forum + mail + point réunion mensuelle


       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
           * ordre des questions à se poser : préfixe, sous-préfixe


       * Un jour peut-être : 
           * metrics.ci


       * MAJ de [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties) avec les éléments de la dernière réunion
           * TODO : Antoine
       * 

       * Métriques génériques pour la modération ?
           * jusqu'ici, c'est un sous préfixe :
               * metrics.xxx.moderation.reports
               * metrics.xxx.moderation.disabledaccounts
               * metrics.xxx.moderation.silencedaccounts
               * metrics.xxx.moderation.cancelledaccounts
           * avec le recul, il apparait que ce métric est générique à tous les services :
               * metrics.moderation.reports
               * metrics.moderation.sanctions ~~penalties~~
               * metrics.moderation.disabledaccounts
               * metrics.moderation.silencedaccounts
               * metrics.moderation.cancelledaccounts
               * TODO Angie Flo : valider OK
               * TODO Antoine : les retirerl es metrics déjà faits


       * [Metrics spécifiques aux services de partage temporaire de fichiers] 
           * Antoine : ça a été déjà fait ligne #2522 mais j'ai fait un mauvais control+f sur le git
               * [https://mypads.framapad.org/p/gt-stats-chatons-org-8h6ly7dn#L2522](https://mypads.framapad.org/p/gt-stats-chatons-org-8h6ly7dn#L2522)
               * ligne 125 du git
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties#L125](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties#L125)
               * // pendant la période
           * ~~metrics.drop.files.count~~
           * metrics.drop.shares
           * metrics.drop.secured / .password 
           * metrics.drop.singledownload
           * metrics.drop.duration.unlimited
           * metrics.drop.duration.annual
           * metrics.drop.duration.monthly
           * metrics.drop.duration.weekly
           * metrics.drop.duration.daily
           * metrics.drop.created
           * metrics.drop.expired
           * metrics.drop.purged
           * metrics.drop.deleted
           * metrics.drop.accounts
           * ~~metrics.drop.database.bytes~~
           * ~~metrics.drop.files.bytes~~
           * questions :
               * temporaryfilesharing vs drop ?
                   * drop fait penser à temporaire
                   * drop fait penser à dropbox donc pas si temporaire que ça
               * rajouter dans temporaryfilesharing le sous-prefixe duration ?
               * TODO Angie Flo : valider OK
               * TODO Antoine : rajouter de nouveaux métriques 


[Metrics spécifiques aux gestionnaires de finances personnelles] (IHateMoney, Kresus)

           * choix du préfixe : finances vs financesperso vs personalfinances vs ~~budget~~
               * finances +++
           * metrics.finances.accounts
           * metrics.finances.accounts.active
           * metrics.finances.bankaccounts
           * metrics.finances.categories
           * metrics.finances.operations
           * ~~metrics.finances.statements~~
           * ~~metrics.finances.participants~~
           * metrics.finances.database.bytes
           * metrics.finances.files.bytes


[Metrics spécifiques aux progiciels de Gestion associative] (Bénévalibre, Galette, Structura)

           * choix du préfixe : ~~asso~~ vs volonteering vs association vs nonprofitmanagement
               * asso ?
           * TODO Antoine : trouver une icône (actuellement default.svg)
           * TODO : trouver le site web de Structura
           * metrics.asso.count 
           * metrics.asso.actions
           * metrics.asso.actions.categories
           * metrics.asso.projects
           * metrics.asso.members
           * metrics.asso.members.contributions
           * metrics.asso.members.leaders
           * metrics.asso.groups
           * metrics.asso.accounts
           * metrics.asso.accounts.active
           * metrics.asso.database.bytes
           * metrics.asso.files.bytes
           * + modération ?


[Metrics spécifiques aux services de présentations en ligne] (Strut)

           * presentations vs diapositives vs diaporama vs slides
               * presentations ?
           * metrics.presentations.count
           * metrics.presentations.diapositives
           * metrics.presentations.accounts
           * metrics.presentations.accounts.active
           * metrics.presentations.database.bytes
           * metrics.presentations.files.bytes


[Metrics spécifiques aux VPN]

           * metrics.vpn.count     vs vpn.connections vs vpn.peering
           * metrics.vpn.accounts
           * metrics.vpn.accounts.active
           * metrics.vpn.database.bytes
           * metrics.vpn.files.bytes


**Catégories de Metrics restant à faire (ordre alphabétique)**

Administration de machines virtuelles // virtual machine administrator

Mesure de statistiques (Dolomon, Matomo, Open Web Analytics)

Outils de monitoring (Healthchecks, Monitorix)



***Catégories de Metrics restant mais qui n'ont pas de services sur stats (ordre alphabétique)***

*Gestionnaire de facturation / paiement*

*Gestionnaire de marques-pages (Shaarli)*

*Gestionnaire de tâches*

*Lettres d'informations (PHPList, wassup)*

*Outils de prise de décision (Loomio, VotAR)*

*Prise de note (Turtl)*

*Sauvegarde de contenus web (Wallabag)*

*Serveurs de jeux vidéos (Minetest, Trivabble)*

*Stockage et partage d'albums photos (Piwigo)  // Il me semble qu'on avait dit qu'idem à partage d'image*

///

gestionnaires de projet (tracim)





# 31e réunion du groupe de travail

### jeudi 6 mai 2021 à 11h15

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes :  Antoine (Framasoft - stagiaire CHATONS), Christian/Cpm (Chapril)



   * divers précédents :
       * création d'un schéma explicitant les subs
           * TODO Angie
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * TODO Cpm


   * revue de [https://stats.chatons.org/](https://stats.chatons.org/) 😍
       * page CHATONS : 
           * **décision d'afficher par défaut les organisations et services « actifs » (sans enddate ou avec enddate future)**
               * ne pas se contenter de regarder si le enddate est vide, comparer à la date du jour
               * plus tard éventuellement, ajout d'un fonction pour voir les autres aussi "le cimetière des chatons" 😆
               * TODO Cpm
       * page générique chaton : 
           * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
               * TODO Cpm
       * page « Statistiques » (fédération) :
           * ajouter un donuts sur les services de paiement
               * TODO Cpm
       * page « Statistiques chatons » (organization) :
           * nombre d'utilisateurs de l'organisation
               * TODO Cpm
           * un jour peut-être :
               * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
                   * par exemple pour les types d'inscription (à un service)
               * donuts sur les pays
                   * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays


   * revue des catégories ([https://stats.chatons.org/category-autres.xhtml)](https://stats.chatons.org/category-autres.xhtml)) : 
       * Icecast :
           * [https://stats.chatons.org/infini-radiosinfini.xhtml](https://stats.chatons.org/infini-radiosinfini.xhtml)
           * TODO Antoine : enrichir le fichier categories.properties FAIT
               * ~~ajouter dans catégorie « Diffusion de fichiers audio » ~~
               * ~~ajouter dans catégorie « Diffusion de fichiers vidéo »~~
               * finalement ajouté dans la catégorie *#Diffusion en direct de flux audio et vidéo* FAIT
       * Gestionnaire de mots de passe
           * Bitwarden :
               * ce logiciel est dépendant de logiciels non-libres donc n'est pas vraiment libre
               * incompatible charte CHATONS
               * TODO? Antoine : retirer de la liste FAIT
           *  Bitwarden\_rs
               * le logiciel a un nouveau nom : Vaultwarden
                   * [https://www.cachem.fr/vaultwarden-docker/](https://www.cachem.fr/vaultwarden-docker/)
               * TODO? Antoine : supprimer Bitwarden\_rs et ajouter Vaultwarden FAIT


   * revue de fichiers properties de membres :
       * RAS


   * revue des merge requests : [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * RAS


   * revue du forum : [https://forum.chatons.org/c/collectif/stats-chatons-org/83](https://forum.chatons.org/c/collectif/stats-chatons-org/83)
       * [https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28](https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28)
           * ajouter un page pour lister les problèmes de crawling
               * TODO Cpm


   * avancer avec le collectif sur la complétion des metrics ?
       * metrics spécifiques à chaque service à penser
           * besoin de repasser dessus pour le nommage avant de propager
           * besoin de coder leur affichage pour stats.chatons.org
           * besoin de paramétrer des moulinettes pour les récupérations automatisées de moulinettes


   * ONTOLOGIE
       * campagne de communication 
           * sortie de la version 3 de l'ontologie
               * post précédent : [https://forum.chatons.org/t/version-0-2-de-chatonsinfos-mettez-a-jour-vos-fichiers-properties-o/1902](https://forum.chatons.org/t/version-0-2-de-chatonsinfos-mettez-a-jour-vos-fichiers-properties-o/1902)
           * TODO : répondre question du mandatory sur service.registration.load
               * le rendre optionnel et considérer une valeur par défaut open
                   * Antoine + Angie OK
                   * Cpm : OK
                   * Flo : ?
                   * reformalisation de la question, qui voulons-nous avantager ? Ceux que la valeur par défaut contenteraient ou ceux qui doivent vraiment mettre une valeur « full » ? A priori, la deuxième et donc il faudrait que le champ soit obligatoire.
                       * pour : Cpm
           * TODO Cpm : fichier CHANGELOG.md
           * TODO Antoine : préparer post pour le forum + mail + point réunion mensuelle FAIT


       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
           * ordre des questions à se poser : préfixe, sous-préfixe


       * Un jour peut-être : 
           * metrics.ci


       * MAJ de [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties) avec les éléments de la dernière réunion
           * TODO : Antoine
       * 

       * Métriques génériques pour la modération ?
           * jusqu'ici, c'est un sous préfixe :
               * metrics.xxx.moderation.reports
               * metrics.xxx.moderation.disabledaccounts
               * metrics.xxx.moderation.silencedaccounts
               * metrics.xxx.moderation.cancelledaccounts
           * avec le recul, il apparait que ce métric est générique à tous les services :
               * metrics.moderation.reports
               * metrics.moderation.sanctions ~~penalties~~
               * metrics.moderation.disabledaccounts
               * metrics.moderation.silencedaccounts
               * metrics.moderation.cancelledaccounts
               * TODO Angie Flo : valider OK
               * TODO Antoine : les retirer les metrics déjà faits OK


       * [Metrics spécifiques aux services de partage temporaire de fichiers] 
           * Antoine : ça a été déjà fait ligne #2522 mais j'ai fait un mauvais control+f sur le git
               * [https://mypads.framapad.org/p/gt-stats-chatons-org-8h6ly7dn#L2522](https://mypads.framapad.org/p/gt-stats-chatons-org-8h6ly7dn#L2522)
               * ligne 125 du git
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties#L125](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties#L125)
               * // pendant la période
           * ~~metrics.drop.files.count~~
           * metrics.drop.shares
           * metrics.drop.secured / .password 
           * metrics.drop.singledownload
           * metrics.drop.duration.unlimited
           * metrics.drop.duration.annual
           * metrics.drop.duration.monthly
           * metrics.drop.duration.weekly
           * metrics.drop.duration.daily
           * metrics.drop.created
           * metrics.drop.expired
           * metrics.drop.purged
           * metrics.drop.deleted
           * metrics.drop.accounts
           * ~~metrics.drop.database.bytes~~
           * ~~metrics.drop.files.bytes~~
           * questions :
               * temporaryfilesharing vs drop ?
                   * drop fait penser à temporaire
                   * drop fait penser à dropbox donc pas si temporaire que ça
               * rajouter dans temporaryfilesharing le sous-prefixe duration ?
               * TODO Angie Flo : valider OK
               * TODO Antoine : rajouter de nouveaux métriques  OK


[Metrics spécifiques aux gestionnaires de finances personnelles] (IHateMoney, Kresus)

           * choix du préfixe : finances vs financesperso vs personalfinances vs ~~budget~~
               * finances +++
           * metrics.finances.accounts
           * metrics.finances.accounts.active
           * metrics.finances.bankaccounts
           * metrics.finances.categories
           * metrics.finances.operations
           * ~~metrics.finances.statements~~
           * ~~metrics.finances.participants~~
           * metrics.finances.database.bytes
           * metrics.finances.files.bytes


[Metrics spécifiques aux progiciels de Gestion associative] (Bénévalibre, Galette, Structura)

           * choix du préfixe : ~~asso~~ vs volonteering vs association vs nonprofitmanagement
               * asso ?
               * association
                   * pour : Antoine, Cpm
           * TODO Antoine : trouver une icône (actuellement default.svg) FAIT
           * + Organisation d'évènements (type mobilizon) n'avait pas non plus d'icône (ou pb ?) FAIT
           * logiciel Structura 
               * TODO : trouver le site web de Structura
                   * [https://structura.associatif.online/](https://structura.associatif.online/)
                   * Antoine : FAIT
               * TODO Antoine : le proposer dans Framalibre ? FAIT
               * (enfin déplacé sur ma todolist car j'ai d'autres choses à faire en rapport avec framalibre)
           * metrics.association.count 
           * metrics.association.actions
           * metrics.association.actions.categories
           * metrics.association.projects
           * metrics.association.members
           * metrics.association.members.contributions
           * metrics.association.members.leaders
           * metrics.association.groups
           * metrics.association.accounts
           * metrics.association.accounts.active
           * metrics.association.database.bytes
           * metrics.association.files.bytes
           * + modération ?


[Metrics spécifiques aux services de présentations en ligne] (Strut)

           * choix du préfixe :
               * presentations vs diapositives vs diaporama vs ~~slides~~ vs ~~slideshow~~
               * presentations ?
           * metrics.presentations.count
           * metrics.presentations.diapositives  vs  slides
           * metrics.presentations.accounts
               * champ générique ?
           * metrics.presentations.accounts.active
               * champ générique ?
           * metrics.presentations.database.bytes
               * champ générique ?
           * metrics.presentations.files.bytes
               * champ générique ?
           * TODO : rajouter en metrics génériques ces quatres là (Antoine) et modifier les autres métriques spécifiques FAIT


[Metrics spécifiques aux VPN]

           * (metrics.vpn.~~count~~     vs vpn.connections vs vpn.peering)
           * metrics.vpn.accounts
           * metrics.vpn.accounts.active
           * metrics.vpn.sent.bytes
           * metrics.vpn.received.bytes
           * metrics.vpn.database.bytes
           * metrics.vpn.files.bytes


**Catégories de Metrics restant à faire (ordre alphabétique)**

Administration de machines virtuelles // virtual machine administrator

Mesure de statistiques (Dolomon, Matomo, Open Web Analytics)

Outils de monitoring (Healthchecks, Monitorix)



***Catégories de Metrics restant mais qui n'ont pas de services sur stats (ordre alphabétique)***

*Gestionnaire de facturation / paiement*

*Gestionnaire de marques-pages (Shaarli)*

*Gestionnaire de tâches*

*Lettres d'informations (PHPList, wassup)*

*Outils de prise de décision (Loomio, VotAR)*

*Prise de note (Turtl)*

*Sauvegarde de contenus web (Wallabag)*

*Serveurs de jeux vidéos (Minetest, Trivabble)*

*Stockage et partage d'albums photos (Piwigo) *

///

gestionnaires de projet (tracim)







# 32e réunion du groupe de travail

### ~~jeudi 13 mai 2021 à 11h15~~

### ~~jeudi 20 mai 2021 à 11h15~~

### jeudi 27 mai 2021 à 11h15

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes : Christian/Cpm (Chapril), Antoine (Framasoft - stagiaire CHATONS)



   * divers précédents :
       * création d'un schéma explicitant les subs
           * TODO Angie/Antoine?
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * TODO Cpm


   * revue de [https://stats.chatons.org/](https://stats.chatons.org/) 😍
       * page CHATONS : 
           * **décision d'afficher par défaut les organisations et services « actifs » (sans enddate ou avec enddate future)**
               * ne pas se contenter de regarder si le enddate est vide, comparer à la date du jour
               * plus tard éventuellement, ajout d'un fonction pour voir les autres aussi "le cimetière des chatons" 😆
               * TODO Cpm
           * améliorer colonne date
               * Cpm FAIT
       * page générique d'un chaton : 
           * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
               * TODO Cpm
           * ajout d'un bouton « check alerts »
               * TODO Cpm FAIT
           * améliorer colonne date (1970…)
               * TODO Cpm FAIT
       * page « Statistiques » (fédération) :
           * ajouter un donuts sur les services de paiement
               * TODO Cpm
       * page « Statistiques chatons » (organization) :
           * nombre d'utilisateurs de l'organisation
               * TODO Cpm
           * un jour peut-être :
               * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
                   * par exemple pour les types d'inscription (à un service)
               * donuts sur les pays
                   * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays
           * metrics HTTP
               * TODO Cpm FAIT
       * page « Fichiers »
           * améliorer colonne dateŀ
               * Cpm FAIT
       * page « Statistiques service » :
           * metrics HTTP
               * TODO Cpm FAIT
       * refonte du crawl pour la gestion d'un journal de téléchargement
           * page fédération, page organisme, page service : ajout d'un bouton qui devient rouge si une erreur de téléchargement est recensée
           * nouvelles pages :
               * page journal pour la fédération
               * page journal pour chaque organisation
               * page journal pour chaque service
           * TODO Cpm FAIT


   * revue des catégories ([https://stats.chatons.org/category-autres.xhtml)](https://stats.chatons.org/category-autres.xhtml)) : 
       * Icecast :
           * [https://stats.chatons.org/infini-radiosinfini.xhtml](https://stats.chatons.org/infini-radiosinfini.xhtml)
           * TODO Antoine : enrichir le fichier categories.properties FAIT
               * ~~ajouter dans catégorie « Diffusion de fichiers audio » ~~
               * ~~ajouter dans catégorie « Diffusion de fichiers vidéo »~~
               * finalement ajouté dans la catégorie *#Diffusion en direct de flux audio et vidéo* FAIT
       * Gestionnaire de mots de passe
           * Bitwarden :
               * ce logiciel est dépendant de logiciels non-libres donc n'est pas vraiment libre
               * incompatible charte CHATONS
               * TODO? Antoine : retirer de la liste FAIT
           *  Bitwarden\_rs
               * le logiciel a un nouveau nom : Vaultwarden
                   * [https://www.cachem.fr/vaultwarden-docker/](https://www.cachem.fr/vaultwarden-docker/)
               * TODO? Antoine : supprimer Bitwarden\_rs et ajouter Vaultwarden FAIT
               * TODO Antoine : demander à Mario de mettre à jour sa fiche


   * revue de fichiers properties de membres : [https://stats.chatons.org/chatons-crawl.xhtml](https://stats.chatons.org/chatons-crawl.xhtml)
       * 3HG :     [https://stats.chatons.org/3hg-crawl.xhtml](https://stats.chatons.org/3hg-crawl.xhtml)
           * [https://stats.chatons.org/3hg-crawl.xhtml](https://stats.chatons.org/3hg-crawl.xhtml)
           * [https://www.3hg.fr/.well-known/3hg\_organization.properties](https://www.3hg.fr/.well-known/3hg\_organization.properties)
           * DOWNLOADERROR  
           * file.class = Service
           * TODO Cpm : envoyer un courriel à prx@si3t.ch, FAIT
           * TODO prx : en attente de réponse
       * Vulepcula :     [https://stats.chatons.org/vulpecula-crawl.xhtml](https://stats.chatons.org/vulpecula-crawl.xhtml)
           * [https://forge.vulpecula.fr/assets/gitlab\_logo-7ae504fe4f68fdebb3c2034e36621930cd36ea87924c11ff65dbcb8ed50dca58.png](https://forge.vulpecula.fr/assets/gitlab\_logo-7ae504fe4f68fdebb3c2034e36621930cd36ea87924c11ff65dbcb8ed50dca58.png)
           * DOWNLOADERROR 
           * TODO Cpm : envoyer un courriel à bonjour@vulpecula.fr, FAIT
           * TODO Niko : corriger, en cours
       * Bastet :    [https://stats.chatons.org/bastet-crawl.xhtml](https://stats.chatons.org/bastet-crawl.xhtml)
           * [https://bastet.parinux.org/.well-known/service-agendas.properties](https://bastet.parinux.org/.well-known/service-agendas.properties)
               * BADCHILDCLASS
               * subs.contacts = [https://bastet.parinux.org/.well-known/service-contacts.properties](https://bastet.parinux.org/.well-known/service-contacts.properties) 
               * TODO Cpm : informer Dino, FAIT
               * TODO Dino : FAIT
           * [https://bastet.parinux.org/.well-known/service-contacts.properties](https://bastet.parinux.org/.well-known/service-contacts.properties)
               * BADCHILDCLASS
               * subs.agendas = [https://bastet.parinux.org/.well-known/service-agendas.properties](https://bastet.parinux.org/.well-known/service-agendas.properties)
               * TODO Cpm : informer Dino, FAIT
               * TODO Dinon : FAIT


   * revue des merge requests : [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * RAS


   * revue du forum : [https://forum.chatons.org/c/collectif/stats-chatons-org/83](https://forum.chatons.org/c/collectif/stats-chatons-org/83)
       * [https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28](https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28)
           * ajouter un page pour lister les problèmes de crawling
               * TODO Cpm FAIT (voir ci-dessus la section « revue de [https://stats.chatons.org/](https://stats.chatons.org/)  »
               * TODO ? : répondre sur le forum en expliquant la nouvelle fonctionnalité


   * avancer avec le collectif sur la complétion des metrics ?
       * metrics spécifiques à chaque service à penser
           * besoin de repasser dessus pour le nommage avant de propager
           * besoin de coder leur affichage pour stats.chatons.org
           * besoin de paramétrer des moulinettes pour les récupérations automatisées de moulinettes


   * ONTOLOGIE
       * campagne de communication 
           * sortie de la version 3 de l'ontologie
               * post précédent : [https://forum.chatons.org/t/version-0-2-de-chatonsinfos-mettez-a-jour-vos-fichiers-properties-o/1902](https://forum.chatons.org/t/version-0-2-de-chatonsinfos-mettez-a-jour-vos-fichiers-properties-o/1902)
           * TODO Cpm : fichier CHANGELOG.md FAIT
           * TODO Antoine : préparer post pour le forum + mail + point réunion mensuelle  FAIT
               * [https://forum.chatons.org/t/chatonsinfos-version-0-3-mettez-a-jour-vos-fichiers-properties/2406](https://forum.chatons.org/t/chatonsinfos-version-0-3-mettez-a-jour-vos-fichiers-properties/2406)
           * TODO Antoine : faire un rappel FAIT


       * categories.properties
           * ~~Antoine ; j'ai actualisé le fichier categories.properties suivant les préfixes discutés à ce jour sur la complétion des métriques :~~
               * ~~+ harmonisation des préfixes~~
                   * ~~au singulier~~
                   * ~~impliquant impliciment (sauf indication contraire)~~
                       * ~~categories."prefix"[service] ou categories."prefix"[management]~~
                   * ~~Exemple : categories.event pour categories.event[management]~~
               * ~~+ corrections dans l'ordre des titres # et ##~~
               * Antoine : J'ai tout annulé 
           * Différentes possibilités de normes (selon les différents cas que l'on a maintenant)
                   * mot clé au singulier
                       * Exemple : "forge" "forum"
                   * mot clé au pluriel
                       * Exemple : "videos" "mails"
                   * action que le service fait
                       * Exemple : "filesharing" "webhosting" "videoconferencing"
                   * rôle que le service a
                       * Exemple : "urlshortenner"
                   * type de logiciel que le service utilise
                       * Exemple "erp"
               * Proposition (Antoine) : choisir une option et s'y tenir
               * Proposition (Antoine) : Je pense que "mot clé au singulier" est celui qui est la plus cohérente au regard des choix de sous-préfixes qu'y ont été faits jusqu'alors


           * Proposition (Antoine) : supprimer la catégorie ".saveforlater" et la fusionner avec ".bookmarking"
               * *##Sauvegarde de contenus web (alternative à Pocket, Instapaper, etc.)*
               * categories.saveforlater.name=Sauvegarde de contenus web
               * categories.saveforlater.description=
               * categories.saveforlater.logo=saveforlater.svg
               * categories.saveforlater.softwares=Wallabag


       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
           * ordre des questions à se poser : préfixe, sous-préfixe


       * Un jour peut-être : 
           * metrics.ci


       * MAJ de [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties) avec les éléments de la dernière réunion
           * TODO : Antoine FAIT


       * Métriques génériques pour la modération ?
           * jusqu'ici, c'est un sous préfixe :
               * metrics.xxx.moderation.reports
               * metrics.xxx.moderation.disabledaccounts
               * metrics.xxx.moderation.silencedaccounts
               * metrics.xxx.moderation.cancelledaccounts
           * avec le recul, il apparait que ce métric est générique à tous les services :
               * metrics.moderation.reports
               * metrics.moderation.sanctions ~~penalties~~
               * metrics.moderation.disabledaccounts
               * metrics.moderation.silencedaccounts
               * metrics.moderation.cancelledaccounts
               * TODO Angie Flo : valider OK
               * TODO Antoine : les retirer les metrics déjà faits OK


       * [Metrics spécifiques aux services de partage temporaire de fichiers] 
           * Antoine : ça a été déjà fait ligne #2522 mais j'ai fait un mauvais control+f sur le git
               * [https://mypads.framapad.org/p/gt-stats-chatons-org-8h6ly7dn#L2522](https://mypads.framapad.org/p/gt-stats-chatons-org-8h6ly7dn#L2522)
               * ligne 125 du git
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties#L125](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties#L125)
               * // pendant la période
           * ~~metrics.drop.files.count~~
           * metrics.drop.shares
           * metrics.drop.secured / .password 
           * metrics.drop.singledownload
           * metrics.drop.duration.unlimited
           * metrics.drop.duration.annual
           * metrics.drop.duration.monthly
           * metrics.drop.duration.weekly
           * metrics.drop.duration.daily
           * metrics.drop.created
           * metrics.drop.expired
           * metrics.drop.purged
           * metrics.drop.deleted
           * metrics.drop.accounts
           * ~~metrics.drop.database.bytes~~
           * ~~metrics.drop.files.bytes~~
           * questions :
               * temporaryfilesharing vs drop ?
                   * drop fait penser à temporaire
                   * drop fait penser à dropbox donc pas si temporaire que ça
               * rajouter dans temporaryfilesharing le sous-prefixe duration ?
               * TODO Angie Flo : valider OK
               * TODO Antoine : rajouter de nouveaux métriques 
                   * metrics.temporaryfilesharing.shares
                   * metrics.temporaryfilesharing.secured
                   * metrics.temporaryfilesharing.singledownload
                   * metrics.temporaryfilesharing.duration.unlimited
                   * metrics.temporaryfilesharing.duration.annual
                   * metrics.temporaryfilesharing.duration.monthly
                   * metrics.temporaryfilesharing.duration.weekly
                   * metrics.temporaryfilesharing.duration.daily
                   * metrics.temporaryfilesharing.created
                   * metrics.temporaryfilesharing.expired
                   * metrics.temporaryfilesharing.purged
                   * metrics.temporaryfilesharing.deleted


       * [Metrics spécifiques aux progiciels de Gestion associative] (Bénévalibre, Galette, Structura)
           * choix du préfixe : ~~asso~~ vs volonteering vs association vs nonprofitmanagement
               * asso ?
               * association
                   * pour : Antoine, Cpm
           * TODO Antoine : trouver une icône (actuellement default.svg) FAIT
           * + Organisation d'évènements (type mobilizon) n'avait pas non plus d'icône (ou pb ?) FAIT
           * logiciel Structura 
               * TODO : trouver le site web de Structura
                   * [https://structura.associatif.online/](https://structura.associatif.online/)
                   * Antoine : FAIT
               * TODO Antoine : le proposer dans Framalibre ? FAIT
               * (enfin déplacé sur ma todolist car j'ai d'autres choses à faire en rapport avec framalibre)
           * metrics.association.count 
           * metrics.association.actions
           * metrics.association.actions.categories
           * metrics.association.projects
           * metrics.association.members
           * metrics.association.members.contributions
           * metrics.association.members.leaders
           * metrics.association.groups
           * metrics.association.accounts
           * metrics.association.accounts.active
           * metrics.association.database.bytes
           * metrics.association.files.bytes
           * + modération ?


       * [Metrics spécifiques aux services de présentations en ligne] (Strut)
           * choix du préfixe :
               * presentations vs diapositives vs diaporama vs ~~slides~~ vs ~~slideshow~~
               * presentations ?
           * metrics.presentations.count
           * metrics.presentations.diapositives  vs  slides
           * metrics.presentations.accounts
               * champ générique ?
           * metrics.presentations.accounts.active
               * champ générique ?
           * metrics.presentations.database.bytes
               * champ générique ?
           * metrics.presentations.files.bytes
               * champ générique ?
           * TODO : rajouter en metrics génériques ces quatres là (Antoine) et modifier les autres métriques spécifiques FAIT






       * Administration de machines virtuelles // virtual machine administrator (Ganeti)
           * Choix du préfixe : vps (virtual private server)
           * metrics.vps.clusters
           * metrics.vps.nodes
           * metrics.vps.encrypted
           * metrics.vps.sent.bytes
           * metrics.vps.received.bytes
       * 

       * 

       * Mesure de statistiques (Dolomon, Matomo, Open Web Analytics)
           * Choix du préfixe : metrics (actuellement)... , stats, statistics, analytics
               * metrics.metrics.count ?? ça ne me semble pas une bonne idée / possible
           * Exemples de features (ici pour AWstats) [http://www.awstats.org/](http://www.awstats.org/)
           * 

           * metrics.stats.logs
   * 

   * 

       * Outils de monitoring (Healthchecks, Monitorix)
           * Choix du préfixe : monitoring


           * metrics.monitoring.


***Catégories de Metrics restant mais qui n'ont pas de services sur stats (ordre alphabétique)***

*Gestionnaire de facturation / paiement*

*Gestionnaire de marques-pages (Shaarli)*

*Gestionnaire de tâches*

*Lettres d'informations (PHPList, wassup)*

*Outils de prise de décision (Loomio, VotAR)*

*Prise de note (Turtl)*

*Sauvegarde de contenus web (Wallabag)*

*Serveurs de jeux vidéos (Minetest, Trivabble)*

*Stockage et partage d'albums photos (Piwigo)*

///

gestionnaires de projet (tracim)







# 33e réunion du groupe de travail

### jeudi 3 juin 2021 à 11h15

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*

 

Personnes présentes : mrflos (Colibris), Antoine (Framasoft - stagiaire chaton), Christian/Cpm (Chapril)



   * divers précédents :
       * création d'un schéma explicitant les subs
           * TODO Antoine
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * TODO Cpm


   * revue de [https://stats.chatons.org/](https://stats.chatons.org/) 😍
       * page CHATONS : 
           * **décision d'afficher par défaut les organisations et services « actifs » (sans enddate ou avec enddate future)**
               * ne pas se contenter de regarder si le enddate est vide, comparer à la date du jour
               * plus tard éventuellement, ajout d'un fonction pour voir les autres aussi "le cimetière des chatons" 😆
               * TODO Cpm
       * page générique d'un chaton : 
           * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
               * TODO Cpm
       * page « Statistiques » (fédération) :
           * ajouter un donuts sur les services de paiement
               * TODO Cpm 
       * page «  chatons » (organization) :
           * nombre d'utilisateurs de l'organisation
               * TODO Cpm FAIT
           * un jour peut-être :
               * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
                   * par exemple pour les types d'inscription (à un service)
               * donuts sur les pays
                   * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays
       * page « Fédération »
           * ajout d'un bouton métriques FAIT
       * page « Métrique fédération »
           * création Cpm FAIT
           * TODO Cpm splitter les pages trop lourdes
       * page « Métrique organization »
           * création Cpm FAIT
           * ajout des vues Années et semaines + refonte menu
           * TODO Cpm splitter les pages trop lourdes
       * page « Métrique service »
           * création Cpm FAIT
           * ajout des vues Années et semaines + refonte menu
           * TODO Cpm splitter les pages trop lourdes
       * espace disque :
           * la taille des fichiers générés pourrait prendre jusqu'à environ 10 Go.
           * y a-t-il un quota ? Lequel ?
           * la moulinette tourne sous le user framasoft\_angie
           * TODO Antoine vérifier
       * Flo :
           * j’aurais eu tendance a mettre de coté « Général » comme une partie de l’information générale du chatons, car les filtres d’année et de période ne sont pas pertinents pour ces stats là et elles décrivent plus une philosophie et des choix techniques que vraiment des stats
               * Cpm : affirmatif, général a maintenant son propre bouton, FAIT
           * peut être avoir dans résumé des moyennes sans graphes, genre du texte « sur 2020 XXX visiteurs uniques, YYY ips différentes »
               * Cpm : une notion de « tendance » ?
               * Cpm : donner exemple ?
               * TODO Flo, à réfléchir l'enrichissement de texte des graphes
               * TODO Flo, à réfléchir à des cadres de tendances dans « Résumé »
           * changer les intitulés « Web » et « Spécifique » par « Graphes de visites web » ou plus court « Graphes Web » et « Statistiques propres aux services » ou plus court « Stats des services » ?
               * Cpm : préciser l'intention
               * Flo : expliquer les items du menu type
               * TODO Cpm : ajouter des bulles
               * TODO Flo : tester le menu métriques auprès de personnes
               * TODO réfléchir
               * TODO revoir l'ordre du menu type
                   * générique, web, spécifique : vision technique, pas forcément pertinent
                   * web, générique, spécifique :
                   * générique, spécifique, web : plus intéressant pour l'utilsateur
                       * TODO mettre cet ordre


   * revue des catégories ([https://stats.chatons.org/category-autres.xhtml)](https://stats.chatons.org/category-autres.xhtml)) :
       * Gestionnaire de mots de passe
           *  Bitwarden\_rs
               * le logiciel a un nouveau nom : Vaultwarden
                   * [https://www.cachem.fr/vaultwarden-docker/](https://www.cachem.fr/vaultwarden-docker/)
               * TODO Antoine : supprimer Bitwarden\_rs et ajouter Vaultwarden FAIT
               * TODO Antoine : demander à Mario de mettre à jour sa fiche FAIT
               * TODO Mario : en cours
       * Icecast
           * TODO ???
               * Antoine : réglé par maj fichier properties FAIT
       * synapse/element
           * Synapse est le serveur Matrix
           * Element est le client web pour un serveur Synapse
           * TODO Flo demander à l'utilisateur de modifier sa fiche properties avec le champ x.modules
       * categories.properties
           * Pb avec le logo pour "Organisation d'évènements" 
               * il ne s'affiche pas
               * ça vient du bloqueur de pub
               * FAIT
           * Différentes possibilités de normes (selon les différents cas que l'on a maintenant)
                   * mot clé au singulier
                       * Exemple : "forge" "forum"
                   * mot clé au pluriel
                       * Exemple : "videos" "mails"
                   * action que le service fait
                       * Exemple : "filesharing" "webhosting" "videoconferencing"
                   * rôle que le service a
                       * Exemple : "urlshortenner"
                   * type de logiciel que le service utilise
                       * Exemple "erp"
               * Proposition (Antoine) : choisir une option et s'y tenir
               * Avis? :
                   * risque d'être plus une contrainte qu'utile
                   * pertinent de garder en tête la question car très utile
                   * attendre la deuxième revue ?
               * Proposition (Antoine) : Je pense que "mot clé au singulier" est celui qui est la plus cohérente au regard des choix de sous-préfixes qu'y ont été faits jusqu'alors
                   * (Antoine : je n'ai pas un avis ferme du tout)
                   * Pour : fonctionne avec le sous prefixe count très présent + 
               * Avis? (pas besoin d'en avoir un directement) :
                   * à réfléchir, on se laisse du temps pour prendre du recul
           * Proposition (Antoine) : supprimer la catégorie ".saveforlater" et la fusionner avec ".bookmarking"
               * *##Sauvegarde de contenus web (alternative à Pocket, Instapaper, etc.)*
               * categories.saveforlater.name=Sauvegarde de contenus web
               * categories.saveforlater.description=
               * categories.saveforlater.logo=saveforlater.svg
               * categories.saveforlater.softwares=Wallabag
               * Avis ?
                   * comment nommer la catégorie de fusion ?
                   * à réfléchir




   * revue de fichiers properties de membres : [https://stats.chatons.org/chatons-crawl.xhtml](https://stats.chatons.org/chatons-crawl.xhtml)
       * 3HG :     [https://stats.chatons.org/3hg-crawl.xhtml](https://stats.chatons.org/3hg-crawl.xhtml)
           * [https://stats.chatons.org/3hg-crawl.xhtml](https://stats.chatons.org/3hg-crawl.xhtml)
           * [https://www.3hg.fr/.well-known/3hg\_organization.properties](https://www.3hg.fr/.well-known/3hg\_organization.properties)
           * DOWNLOADERROR  
           * file.class = Service
           * TODO Cpm : envoyer un courriel à prx@si3t.ch, FAIT
           * TODO prx : en attente de réponse
           * TODO Antoine relancer 
       * Vulepcula :     [https://stats.chatons.org/vulpecula-crawl.xhtml](https://stats.chatons.org/vulpecula-crawl.xhtml)
           * [https://forge.vulpecula.fr/assets/gitlab\_logo-7ae504fe4f68fdebb3c2034e36621930cd36ea87924c11ff65dbcb8ed50dca58.png](https://forge.vulpecula.fr/assets/gitlab\_logo-7ae504fe4f68fdebb3c2034e36621930cd36ea87924c11ff65dbcb8ed50dca58.png)
           * DOWNLOADERROR 
           * TODO Cpm : envoyer un courriel à bonjour@vulpecula.fr, FAIT
           * TODO Niko : corriger,~~ en cours~~ FAIT


   * revue des merge requests : [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * RAS


   * revue du forum : [https://forum.chatons.org/c/collectif/stats-chatons-org/83](https://forum.chatons.org/c/collectif/stats-chatons-org/83)
       * [https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28](https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28)
           * ajouter un page pour lister les problèmes de crawling
               * TODO Cpm FAIT (voir ci-dessus la section « revue de [https://stats.chatons.org/](https://stats.chatons.org/)  »
               * TODO Antoine : répondre sur le forum en expliquant la nouvelle fonctionnalité
                   * Nouveau sujet indépendant
                   * + répondre sur le topic original
                   * Photo du nuage
       * [https://forum.chatons.org/t/pendant-ce-temps-chez-hadoly/2489](https://forum.chatons.org/t/pendant-ce-temps-chez-hadoly/2489)
           * RAS




   * avancer avec le collectif sur la complétion des metrics ?
       * metrics spécifiques à chaque service à penser
           * besoin de repasser dessus pour le nommage avant de propager
           * besoin de coder leur affichage pour stats.chatons.org
           * besoin de paramétrer des moulinettes pour les récupérations automatisées de moulinettes


   * ONTOLOGIE


       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
           * ordre des questions à se poser : préfixe, sous-préfixe


       * Un jour peut-être : 
           * metrics.ci


       * MAJ de [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties) avec les éléments de la dernière réunion
           * TODO : Antoine FAIT
       * 

       * Métriques génériques
           * préfixe spécifique au metrics géneriques ?
               * metrics.users.count vs metrics.generic.users.count vs  metrics.cross.users.count vs metrics.default.users.count vs metrics.trans.users.count
               * TODO avis : plutôt pour, avec « generic »
               * problème vu ci-après :
                   * metrics.generic.moderation.* n'a pas de sens
                   * donc le vrai problème ça serait que metrics.accouts, metrics.databse manquent d'un vrai préfixe
                   * TODO trouver lequel :
                       * metrics.application
                       * metrics.
           * TODO Antoine : rajouter en metrics génériques ces quatres là
           * + TODO Antoine : màj des métriques spécifiques
               * metrics.generic.accounts  ~~/  accounts.count ~~
               * metrics.generic.accounts.active
               * metrics.generic.database.bytes : OK
               * metrics.generic.files.bytes : OK
           * Métriques génériques pour la modération ?
               * jusqu'ici, c'est un sous préfixe :
                   * metrics.xxx.moderation.reports
                   * metrics.xxx.moderation.disabledaccounts
                   * metrics.xxx.moderation.silencedaccounts
                   * metrics.xxx.moderation.cancelledaccounts
               * avec le recul, il apparait que ce métric est générique à tous les services :
                   * metrics.moderation.reports
                   * metrics.moderation.sanctions ~~penalties~~
                   * metrics.moderation.disabledaccounts
                   * metrics.moderation.silencedaccounts
                   * metrics.moderation.cancelledaccounts
                   * TODO valider :
                       * Angie : OK
                       * Flo :  OK     
                   * TODO Antoine : les retirer des metrics
                       * FAIT
           * préfixe spécifique au metrics géneriques ?
               * metrics.users.count vs metrics.generic.users.count vs  metrics.cross.users.count vs metrics.default.users.count
               * TODO avis : 
               * TODO? propagation
           * métriques génériques de durée de vie
               * comme pour pics et temporary files sharing
                   * exemple : pad, calc, presentation...
               * Avis? :
           * métriques génériques pour les services fédérés
               * comme pour videos ou social networks
                   * exemple : funkwhale, events,
                   * peut-être d'autres arriveront
               * Avis? :


       * [Metrics spécifiques aux services de partage temporaire de fichiers] 
           * Antoine : ça a été déjà fait ligne #2522 mais j'ai fait un mauvais control+f sur le git
               * [https://mypads.framapad.org/p/gt-stats-chatons-org-8h6ly7dn#L2522](https://mypads.framapad.org/p/gt-stats-chatons-org-8h6ly7dn#L2522)
               * ligne 125 du git
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties#L125](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties#L125)
               * // pendant la période
           * ~~metrics.drop.files.count~~
           * metrics.drop.shares
           * metrics.drop.secured / .password 
           * metrics.drop.singledownload
           * metrics.drop.duration.unlimited
           * metrics.drop.duration.annual
           * metrics.drop.duration.monthly
           * metrics.drop.duration.weekly
           * metrics.drop.duration.daily
           * metrics.drop.created
           * metrics.drop.expired
           * metrics.drop.purged
           * metrics.drop.deleted
           * metrics.drop.accounts
           * ~~metrics.drop.database.bytes~~
           * ~~metrics.drop.files.bytes~~
           * questions :
               * temporaryfilesharing vs drop ?
                   * drop fait penser à temporaire
                   * drop fait penser à dropbox donc pas si temporaire que ça
               * rajouter dans temporaryfilesharing le sous-prefixe duration ?
               * TODO Angie Flo : valider OK                                                    <------------- ???
               * TODO Antoine : rajouter de nouveaux métriques 
                   * metrics.temporaryfilesharing.shares
                   * metrics.temporaryfilesharing.secured
                   * metrics.temporaryfilesharing.singledownload
                   * metrics.temporaryfilesharing.duration.unlimited
                   * metrics.temporaryfilesharing.duration.annual
                   * metrics.temporaryfilesharing.duration.monthly
                   * metrics.temporaryfilesharing.duration.weekly
                   * metrics.temporaryfilesharing.duration.daily
                   * metrics.temporaryfilesharing.created
                   * metrics.temporaryfilesharing.expired
                   * metrics.temporaryfilesharing.purged
                   * metrics.temporaryfilesharing.deleted


       * [Metrics spécifiques aux progiciels de Gestion associative] (Bénévalibre, Galette, Structura)
           * choix du préfixe : ~~asso~~ vs volonteering vs association vs nonprofitmanagement
               * asso ?
               * association
                   * pour : Antoine, Cpm
           * TODO Antoine : trouver une icône (actuellement default.svg) FAIT
           * + Organisation d'évènements (type mobilizon) n'avait pas non plus d'icône (ou pb ?) FAIT
           * logiciel Structura 
               * TODO : trouver le site web de Structura
                   * [https://structura.associatif.online/](https://structura.associatif.online/)
                   * Antoine : FAIT
               * TODO Antoine : le proposer dans Framalibre ? FAIT
               * (enfin déplacé sur ma todolist car j'ai d'autres choses à faire en rapport avec framalibre)
           * metrics.association.count 
           * metrics.association.actions
           * metrics.association.actions.categories
           * metrics.association.projects
           * metrics.association.members
           * metrics.association.members.contributions
           * metrics.association.members.leaders
           * metrics.association.groups
           * metrics.association.accounts
           * metrics.association.accounts.active
           * metrics.association.database.bytes
           * metrics.association.files.bytes




       * [Metrics spécifiques aux services de présentations en ligne] (Strut)
           * choix du préfixe :
               * presentations vs diapositives vs diaporama vs ~~slides~~ vs ~~slideshow~~
               * presentations ?
           * metrics.presentations.count
           * metrics.presentations.diapositives  vs  slides
           * metrics.presentations.accounts
               * champ générique ?
           * metrics.presentations.accounts.active
               * champ générique ?
           * metrics.presentations.database.bytes
               * champ générique ?
           * metrics.presentations.files.bytes
               * champ générique ?
           * TODO : rajouter en metrics génériques ces quatres là (Antoine) et modifier les autres métriques spécifiques FAIT                 <------------- ???






       * Administration de machines virtuelles // virtual machine administrator (Ganeti)
               * Choix du préfixe : vps (virtual private server)
           * metrics.vps.clusters
           * metrics.vps.nodes
           * metrics.vps.encrypted
           * metrics.vps.sent.bytes
           * metrics.vps.received.bytes
       * 

       * 

       * Mesure de statistiques (Dolomon, Matomo, Open Web Analytics)
               * Choix du préfixe : metrics (actuellement)... , stats, statistics, analytics
                   * metrics.metrics.count ?? ça ne me semble pas une bonne idée / possible
               * Exemples de features (ici pour AWstats) [http://www.awstats.org/](http://www.awstats.org/)
           * metrics.stats.logs
           * metrics.stats.
   * 

   * 

       * Outils de monitoring (Healthchecks, Monitorix)
               * Choix du préfixe : monitoring
           * metrics.monitoring.checks
           * metrics.monitoring.




       * Gestionnaire de marques-pages (Shaarli)
       * et Sauvegarde de contenus web (Wallabag) ?
               * Choix du préfixe : bookmark, saveforlater
           * metrics.bookmark.count
           * metrics.bookmark.




***Catégories de Metrics restant mais qui n'ont pas de services sur stats (ordre alphabétique)***

*Gestionnaire de facturation / paiement*

*Gestionnaire de tâches*

*Lettres d'informations (PHPList, wassup)*

*Outils de prise de décision (Loomio, VotAR)*

*Prise de note (Turtl)*

*Serveurs de jeux vidéos (Minetest, Trivabble)*

*Stockage et partage d'albums photos (Piwigo)*

///

gestionnaires de projet (tracim) (alternative à monday) = filesharing comme nextcloud ? \& group ?







# 34e réunion du groupe de travail

### jeudi 10 juin 2021 à 11h15

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*

 

Personnes présentes : Antoine (Framasoft - CHATONS), Christian/Cpm (Chapril), Florian (colibris)



   * question de la persistance des compte-rendus de réunions
       * le pad est un espace temporaire de travail
       * où persister ?
           * page wiki dans le dépôt ChatonsInfos
           * 1 document LibreOffice par réunion
           * 1 document LibreOffice pour toutes les réunions
       * Antoine volontaire avec disponibilité en juillet


   * divers précédents :
       * création d'un schéma explicitant les subs
           * TODO Antoine
           * Combien de fichiers metrics ?
               * Ce sont des subs de organization ou par services ?
               * fédération,  organization et services peuvent avoir un fichier métrics
               * un fichier métrics ne peut pas contenir de sub
               * question FAIT
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * TODO Cpm


   * revue de [https://stats.chatons.org/](https://stats.chatons.org/) 😍
       * page CHATONS : 
           * **décision d'afficher par défaut les organisations et services « actifs » (sans enddate ou avec enddate future)**
               * ne pas se contenter de regarder si le enddate est vide, comparer à la date du jour
               * plus tard éventuellement, ajout d'un fonction pour voir les autres aussi "le cimetière des chatons" 😆
               * TODO Cpm
       * page générique d'un chaton : 
           * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
               * TODO Cpm
       * page « Statistiques » (fédération) :
           * ajouter un donuts sur les services de paiement
               * TODO Cpm 
       * un jour peut-être :
           * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
               * par exemple pour les types d'inscription (à un service)
           * donuts sur les pays
               * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays
       * page « Fédération »
           * TODO Cpm ajout colonne « Visites mensuelles » FAIT
       * page « Métriques fédération »
           * TODO Cpm splitter les pages trop lourdes FAIT
       * page « Métriques organization »
           * TODO Cpm splitter les pages trop lourdes FAIT
       * page « Métriques service »
           * TODO Cpm splitter les pages trop lourdes FAIT
       * espace disque :
           * la taille des fichiers générés pourrait prendre jusqu'à environ 10 Go.
           * y a-t-il un quota ? Lequel ?
           * la moulinette tourne sous le user framasoft\_angie
           * TODO Antoine vérifie FAIT
               * Retours : Aucun problème, s'il faut 10 Go il y aura 10 Go
                           * + Très surpris que ça pèse autant
                   * (4 types x 4 vues x 4 périodes) pages x (400 services + 200 organizations) = 38400 fichiers
       * pages Uptimes (Federation, Organization, Services)
           * Cpm FAIT
           * des améliorations à faire
           * vocabulaire :
               * anglais : uptime,  monitoring
               * français : disponibilité des services, supervision, disponibilité, "~~Etats ~~Disponibilité des services mutualisé"
           * questions de statut manuel vs statut mesuré (page organization)
               * statut manuel seulement
               * statut mesuré seulement
               * les deux
               * un seul combiné des deux
           * partager avec les membres :
               * ~~forum~~
               * ~~réunion ~~
               * forum + réunion ++++
                   * toujours mentionné « expérimental »
                   * TODO Antoine : message forum + entrée pad réunion
       * Flo :
           * peut être avoir dans résumé des moyennes sans graphes, genre du texte « sur 2020 XXX visiteurs uniques, YYY ips différentes »
               * Cpm : une notion de « tendance » ?
               * Cpm : donner exemple ?
               * TODO Flo, à réfléchir l'enrichissement de texte des graphes
               * TODO Flo, à réfléchir à des cadres de tendances dans « Résumé »
           * changer les intitulés « Web » et « Spécifique » par « Graphes de visites web » ou plus court « Graphes Web » et « Statistiques propres aux services » ou plus court « Stats des services » ?
               * Cpm : préciser l'intention
               * Flo : expliquer les items du menu type
               * TODO Cpm : ajouter des bulles
               * TODO Flo : tester le menu métriques auprès de personnes
                   * en cours
               * TODO réfléchir
               * TODO revoir l'ordre du menu type
                   * générique, web, spécifique : vision technique, pas forcément pertinent
                   * web, générique, spécifique :
                   * générique, spécifique, web : plus intéressant pour l'utilsateur
                       * TODO Cpm mettre cet ordre FAIT
           * TODO Cpm afficher les champs nom et description des métrics dans les diagrammes


   * revue des catégories ([https://stats.chatons.org/category-autres.xhtml)](https://stats.chatons.org/category-autres.xhtml)) :
       * Gestionnaire de mots de passe
           *  Bitwarden\_rs
               * le logiciel a un nouveau nom : Vaultwarden
                   * [https://www.cachem.fr/vaultwarden-docker/](https://www.cachem.fr/vaultwarden-docker/)
               * TODO Mario : en cours FAIT
       * synapse/element
           * Synapse est le serveur Matrix
           * Element est le client web pour un serveur Synapse
           * TODO Flo demander à l'utilisateur de modifier sa fiche properties avec le champ x.modules FAIT
           * TODO Stéphane : corriger, en cours
       * Drawio
           * underwolrd
           * [https://github.com/jgraph/drawio/blob/dev/README.md](https://github.com/jgraph/drawio/blob/dev/README.md)
               * « diagrams.net, previously draw.io,  »
           * TODO Flo ajouter la graphie « Drawio » fait
           * TODO Antoine informer Underworld du changement de nom/politique, à surveiller
       * Nextcloud\&OnlyOffice
           * Sans Nuage/ARN
           * TODO Antoine relancer demande 
       * categories.properties
           * Proposition (Antoine) : supprimer la catégorie ".saveforlater" et la fusionner avec ".bookmarking"
               * *##Sauvegarde de contenus web (alternative à Pocket, Instapaper, etc.)*
               * categories.saveforlater.name=Sauvegarde de contenus web
               * categories.saveforlater.description=
               * categories.saveforlater.logo=saveforlater.svg
               * categories.saveforlater.softwares=Wallabag
               * Avis ?
                   * comment nommer la catégorie de fusion ?
                   * à réfléchir


   * revue de fichiers properties de membres : [https://stats.chatons.org/chatons-crawl.xhtml](https://stats.chatons.org/chatons-crawl.xhtml)
       * 3HG :     [https://stats.chatons.org/3hg-crawl.xhtml](https://stats.chatons.org/3hg-crawl.xhtml)
           * [https://www.3hg.fr/.well-known/3hg\_organization.properties](https://www.3hg.fr/.well-known/3hg\_organization.properties)
           * DOWNLOADERROR  
           * file.class = Service
           * TODO Cpm : envoyer un courriel à prx@si3t.ch, FAIT
           * TODO prx : en attente de réponse
           * TODO Antoine relancer FAIT
               * "BADCHILDCLASS" indiqué désormais. Ce n'est donc pas un problème de leur côté ?
           * TODO 3hg corriger FAIT


   * revue des tickets :
       * [https://framagit.org/chatons/chatonsinfos/-/issues/1](https://framagit.org/chatons/chatonsinfos/-/issues/1)
           * Redesign des encarts au dessus des tableaux
           * prévu lorsqu'on aura toutes les informations affichées
           * statut : plus tard
       * [https://framagit.org/chatons/chatonsinfos/-/issues/2](https://framagit.org/chatons/chatonsinfos/-/issues/2)
           * Dans le tableau des services de la fiche organisation des chatons, supprimer la colonne "Organisation"
           * Cpm : ce tableau est une vue mutualisée entre plusieurs pages : organisation, services, catégorie,  logiciel ; l'information est effectivement redondante pour la page organisation, mais ça permet de conserver l'homogénéité de la vue.
           * Cpm : pour gagner de la place, possibilité de ne mettre que le logo de l'organisation et le nom en bulle
           * statut : réfléchir et sinon sera traité par la grande revue visuelle prévue un jour
       * [https://framagit.org/chatons/chatonsinfos/-/issues/4](https://framagit.org/chatons/chatonsinfos/-/issues/4)
           * Penser un nouveau fichier properties dédié aux offres non logicielles
           * statut : priorité aux services utilisateurs donc pertinent mais plus tard
       * [https://framagit.org/chatons/chatonsinfos/-/issues/6](https://framagit.org/chatons/chatonsinfos/-/issues/6)
           * Catégorie : Création de schémas et diagrammes - confusion
               * statut : nouveau
               * « Dans [https://stats.chatons.org/category-creationdeschemasetdiagrammes.xhtml](https://stats.chatons.org/category-creationdeschemasetdiagrammes.xhtml) La catégorie liste des "logiciels" qui n'en sont pas. "diagrams.net" et "draw.io" sont des SERVICES qui utilisent le LOGICIEL "drawio" ([https://github.com/jgraph/drawio)](https://github.com/jgraph/drawio)) exemple : [https://stats.chatons.org/software-drawio.xhtml](https://stats.chatons.org/software-drawio.xhtml) »
           * TODO trouver une bonne source d'information
       * [https://framagit.org/chatons/chatonsinfos/-/issues/7](https://framagit.org/chatons/chatonsinfos/-/issues/7)
           * Proposition de rationalisation "Sondages, Questionnaires et RDV"
           * statut : nouveau
           * a priori, volonté de se mettre à la place des utilisateurs de ne pas avoir trop de propositions hors sujet
           * TODO Antoine répondre
       * [https://framagit.org/chatons/chatonsinfos/-/issues/8](https://framagit.org/chatons/chatonsinfos/-/issues/8)
           * Trie par la colonne "Date"
           * Cpm : FAIT


   * revue des merge requests : [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/30](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/30)
           * Update organization url for Chère de Prince 
           * Cpm : FAIT
           * suggestion de faire un message sur le forum 
               * TODO Antoine : évaluer la pertinence et faire la réalisation FAIT
               * Forum : [https://forum.chatons.org/t/encore-un-chaton-sur-chatonsinfos/2503](https://forum.chatons.org/t/encore-un-chaton-sur-chatonsinfos/2503)


       * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/31](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/31)
           * proposition de remise en forme de la documentation par @labecasse (Chère de Prince)
           * « Je propose de déplacer la documentation essentielle pour la mise en place dans le README. »
           * Avis ?
               * bonne proposition
               * changer le titre « documentation » en « Bien commencer »
           * TODO Antoine : accepter le merge + mettre un petit message de remerciement
           * TODO Antoine : faire une revue de titre et de plan
           * TODO Antoine : dans la FAQ, conserver la section et mettre une phrase pour envoyer sur le README


   * revue du forum : [https://forum.chatons.org/c/collectif/stats-chatons-org/83](https://forum.chatons.org/c/collectif/stats-chatons-org/83)
       * [https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28](https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/28)
           * ajouter un page pour lister les problèmes de crawling
               * TODO Antoine : répondre sur le forum en expliquant la nouvelle fonctionnalité FAIT
                   * Nouveau sujet indépendant FAIT
                   * + répondre sur le topic original FAIT
                   * Photo du nuage
               * Forum : [https://forum.chatons.org/t/nouveaute-un-journal-de-telechargement-pour-stats-chatons-org/2505](https://forum.chatons.org/t/nouveaute-un-journal-de-telechargement-pour-stats-chatons-org/2505)


   * avancer avec le collectif sur la complétion des metrics ?
       * metrics spécifiques à chaque service à penser
           * besoin de repasser dessus pour le nommage avant de propager
           * besoin de coder leur affichage pour stats.chatons.org
           * besoin de paramétrer des moulinettes pour les récupérations automatisées de moulinettes


   * ONTOLOGIE


       * organization
           * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/30#note\_1011677](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/30#note\_1011677)
           * « pourquoi ne pas ajouter les coordonnées GPS de l'organisation + une adresse ? »
           * avis ?
               * ~~organization.gps~~
               * ~~organization.coordinates~~
               * ~~organization.coordinates.latitude~~
               * ~~organization.coordinates.longitude~~
               * ~~organization.address~~
               * organization.geolocation.latitude
               * organization.geolocation.longitude
               * organization.geolocation.address
           * TODO Antoine : ajouter dans le fichier modèle organization


       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
           * ordre des questions à se poser : préfixe, sous-préfixe


       * Un jour peut-être : 
           * metrics.ci


       * MAJ de [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties) avec les éléments de la dernière réunion
           * 

       * 

       * Métriques génériques
           * préfixe spécifique au metrics géneriques ?
               * metrics.users.count vs metrics.generic.users.count vs  metrics.cross.users.count vs metrics.default.users.count vs metrics.trans.users.count
               * TODO avis : plutôt pour, avec « generic »
               * problème vu ci-après :
                   * metrics.generic.moderation.* n'a pas de sens
                   * donc le vrai problème ça serait que metrics.accouts, metrics.databse manquent d'un vrai préfixe
                   * TODO trouver lequel :
                       * ~~metrics.generic.accounts~~
                       * metrics.application.database.bytes
                       * metrics.general.database.bytes
                       * metrics.various.database.bytes
                       * metrics.service.database.bytes  +
                       * metrics.????.database.bytes
                       * metrics.????.database.bytes
                       * metrics.????.database.bytes
           * TODO Antoine : rajouter en metrics génériques ces quatres là
           * + TODO Antoine : màj des métriques spécifiques
               * metrics.generic.accounts  ~~/  accounts.count ~~
               * metrics.generic.accounts.active
               * metrics.generic.database.bytes : OK
               * metrics.generic.files.bytes : OK
           * Métriques génériques pour la modération ?
               * jusqu'ici, c'est un sous préfixe :
                   * metrics.xxx.moderation.reports
                   * metrics.xxx.moderation.disabledaccounts
                   * metrics.xxx.moderation.silencedaccounts
                   * metrics.xxx.moderation.cancelledaccounts
               * avec le recul, il apparait que ce métric est générique à tous les services :
                   * metrics.moderation.reports
                   * metrics.moderation.sanctions ~~penalties~~
                   * metrics.moderation.disabledaccounts
                   * metrics.moderation.silencedaccounts
                   * metrics.moderation.cancelledaccounts
                   * TODO valider :
                       * Angie : OK
                       * Flo :  OK     
                   * TODO Antoine : les retirer des metrics
                       * FAIT
           * préfixe spécifique au metrics géneriques ?
               * metrics.users.count vs metrics.generic.users.count vs  metrics.cross.users.count vs metrics.default.users.count
               * TODO avis : 
               * TODO? propagation
           * métriques génériques de durée de vie
               * comme pour pics et temporary files sharing
                   * exemple : pad, calc, presentation...
               * Avis? :
           * métriques génériques pour les services fédérés
               * comme pour videos ou social networks
                   * exemple : funkwhale, events,
                   * peut-être d'autres arriveront
               * Avis? :


       * [Metrics spécifiques aux services de partage temporaire de fichiers] 
           * Antoine : ça a été déjà fait ligne #2522 mais j'ai fait un mauvais control+f sur le git
               * [https://mypads.framapad.org/p/gt-stats-chatons-org-8h6ly7dn#L2522](https://mypads.framapad.org/p/gt-stats-chatons-org-8h6ly7dn#L2522)
               * ligne 125 du git
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties#L125](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties#L125)
               * // pendant la période
           * ~~metrics.drop.files.count~~
           * metrics.drop.shares
           * metrics.drop.secured / .password 
           * metrics.drop.singledownload
           * metrics.drop.duration.unlimited
           * metrics.drop.duration.annual
           * metrics.drop.duration.monthly
           * metrics.drop.duration.weekly
           * metrics.drop.duration.daily
           * metrics.drop.created
           * metrics.drop.expired
           * metrics.drop.purged
           * metrics.drop.deleted
           * metrics.drop.accounts
           * ~~metrics.drop.database.bytes~~
           * ~~metrics.drop.files.bytes~~
           * questions :
               * temporaryfilesharing vs drop ?
                   * drop fait penser à temporaire
                   * drop fait penser à dropbox donc pas si temporaire que ça
               * rajouter dans temporaryfilesharing le sous-prefixe duration ?
               * TODO Angie Flo : valider OK                                                    <------------- ???
               * TODO Antoine : rajouter de nouveaux métriques 
                   * metrics.temporaryfilesharing.shares
                   * metrics.temporaryfilesharing.secured
                   * metrics.temporaryfilesharing.singledownload
                   * metrics.temporaryfilesharing.duration.unlimited
                   * metrics.temporaryfilesharing.duration.annual
                   * metrics.temporaryfilesharing.duration.monthly
                   * metrics.temporaryfilesharing.duration.weekly
                   * metrics.temporaryfilesharing.duration.daily
                   * metrics.temporaryfilesharing.created
                   * metrics.temporaryfilesharing.expired
                   * metrics.temporaryfilesharing.purged
                   * metrics.temporaryfilesharing.deleted


       * [Metrics spécifiques aux progiciels de Gestion associative] (Bénévalibre, Galette, Structura)
           * choix du préfixe : ~~asso~~ vs volonteering vs association vs nonprofitmanagement
               * asso ?
               * association
                   * pour : Antoine, Cpm
           * TODO Antoine : trouver une icône (actuellement default.svg) FAIT
           * + Organisation d'évènements (type mobilizon) n'avait pas non plus d'icône (ou pb ?) FAIT
           * logiciel Structura 
               * TODO : trouver le site web de Structura
                   * [https://structura.associatif.online/](https://structura.associatif.online/)
                   * Antoine : FAIT
               * TODO Antoine : le proposer dans Framalibre ? FAIT
               * (enfin déplacé sur ma todolist car j'ai d'autres choses à faire en rapport avec framalibre)
           * metrics.association.count 
           * metrics.association.actions
           * metrics.association.actions.categories
           * metrics.association.projects
           * metrics.association.members
           * metrics.association.members.contributions
           * metrics.association.members.leaders
           * metrics.association.groups
           * metrics.association.accounts
           * metrics.association.accounts.active
           * metrics.association.database.bytes
           * metrics.association.files.bytes




       * [Metrics spécifiques aux services de présentations en ligne] (Strut)
           * choix du préfixe :
               * presentations vs diapositives vs diaporama vs ~~slides~~ vs ~~slideshow~~
               * presentations ?
           * metrics.presentations.count
           * metrics.presentations.diapositives  vs  slides
           * metrics.presentations.accounts
               * champ générique ?
           * metrics.presentations.accounts.active
               * champ générique ?
           * metrics.presentations.database.bytes
               * champ générique ?
           * metrics.presentations.files.bytes
               * champ générique ?
           * TODO : rajouter en metrics génériques ces quatres là (Antoine) et modifier les autres métriques spécifiques FAIT                 <------------- ???






       * Administration de machines virtuelles // virtual machine administrator (Ganeti)
               * Choix du préfixe : vps (virtual private server)
           * metrics.vps.clusters
           * metrics.vps.nodes
           * metrics.vps.encrypted
           * metrics.vps.sent.bytes
           * metrics.vps.received.bytes
       * 

       * 

       * Mesure de statistiques (Dolomon, Matomo, Open Web Analytics)
               * Choix du préfixe : metrics (actuellement)... , stats, statistics, analytics
                   * metrics.metrics.count ?? ça ne me semble pas une bonne idée / possible
               * Exemples de features (ici pour AWstats) [http://www.awstats.org/](http://www.awstats.org/)
           * metrics.stats.logs
           * metrics.stats.
   * 

   * 

       * Outils de monitoring (Healthchecks, Monitorix)
               * Choix du préfixe : monitoring
           * metrics.monitoring.checks
           * metrics.monitoring.




       * Gestionnaire de marques-pages (Shaarli)
       * et Sauvegarde de contenus web (Wallabag) ?
               * Choix du préfixe : bookmark, saveforlater
           * metrics.bookmark.count
           * metrics.bookmark.




***Catégories de Metrics restant mais qui n'ont pas de services sur stats (ordre alphabétique)***

*Gestionnaire de facturation / paiement*

*Gestionnaire de tâches*

*Lettres d'informations (PHPList, wassup)*

*Outils de prise de décision (Loomio, VotAR)*

*Prise de note (Turtl)*

*Serveurs de jeux vidéos (Minetest, Trivabble)*

*Stockage et partage d'albums photos (Piwigo)*

///

gestionnaires de projet (tracim) (alternative à monday) = filesharing comme nextcloud ? \& group ?







# 35e réunion du groupe de travail

### jeudi 17 juin 2021 à 11h15

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes : Antoine (Framasoft - CHATONS), Christian/Cpm (Chapril), Florian (Colibris)



   * question de la persistance des compte-rendus de réunions
       * le pad est un espace temporaire de travail
       * où persister ?
           * page wiki dans le dépôt ChatonsInfos
           * 1 document LibreOffice par réunion
           * 1 document LibreOffice pour toutes les réunions
       * Antoine volontaire avec disponibilité en juillet


   * divers précédents :
       * création d'un schéma explicitant les subs
           * TODO Antoine
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * TODO Cpm


   * revue de [https://stats.chatons.org/](https://stats.chatons.org/) 😍
       * page CHATONS : 
           * **décision d'afficher par défaut les organisations et services « actifs » (sans enddate ou avec enddate future)**
               * ne pas se contenter de regarder si le enddate est vide, comparer à la date du jour
               * plus tard éventuellement, ajout d'un fonction pour voir les autres aussi "le cimetière des chatons" 😆
               * TODO Cpm
       * page générique d'un chaton : 
           * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
               * TODO Cpm
       * page « Statistiques » (fédération) :
           * ajouter un donuts sur les services de paiement
               * TODO Cpm 
       * un jour peut-être :
           * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
               * par exemple pour les types d'inscription (à un service)
           * donuts sur les pays
               * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays
       * pages Uptimes (Federation, Organization, Services)
           * des améliorations à faire
               * TODO Cpm Datatable FAIT
               * TODO Cpm parallélisation
               * TODO Cpm autres liens
               * TODO Cpm ajout boutons « Tout / Indisponible » FAIT
               * TODO Cpm bulle de la liste des mesures FAIT
               * TODO pour le bouton « Indisponible » peut-être ne pas mettre les jaunes, à voir
               * TODO mrflos (pour l'été) : bidouiller la page statsuptime pour utiliser les filtres par état en js datatables
           * vocabulaire :
               * anglais : uptime,  monitoring
               * français : disponibilité des services, supervision, disponibilité, "~~Etats ~~Disponibilité des services mutualisé"
               * avec le recul, « disponibilité des services » colle bien
               * TODO Cpm modifier le libellé du bouton en « Disponibilité des services » 
           * questions de statut manuel vs statut mesuré (page organization)
               * statut manuel seulement
               * statut mesuré seulement
               * les deux
               * un seul combiné des deux
               * discussion :
                   * est-ce que la version manuelle est encore utile ? pertinence du mesuré
                   * se poser la question de ce que cherche l'utilisateur
                   * cas des statuts manuels « en travaux » ou  « fermé »
                   * le statut manuel est plus important que le statut mesuré, respecté l'expression des admins
                   * ne surtout pas afficher les deux
                   * étudier la conjonction
               * TODO Cpm voir pour une version « combinée » avec bulle informative
           * partager avec les membres :
               * ~~forum~~
               * ~~réunion ~~
               * forum + réunion ++++
                   * toujours mentionné « expérimental »
                   * TODO Antoine : message forum + entrée pad réunion FAIT
       * refonte menu propertyCheck et propertyAlerts, Cpm FAIT
       * revue du file d'Ariane, Cpm FAIT
       * Flo :
           * peut être avoir dans résumé des moyennes sans graphes, genre du texte « sur 2020 XXX visiteurs uniques, YYY ips différentes »
               * Cpm : une notion de « tendance » ?
               * Cpm : donner exemple ?
               * TODO Flo, à réfléchir l'enrichissement de texte des graphes toujours en TODO
               * TODO Flo, à réfléchir à des cadres de tendances dans « Résumé » toujours en TODO
           * changer les intitulés « Web » et « Spécifique » par « Graphes de visites web » ou plus court « Graphes Web » et « Statistiques propres aux services » ou plus court « Stats des services » ?
               * Cpm : préciser l'intention
               * Flo : expliquer les items du menu type
               * TODO Cpm : ajouter des bulles
               * TODO Flo : tester le menu métriques auprès de personnes
                   * en cours toujours en TODO
               * TODO réfléchir
               * TODO revoir l'ordre du menu type
                   * générique, web, spécifique : vision technique, pas forcément pertinent
                   * web, générique, spécifique :
                   * générique, spécifique, web : plus intéressant pour l'utilsateur
                       * TODO Cpm mettre cet ordre FAIT
           * TODO Cpm afficher les champs nom et description des métrics dans les diagrammes


   * revue des catégories ([https://stats.chatons.org/category-autres.xhtml)](https://stats.chatons.org/category-autres.xhtml)) :
       * synapse/element
           * Synapse est le serveur Matrix
           * Element est le client web pour un serveur Synapse
           * TODO Flo demander à l'utilisateur de modifier sa fiche properties avec le champ x.modules FAIT
           * TODO Stéphane : corriger, en cours FAIT
       * Drawio
           * TODO Antoine informer Underworld du changement de nom/politique, à surveiller
               * TODO trouver une bonne source d'information
                   * cf Issue sur le git
           * TODO Flo enrichissement de la liste des noms associés FAIT
       * Nextcloud\&OnlyOffice
           * Sans Nuage/ARN
           * TODO Antoine relancer demande  TODO FAIT
           * TODO Sans Nuage FAIT
       * Yakforms
           * Sans Nuage/ARN
           * TODO Flo FAIT
           * TODO Antoine : propager le changement de nom dans le fichier property Framasoft
       * Nextcloud Contact
           * Sans Nuage /ARN
           * TODO Flo rajouter dans catégorie "Carnet d'adresse" FAIT
       * TODO Cpm voir le problème des pictos qui disparaissent
       * categories.properties
           * Proposition (Antoine) : supprimer la catégorie ".saveforlater" et la fusionner avec ".bookmarking"
               * *##Sauvegarde de contenus web (alternative à Pocket, Instapaper, etc.)*
               * categories.saveforlater.name=Sauvegarde de contenus web
               * categories.saveforlater.description=
               * categories.saveforlater.logo=saveforlater.svg
               * categories.saveforlater.softwares=Wallabag
               * Avis ?
                   * comment nommer la catégorie de fusion ?
                   * à réfléchir


   * revue de fichiers properties de membres : [https://stats.chatons.org/chatons-crawl.xhtml](https://stats.chatons.org/chatons-crawl.xhtml)
       * RAS


   * revue des tickets :
       * [https://framagit.org/chatons/chatonsinfos/-/issues/1](https://framagit.org/chatons/chatonsinfos/-/issues/1)
           * Redesign des encarts au dessus des tableaux
           * prévu lorsqu'on aura toutes les informations affichées
           * statut : plus tard
       * [https://framagit.org/chatons/chatonsinfos/-/issues/2](https://framagit.org/chatons/chatonsinfos/-/issues/2)
           * Dans le tableau des services de la fiche organisation des chatons, supprimer la colonne "Organisation"
           * Cpm : ce tableau est une vue mutualisée entre plusieurs pages : organisation, services, catégorie,  logiciel ; l'information est effectivement redondante pour la page organisation, mais ça permet de conserver l'homogénéité de la vue.
           * Cpm : pour gagner de la place, possibilité de ne mettre que le logo de l'organisation et le nom en bulle
           * statut : réfléchir et sinon sera traité par la grande revue visuelle prévue un jour
       * [https://framagit.org/chatons/chatonsinfos/-/issues/4](https://framagit.org/chatons/chatonsinfos/-/issues/4)
           * Penser un nouveau fichier properties dédié aux offres non logicielles
           * statut : priorité aux services utilisateurs donc pertinent mais plus tard
       * [https://framagit.org/chatons/chatonsinfos/-/issues/6](https://framagit.org/chatons/chatonsinfos/-/issues/6)
           * Catégorie : Création de schémas et diagrammes - confusion
               * statut : nouveau
               * « Dans [https://stats.chatons.org/category-creationdeschemasetdiagrammes.xhtml](https://stats.chatons.org/category-creationdeschemasetdiagrammes.xhtml) La catégorie liste des "logiciels" qui n'en sont pas. "diagrams.net" et "draw.io" sont des SERVICES qui utilisent le LOGICIEL "drawio" ([https://github.com/jgraph/drawio)](https://github.com/jgraph/drawio)) exemple : [https://stats.chatons.org/software-drawio.xhtml](https://stats.chatons.org/software-drawio.xhtml) »
           * TODO trouver une bonne source d'information
       * [https://framagit.org/chatons/chatonsinfos/-/issues/7](https://framagit.org/chatons/chatonsinfos/-/issues/7)
           * Proposition de rationalisation "Sondages, Questionnaires et RDV"
           * statut : nouveau
           * a priori, volonté de se mettre à la place des utilisateurs de ne pas avoir trop de propositions hors sujet
           * TODO Antoine répondre TODO




   * revue des merge requests : [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/31](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/31)
           * proposition de remise en forme de la documentation par @labecasse (Chère de Prince)
           * « Je propose de déplacer la documentation essentielle pour la mise en place dans le README. »
           * Avis ?
               * bonne proposition
               * changer le titre « documentation » en « Bien commencer »
           * TODO Antoine : accepter le merge + mettre un petit message de remerciement FAIT
           * TODO Antoine : faire une revue de titre et de plan
           * TODO Antoine : dans la FAQ, conserver la section et mettre une phrase pour envoyer sur le README FAIT
           * TODO Antoine voir si dans FAQ.md il est possible de mettre le lien de README.md en relatif


   * revue du forum : [https://forum.chatons.org/c/collectif/stats-chatons-org/83](https://forum.chatons.org/c/collectif/stats-chatons-org/83)
       * [https://forum.chatons.org/t/service-properties-registration-status/2068/9](https://forum.chatons.org/t/service-properties-registration-status/2068/9)
           * TODO Cpm : dans son cas c'est compatible avec « member » même si c'est un peu spécial, différent niveau de souscription, pas de rapport clientèle avec factures et autres


   * revue des disponibilités des services (uptimes) :
       * Sans-nuage/ARN > Agenda
           * [https://stats.chatons.org/sansnuagearn-agenda-uptimes.xhtml](https://stats.chatons.org/sansnuagearn-agenda-uptimes.xhtml)
               * certificat « Pas après Tue, 10 Jan 2017 10:44:00 GMT »
           * ljf a fait une mise à jour, maintenant c'est bon, FAIT
       * Sans-nuage/ARN > VPS
           * [https://stats.chatons.org/sansnuagearn-vps-uptimes.xhtml](https://stats.chatons.org/sansnuagearn-vps-uptimes.xhtml)
           * SSL certificate problem: unable to get local issuer certificate
           * TODO Cpm contacter admin FAIT
       * Bee-home > Compte messagerie et cloud
           * [https://stats.chatons.org/beehome-comptemessagerieetcloud.xhtml](https://stats.chatons.org/beehome-comptemessagerieetcloud.xhtml)
           * absence d'URL de service
           * TODO Flo demande de compléter l'URL
           * TODO Flo vérifier si c'est un vrai service ou une offre de service
       * Bee-home >Hébergement de site
           * [https://stats.chatons.org/beehome-comptemessagerieetcloud-uptimes.xhtml](https://stats.chatons.org/beehome-comptemessagerieetcloud-uptimes.xhtml)
           * absence d'URL de service
           * TODO Flo demande de compléter l'URL
           * TODO Flo vérifier si c'est un vrai service ou une offre de service
       * Infini > tous les services
           * [https://stats.chatons.org/infini-uptimes.xhtml](https://stats.chatons.org/infini-uptimes.xhtml)
           * coupure réseau en cours
           * TODO attendre le rétablissement, FAIT
       * Sleto > Équipe
           * [https://stats.chatons.org/sleto-equipe-uptimes.xhtml](https://stats.chatons.org/sleto-equipe-uptimes.xhtml)
           * [https://equipe.sleto.net/](https://equipe.sleto.net/)
           * « Dysfonctionnement du site ! »
           * TODO Antoine contacter les admins
       * TODO Cpm rajouter dans la FAQ le cas de PrivateBin UserAgent
           * mon site est indiqué indisponible alors que tout va bien, que vérifier ?


   * avancer avec le collectif sur la complétion des metrics ?
       * metrics spécifiques à chaque service à penser
           * besoin de repasser dessus pour le nommage avant de propager
           * besoin de coder leur affichage pour stats.chatons.org
           * besoin de paramétrer des moulinettes pour les récupérations automatisées de moulinettes


   * ONTOLOGIE
       * ordre des questions à se poser : préfixe, sous-préfixe


       * organization
           * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/30#note\_1011677](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/30#note\_1011677)
           * « pourquoi ne pas ajouter les coordonnées GPS de l'organisation + une adresse ? »
           * avis ?
               * ~~organization.gps~~
               * ~~organization.coordinates~~
               * ~~organization.coordinates.latitude~~
               * ~~organization.coordinates.longitude~~
               * ~~organization.address~~
               * organization.geolocation.latitude
               * organization.geolocation.longitude
               * organization.geolocation.address
           * TODO Antoine : ajouter dans le fichier modèle organization
           * TODO Cpm : ajouter dans StatoolInfos le check


       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
           * nouveaux suite au codage de la génération et des graphiques
               * metrics.http.hits.bots
               * metrics.http.hits.visitors
               * metrics.http.hits.visitors.ipv4
               * metrics.http.hits.visitors.ipv6
               * metrics.http.ip.bots
               * metrics.http.ip.visitors
               * metrics.http.visits.bots
               * metrics.http.visits.humans
               * metrics.http.visitors
               * metrics.http.visitors.ipv4
               * metrics.http.visitors ipv6
               * metrics.http.visitors.bots
               * metrics.http.visitors.humans
               * questions de bots vs humans vs visitors
                   * metrics.http.hits.bots, metrics.http.hits.visitors => metrics.http.hits.humans ?
                   * metrics.http.ip.bots, metrics.http.ip.visitors => metrics.http.ip.humans ?
                   * metrics.http.visitors.bots, metrics.http.visitors.visitors => metrics.http.visitors.humans ?
               * TODO Cpm voir à généraliser human


       * Un jour peut-être : 
           * metrics.ci


       * MAJ de [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties) avec les éléments de la dernière réunion
           * 

       * 

       * Métriques génériques
           * préfixe spécifique au metrics géneriques ?
               * metrics.users.count vs metrics.generic.users.count vs  metrics.cross.users.count vs metrics.default.users.count vs metrics.trans.users.count
               * TODO avis : plutôt pour, avec « generic »
               * problème vu ci-après :
                   * metrics.generic.moderation.* n'a pas de sens
                   * donc le vrai problème ça serait que metrics.accounts, metrics.database manquent d'un vrai préfixe
                   * TODO trouver lequel :
                       * ~~metrics.generic.accounts~~
                       * metrics.application.database.bytes
                       * metrics.general.database.bytes
                       * metrics.various.database.bytes
                       * metrics.service.database.bytes  +
                       * metrics.generic.database.bytes  +
                       * metrics.????.database.bytes
                   * décision d'adopter « metrics.service » sauf pour « metrics.moderation.* »
                       * metrics.service.users
                       * metrics.service.accounts
                       * metrics.service.accounts.active
                       * metrics.service.database.bytes
                       * metrics.service.files.bytes
                       * ~~metrics.storage.database.bytes~~
                       * ~~metrics.storage.files.bytes~~
                       * metrics.moderation.*
                       * ~~metrics.service.moderation~~
           * TODO Antoine : rajouter en metrics génériques ces quatres là
           * + TODO Antoine : màj des métriques spécifiques
               * metrics.generic.accounts  ~~/  accounts.count ~~
               * metrics.generic.accounts.active
               * metrics.generic.database.bytes : OK
               * metrics.generic.files.bytes : OK
               * voir ci-dessus (décision d'adopter « metrics.service » sauf pour « metrics.moderation.* »)
           * Métriques génériques pour la modération ?
               * jusqu'ici, c'est un sous préfixe :
                   * metrics.xxx.moderation.reports
                   * metrics.xxx.moderation.disabledaccounts
                   * metrics.xxx.moderation.silencedaccounts
                   * metrics.xxx.moderation.cancelledaccounts
               * avec le recul, il apparait que ce métric est générique à tous les services :
                   * metrics.moderation.reports
                   * metrics.moderation.sanctions ~~penalties~~
                   * metrics.moderation.disabledaccounts
                   * metrics.moderation.silencedaccounts
                   * metrics.moderation.cancelledaccounts
                   * TODO valider :
                       * Angie : OK
                       * Flo :  OK     
                   * TODO Antoine : les retirer des metrics
                       * FAIT
                   * voir ci-dessus (décision d'adopter « metrics.service » sauf pour « metrics.moderation.* »)
           * préfixe spécifique au metrics géneriques ?
               * metrics.users.count vs metrics.generic.users.count vs  metrics.cross.users.count vs metrics.default.users.count
               * TODO avis : 
                   * voir ci-dessus (décision d'adopter « metrics.service » sauf pour « metrics.moderation.* »)
               * convergence :
                   * metrics.service.users
                   * metrics.service.accounts
                   * metrics.service.accounts.active
                   * metrics.service.database.bytes
                   * metrics.service.files.bytes
                   * ~~metrics.storage.database.bytes~~
                   * ~~metrics.storage.files.bytes~~
                   * metrics.moderation.*
                   * ~~metrics.service.moderation~~
               * TODO Antoine : propager dans le fichier metrics.properties
           * métriques génériques de durée de vie
               * comme pour pics et temporary files sharing
                   * exemple : pad, calc, presentation...
               * TODO Avis? :
           * métriques génériques pour les services fédérés
               * comme pour videos ou social networks
                   * exemple : funkwhale, events,
                   * peut-être d'autres arriveront
               * Avis? :


       * [Metrics spécifiques aux services de partage temporaire de fichiers] 
           * Antoine : ça a été déjà fait ligne #2522 mais j'ai fait un mauvais control+f sur le git
               * [https://mypads.framapad.org/p/gt-stats-chatons-org-8h6ly7dn#L2522](https://mypads.framapad.org/p/gt-stats-chatons-org-8h6ly7dn#L2522)
               * ligne 125 du git
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties#L125](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties#L125)
               * // pendant la période
           * ~~metrics.drop.files.count~~
           * metrics.drop.shares
           * metrics.drop.secured / .password 
           * metrics.drop.singledownload
           * metrics.drop.duration.unlimited
           * metrics.drop.duration.annual
           * metrics.drop.duration.monthly
           * metrics.drop.duration.weekly
           * metrics.drop.duration.daily
           * metrics.drop.created
           * metrics.drop.expired
           * metrics.drop.purged
           * metrics.drop.deleted
           * metrics.drop.accounts
           * ~~metrics.drop.database.bytes~~
           * ~~metrics.drop.files.bytes~~
           * questions :
               * temporaryfilesharing vs drop ?
                   * drop fait penser à temporaire
                   * drop fait penser à dropbox donc pas si temporaire que ça
               * rajouter dans temporaryfilesharing le sous-prefixe duration ?
               * TODO Angie Flo : valider OK                                                    <------------- ???
               * TODO Antoine : rajouter de nouveaux métriques 
                   * metrics.temporaryfilesharing.shares
                   * metrics.temporaryfilesharing.secured
                   * metrics.temporaryfilesharing.singledownload
                   * metrics.temporaryfilesharing.duration.unlimited
                   * metrics.temporaryfilesharing.duration.annual
                   * metrics.temporaryfilesharing.duration.monthly
                   * metrics.temporaryfilesharing.duration.weekly
                   * metrics.temporaryfilesharing.duration.daily
                   * metrics.temporaryfilesharing.created
                   * metrics.temporaryfilesharing.expired
                   * metrics.temporaryfilesharing.purged
                   * metrics.temporaryfilesharing.deleted


       * [Metrics spécifiques aux progiciels de Gestion associative] (Bénévalibre, Galette, Structura)
           * choix du préfixe : ~~asso~~ vs volonteering vs association vs nonprofitmanagement
               * asso ?
               * association
                   * pour : Antoine, Cpm
           * TODO Antoine : trouver une icône (actuellement default.svg) FAIT
           * + Organisation d'évènements (type mobilizon) n'avait pas non plus d'icône (ou pb ?) FAIT
           * logiciel Structura 
               * TODO : trouver le site web de Structura
                   * [https://structura.associatif.online/](https://structura.associatif.online/)
                   * Antoine : FAIT
               * TODO Antoine : le proposer dans Framalibre ? FAIT
               * (enfin déplacé sur ma todolist car j'ai d'autres choses à faire en rapport avec framalibre)
           * metrics.association.count 
           * metrics.association.actions
           * metrics.association.actions.categories
           * metrics.association.projects
           * metrics.association.members
           * metrics.association.members.contributions
           * metrics.association.members.leaders
           * metrics.association.groups
           * metrics.association.accounts
           * metrics.association.accounts.active
           * metrics.association.database.bytes
           * metrics.association.files.bytes




       * [Metrics spécifiques aux services de présentations en ligne] (Strut)
           * choix du préfixe :
               * presentations vs diapositives vs diaporama vs ~~slides~~ vs ~~slideshow~~
               * presentations ?
           * metrics.presentations.count
           * metrics.presentations.diapositives  vs  slides
           * metrics.presentations.accounts
               * champ générique ?
           * metrics.presentations.accounts.active
               * champ générique ?
           * metrics.presentations.database.bytes
               * champ générique ?
           * metrics.presentations.files.bytes
               * champ générique ?
           * TODO : rajouter en metrics génériques ces quatres là (Antoine) et modifier les autres métriques spécifiques FAIT                 <------------- ???






       * Administration de machines virtuelles // virtual machine administrator (Ganeti)
               * Choix du préfixe : vps (virtual private server)
           * metrics.vps.clusters
           * metrics.vps.nodes
           * metrics.vps.encrypted
           * metrics.vps.sent.bytes
           * metrics.vps.received.bytes
       * 

       * 

       * Mesure de statistiques (Dolomon, Matomo, Open Web Analytics)
               * Choix du préfixe : metrics (actuellement)... , stats, statistics, analytics
                   * metrics.metrics.count ?? ça ne me semble pas une bonne idée / possible
               * Exemples de features (ici pour AWstats) [http://www.awstats.org/](http://www.awstats.org/)
           * metrics.stats.logs
           * metrics.stats.
   * 

   * 

       * Outils de monitoring (Healthchecks, Monitorix)
               * Choix du préfixe : monitoring
           * metrics.monitoring.checks
           * metrics.monitoring.




       * Gestionnaire de marques-pages (Shaarli)
       * et Sauvegarde de contenus web (Wallabag) ?
               * Choix du préfixe : bookmark, saveforlater
           * metrics.bookmark.count
           * metrics.bookmark.




***Catégories de Metrics restant mais qui n'ont pas de services sur stats (ordre alphabétique)***

*Gestionnaire de facturation / paiement*

*Gestionnaire de tâches*

*Lettres d'informations (PHPList, wassup)*

*Outils de prise de décision (Loomio, VotAR)*

*Prise de note (Turtl)*

*Serveurs de jeux vidéos (Minetest, Trivabble)*

*Stockage et partage d'albums photos (Piwigo)*

///

gestionnaires de projet (tracim) (alternative à monday) = filesharing comme nextcloud ? \& group ?





# 36e réunion du groupe de travail

### jeudi 24 juin 2021 à 11h15

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes : Antoine (CHATONS – Framasoft), Christian/Cpm (Chapril)



   * question de la persistance des compte-rendus de réunions
       * le pad est un espace temporaire de travail
           * où persister ?
           * page wiki dans le dépôt ChatonsInfos
           * ~~1 document LibreOffice par réunion~~
           * 1 document LibreOffice pour toutes les réunions
           * scinder en plusieurs pad
       * TODO Antoine volontaire avec disponibilité en juillet
           * jusqu'au mois de mai


   * divers précédents :
       * création d'un schéma explicitant les subs
           * TODO Antoine
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * TODO Cpm


   * revue de [https://stats.chatons.org/](https://stats.chatons.org/) 😍
       * page CHATONS : 
           * **décision d'afficher par défaut les organisations et services « actifs » (sans enddate ou avec enddate future)**
               * ne pas se contenter de regarder si le enddate est vide, comparer à la date du jour
               * plus tard éventuellement, ajout d'un fonction pour voir les autres aussi "le cimetière des chatons" 😆
               * TODO Cpm
       * page générique d'un chaton : 
           * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
               * TODO Cpm
       * page « Statistiques » (fédération) :
           * ajouter un donuts sur les services de paiement
               * TODO Cpm 
       * un jour peut-être :
           * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
               * par exemple pour les types d'inscription (à un service)
           * donuts sur les pays
               * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays
       * pages Uptimes (Federation, Organization, Services)
           * des améliorations à faire
               * TODO Cpm parallélisation
               * TODO Cpm autres liens
               * TODO pour le bouton « Indisponible » peut-être ne pas mettre les jaunes, à voir
               * TODO mrflos (pour l'été) : bidouiller la page statsuptime pour utiliser les filtres par état en js datatables
           * vocabulaire :
               * TODO Cpm modifier le libellé du bouton en « Disponibilité des services »   FAIT
           * questions de statut manuel vs statut mesuré (page organization)
               * statut manuel seulement
               * statut mesuré seulement
               * les deux
               * un seul combiné des deux
               * discussion :
                   * est-ce que la version manuelle est encore utile ? pertinence du mesuré
                   * se poser la question de ce que cherche l'utilisateur
                   * cas des statuts manuels « en travaux » ou  « fermé »
                   * le statut manuel est plus important que le statut mesuré, respecté l'expression des admins
                   * ne surtout pas afficher les deux
                   * étudier la conjonction
               * TODO Cpm voir pour une version « combinée » avec bulle informative
           * fonction d'export du journal des uptimes
               * TODO Cpm format CSV FAIT
               * TODO Cpm format ODS FAIT
       * Flo :
           * peut être avoir dans résumé des moyennes sans graphes, genre du texte « sur 2020 XXX visiteurs uniques, YYY ips différentes »
               * Cpm : une notion de « tendance » ?
               * Cpm : donner exemple ?
               * TODO Flo, à réfléchir l'enrichissement de texte des graphes toujours en TODO
               * TODO Flo, à réfléchir à des cadres de tendances dans « Résumé » toujours en TODO
           * changer les intitulés « Web » et « Spécifique » par « Graphes de visites web » ou plus court « Graphes Web » et « Statistiques propres aux services » ou plus court « Stats des services » ?
               * Cpm : préciser l'intention
               * Flo : expliquer les items du menu type
               * TODO Cpm : ajouter des bulles
               * TODO Flo : tester le menu métriques auprès de personnes
                   * en cours toujours en TODO
               * TODO réfléchir
           * TODO Cpm afficher les champs nom et description des métrics dans les diagrammes
       * page jouranl de crawl
           * ajout d'un bouton de filtrage
               * TODO Cpm


   * revue des catégories ([https://stats.chatons.org/category-autres.xhtml)](https://stats.chatons.org/category-autres.xhtml)) :
       * Drawio
           * TODO éclaircir la situation (trouver une bonne source d'information)
           * TODO Antoine informer Underworld du changement de nom/politique, à surveiller
               * cf Issue sur le git
       * Framaforms -> Yakforms :
           * Sans Nuage/ARN
           * TODO Flo FAIT
           * TODO Antoine : propager le changement de nom dans le fichier property Framasoft
       * Synape
           * Exarius
           * Faute de frappe
           * TODO Antoine envoyer un message pour leur indiquer la coquille
       * Dokuwiki
           * question de savoir si c'est un service au sens CHATONS
           * Bastet
               * [https://wiki.parinux.org/](https://wiki.parinux.org/)
               * réponse déjà donnée mais reste interrogation
           * Hadoly
               * [https://wiki.hadoly.fr/](https://wiki.hadoly.fr/)
               * TODO Antoine vérifier si c'est un service au sens CHATONS
               * TODO Antoine communiquer si pas le cas
           * Nomagic
               * [https://wiki.nomagic.uk/doku.php?id=en:start](https://wiki.nomagic.uk/doku.php?id=en:start)
               * TODO Antoine vérifier si c'est un service au sens CHATONS
               * TODO Antoine communiquer si pas le cas
       * TODO Cpm voir le problème des pictos qui disparaissent FAIT
       * categories.properties
           * Proposition (Antoine) : supprimer la catégorie ".saveforlater" et la fusionner avec ".bookmarking"
               * *##Sauvegarde de contenus web (alternative à Pocket, Instapaper, etc.)*
               * categories.saveforlater.name=Sauvegarde de contenus web
               * categories.saveforlater.description=
               * categories.saveforlater.logo=saveforlater.svg
               * categories.saveforlater.softwares=Wallabag
               * Avis ?
                   * comment nommer la catégorie de fusion ?
                   * à réfléchir




   * revue des fichiers properties de membres :
       * passer en revue :
           *  [https://stats.chatons.org/chatons-crawl.xhtml](https://stats.chatons.org/chatons-crawl.xhtml)
           * [https://stats.chatons.org/chatons-propertyalerts.xhtml](https://stats.chatons.org/chatons-propertyalerts.xhtml)
       * création des fichiers par défaut des nouveaux membres
           * TODO Antoine
       * communication vers les nouveaux membres pour qu'ils créent eux-mêmes
           * TODO Antoine
       * [https://stats.chatons.org/exarius.xhtml](https://stats.chatons.org/exarius.xhtml)
           * Nouveau !
               * Fait d'eux-même
       * [https://stats.chatons.org/.well-known/chatonsinfos/chatons.properties](https://stats.chatons.org/.well-known/chatonsinfos/chatons.properties)
           * [https://www.chatons.org/logo\_chatons\_v2.png](https://www.chatons.org/logo\_chatons\_v2.png)      URLNOTFOUND
           * une conséquence de la mise en production de la v2
           * TODO Antoine


   * revue des tickets :
       * [https://framagit.org/chatons/chatonsinfos/-/issues/1](https://framagit.org/chatons/chatonsinfos/-/issues/1)
           * Redesign des encarts au dessus des tableaux
           * prévu lorsqu'on aura toutes les informations affichées
           * statut : plus tard
       * [https://framagit.org/chatons/chatonsinfos/-/issues/2](https://framagit.org/chatons/chatonsinfos/-/issues/2)
           * Dans le tableau des services de la fiche organisation des chatons, supprimer la colonne "Organisation"
           * Cpm : ce tableau est une vue mutualisée entre plusieurs pages : organisation, services, catégorie,  logiciel ; l'information est effectivement redondante pour la page organisation, mais ça permet de conserver l'homogénéité de la vue.
           * Cpm : pour gagner de la place, possibilité de ne mettre que le logo de l'organisation et le nom en bulle
           * statut : réfléchir et sinon sera traité par la grande revue visuelle prévue un jour
       * [https://framagit.org/chatons/chatonsinfos/-/issues/4](https://framagit.org/chatons/chatonsinfos/-/issues/4)
           * Penser un nouveau fichier properties dédié aux offres non logicielles
           * statut : priorité aux services utilisateurs donc pertinent mais plus tard
       * [https://framagit.org/chatons/chatonsinfos/-/issues/6](https://framagit.org/chatons/chatonsinfos/-/issues/6)
           * Catégorie : Création de schémas et diagrammes - confusion
               * statut : nouveau
               * « Dans [https://stats.chatons.org/category-creationdeschemasetdiagrammes.xhtml](https://stats.chatons.org/category-creationdeschemasetdiagrammes.xhtml) La catégorie liste des "logiciels" qui n'en sont pas. "diagrams.net" et "draw.io" sont des SERVICES qui utilisent le LOGICIEL "drawio" ([https://github.com/jgraph/drawio)](https://github.com/jgraph/drawio)) exemple : [https://stats.chatons.org/software-drawio.xhtml](https://stats.chatons.org/software-drawio.xhtml) »
           * TODO trouver une bonne source d'information
       * [https://framagit.org/chatons/chatonsinfos/-/issues/7](https://framagit.org/chatons/chatonsinfos/-/issues/7)
           * Proposition de rationalisation "Sondages, Questionnaires et RDV"
           * statut : nouveau
           * a priori, volonté de se mettre à la place des utilisateurs de ne pas avoir trop de propositions hors sujet
           * TODO Antoine répondre
       * [https://framagit.org/chatons/chatonsinfos/-/issues/9](https://framagit.org/chatons/chatonsinfos/-/issues/9)
           * kaihuri - Propriétés en alertes
           * Cpm : c'est un bug à corriger, FAIT




   * revue des merge requests : [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/32](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/32)
           * Update paquerette.properties 
           * TODO Cpm merger FAIT
           * TODO Antoine annonce forum :
               * grouper avec Kaihuri ?
               * + Exarius ?
               * avec Kaihuri ça fait 30 participants soit 1/3, suggestion de mettre le premier camembert de la page statistiques
               * message de jylebleu : [https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/33](https://forum.chatons.org/t/appel-a-chaton-volontaire-pour-fournir-leur-url-de-fichier-organization-properties/1706/33)
       * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/33](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/33)
           * Ajout modèles BBB, Rocket.Chat, Wordpress 
           * TODO Antoine vérifier et merger
       * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/34](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/34)
           * Update chatons.properties, add kaihuri  (keskonfai)
           * TODO Cpm merger FAIT
           * TODO Antoine annonce forum, avec Paquerette ça fait 30 participants soit 1/3
               * grouper avec Paquerette ?


   * revue du forum : [https://forum.chatons.org/c/collectif/stats-chatons-org/83](https://forum.chatons.org/c/collectif/stats-chatons-org/83)
       * [https://forum.chatons.org/t/service-properties-registration-status/2068/9](https://forum.chatons.org/t/service-properties-registration-status/2068/9)
           * TODO Cpm : dans son cas c'est compatible avec « member » même si c'est un peu spécial, différent niveau de souscription, pas de rapport clientèle avec factures et autres


   * revue des disponibilités des services (uptimes) :
       * faire revue de [https://stats.chatons.org/chatons-uptimes.xhtml](https://stats.chatons.org/chatons-uptimes.xhtml)
       * Sans-nuage/ARN > Agenda
           * [https://stats.chatons.org/sansnuagearn-agenda-uptimes.xhtml](https://stats.chatons.org/sansnuagearn-agenda-uptimes.xhtml)
               * certificat « Pas après Tue, 10 Jan 2017 10:44:00 GMT »
           * ljf a fait une mise à jour, maintenant c'est bon, FAIT
       * Sans-nuage/ARN > VPS
           * [https://stats.chatons.org/sansnuagearn-vps-uptimes.xhtml](https://stats.chatons.org/sansnuagearn-vps-uptimes.xhtml)
           * SSL certificate problem: unable to get local issuer certificate
           * TODO Cpm contacter admin FAIT
           * TODO est-ce un service au sens CHATONS ?
       * Bee-home > Compte messagerie et cloud
           * [https://stats.chatons.org/beehome-comptemessagerieetcloud.xhtml](https://stats.chatons.org/beehome-comptemessagerieetcloud.xhtml)
           * absence d'URL de service
           * TODO Flo demande de compléter l'URL
           * TODO Flo vérifier si c'est un vrai service ou une offre de service
           * déméganement perso  en cours, délai de réactivité à prévoir
       * Bee-home >Hébergement de site
           * [https://stats.chatons.org/beehome-comptemessagerieetcloud-uptimes.xhtml](https://stats.chatons.org/beehome-comptemessagerieetcloud-uptimes.xhtml)
           * absence d'URL de service
           * TODO Flo demande de compléter l'URL
           * TODO Flo vérifier si c'est un vrai service ou une offre de service
       * Infini > tous les services
           * [https://stats.chatons.org/infini-uptimes.xhtml](https://stats.chatons.org/infini-uptimes.xhtml)
           * coupure réseau en cours
           * TODO attendre le rétablissement, FAIT
       * Sleto > Équipe
           * [https://stats.chatons.org/sleto-equipe-uptimes.xhtml](https://stats.chatons.org/sleto-equipe-uptimes.xhtml)
           * [https://equipe.sleto.net/](https://equipe.sleto.net/)
           * « Dysfonctionnement du site ! »
           * ~~TODO Antoine contacter les admins~~
           * réparé de lui-même
           * FAIT
       * UNDERWORLD > Etherpad
           * [https://stats.chatons.org/underworld-etherpad-uptimes.xhtml](https://stats.chatons.org/underworld-etherpad-uptimes.xhtml)
           * 502 Bad Gateway
           * TODO Antoine contacter 
       * rajouter dans la FAQ le cas de PrivateBin UserAgent
           * TODO Cpm  FAIT


   * avancer avec le collectif sur la complétion des metrics ?
       * metrics spécifiques à chaque service à penser
           * besoin de repasser dessus pour le nommage avant de propager
           * besoin de coder leur affichage pour stats.chatons.org
           * besoin de paramétrer des moulinettes pour les récupérations automatisées de moulinettes


   * ONTOLOGIE
       * ordre des questions à se poser : préfixe, sous-préfixe


       * organization
           * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/30#note\_1011677](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/30#note\_1011677)
           * « pourquoi ne pas ajouter les coordonnées GPS de l'organisation + une adresse ? »
           * décision de le faire :
               * organization.geolocation.latitude
                   * quel formats ?
                   * 15°24'15N  vs  15° 10,234'N  vs  15,23456
               * organization.geolocation.longitude
                   * quel formats ?
                   * 30°10'3E  vs  30° 23,456'O  vs  -30,67890
               * organization.geolocation.address
           * TODO Antoine : ajouter dans le fichier modèle organization
           * TODO Cpm : ajouter dans StatoolInfos le check, FAIT


       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
           * questions de vocabulaire :  bots vs humans vs visitors vs people
               * besoin de différencier bots et non bots, avec quel mot ? Humans ? People ?
                   * visitors = bots + humans    vs    visitors = bots + people
               * jusqu'ici, visitor est parfois utilisé comme humans ou comme humans+bots, faut-il renommer les métriques déjà existants et utilisant visitor pour human ?
                   * metrics.http.hits.bots, metrics.http.hits.visitors => metrics.http.hits.humans ?
                   * metrics.http.ip.bots, metrics.http.ip.visitors => metrics.http.ip.humans ?
                   * metrics.http.visitors.bots, metrics.http.visitors.visitors => metrics.http.visitors.humans ?
               * TODO Cpm voir à généraliser human
           * nouveaux suite au codage de la génération et des graphiques
               * metrics.http.hits.visitors           -> metrics.http.hits.humans
               * metrics.http.hits.visitors.ipv4   -> metrics.http.hits.humans.ipv4
               * metrics.http.hits.visitors.ipv6   ->  metrics.http.hits.humans.ipv6
               * metrics.http.ip.bots
               * metrics.http.ip.visitors             -> metrics.http.ip.humans
               * metrics.http.visits.bots
               * metrics.http.visits.humans
               * metrics.http.visitors
               * metrics.http.visitors.ipv4
               * metrics.http.visitors ipv6
               * metrics.http.visitors.bots
               * metrics.http.visitors.humans


       * Un jour peut-être : 
           * metrics.ci


       * MAJ de [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties) avec les éléments de la dernière réunion
           * 

       * 

       * Métriques génériques
           * préfixe spécifique au metrics géneriques ?
               * metrics.users.count vs metrics.generic.users.count vs  metrics.cross.users.count vs metrics.default.users.count vs metrics.trans.users.count
               * TODO avis : plutôt pour, avec « generic »
               * problème vu ci-après :
                   * metrics.generic.moderation.* n'a pas de sens
                   * donc le vrai problème ça serait que metrics.accounts, metrics.database manquent d'un vrai préfixe
                   * TODO trouver lequel :
                       * ~~metrics.generic.accounts~~
                       * metrics.application.database.bytes
                       * metrics.general.database.bytes
                       * metrics.various.database.bytes
                       * metrics.service.database.bytes  +
                       * metrics.generic.database.bytes  +
                       * metrics.????.database.bytes
                   * décision d'adopter « metrics.service » sauf pour « metrics.moderation.* »
                       * metrics.service.users
                       * metrics.service.accounts
                       * metrics.service.accounts.active
                       * metrics.service.database.bytes
                       * metrics.service.files.bytes
                       * ~~metrics.storage.database.bytes~~
                       * ~~metrics.storage.files.bytes~~
                       * metrics.moderation.*
                       * ~~metrics.service.moderation~~
           * TODO Antoine : rajouter en metrics génériques ces quatres là
           * + TODO Antoine : màj des métriques spécifiques
               * metrics.generic.accounts  ~~/  accounts.count ~~
               * metrics.generic.accounts.active
               * metrics.generic.database.bytes : OK
               * metrics.generic.files.bytes : OK
               * voir ci-dessus (décision d'adopter « metrics.service » sauf pour « metrics.moderation.* »)
           * Métriques génériques pour la modération ?
               * jusqu'ici, c'est un sous préfixe :
                   * metrics.xxx.moderation.reports
                   * metrics.xxx.moderation.disabledaccounts
                   * metrics.xxx.moderation.silencedaccounts
                   * metrics.xxx.moderation.cancelledaccounts
               * avec le recul, il apparait que ce métric est générique à tous les services :
                   * metrics.moderation.reports
                   * metrics.moderation.sanctions ~~penalties~~
                   * metrics.moderation.disabledaccounts
                   * metrics.moderation.silencedaccounts
                   * metrics.moderation.cancelledaccounts
                   * TODO valider :
                       * Angie : OK
                       * Flo :  OK     
                   * TODO Antoine : les retirer des metrics
                       * FAIT
                   * voir ci-dessus (décision d'adopter « metrics.service » sauf pour « metrics.moderation.* »)
           * préfixe spécifique au metrics géneriques ?
               * metrics.users.count vs metrics.generic.users.count vs  metrics.cross.users.count vs metrics.default.users.count
               * TODO avis : 
                   * voir ci-dessus (décision d'adopter « metrics.service » sauf pour « metrics.moderation.* »)
               * convergence :
                   * metrics.service.users
                   * metrics.service.accounts
                   * metrics.service.accounts.active
                   * metrics.service.database.bytes
                   * metrics.service.files.bytes
                   * ~~metrics.storage.database.bytes~~
                   * ~~metrics.storage.files.bytes~~
                   * metrics.moderation.*
                   * ~~metrics.service.moderation~~
               * TODO Antoine : propager dans le fichier metrics.properties
           * métriques génériques de durée de vie
               * comme pour pics et temporary files sharing
                   * exemple : pad, calc, presentation...
               * TODO Avis? :
           * métriques génériques pour les services fédérés
               * comme pour videos ou social networks
                   * exemple : funkwhale, events,
                   * peut-être d'autres arriveront
               * Avis? :


       * [Metrics spécifiques aux services de partage temporaire de fichiers] 
           * Antoine : ça a été déjà fait ligne #2522 mais j'ai fait un mauvais control+f sur le git
               * [https://mypads.framapad.org/p/gt-stats-chatons-org-8h6ly7dn#L2522](https://mypads.framapad.org/p/gt-stats-chatons-org-8h6ly7dn#L2522)
               * ligne 125 du git
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties#L125](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties#L125)
               * // pendant la période
           * ~~metrics.drop.files.count~~
           * metrics.drop.shares
           * metrics.drop.secured / .password 
           * metrics.drop.singledownload
           * metrics.drop.duration.unlimited
           * metrics.drop.duration.annual
           * metrics.drop.duration.monthly
           * metrics.drop.duration.weekly
           * metrics.drop.duration.daily
           * metrics.drop.created
           * metrics.drop.expired
           * metrics.drop.purged
           * metrics.drop.deleted
           * metrics.drop.accounts
           * ~~metrics.drop.database.bytes~~
           * ~~metrics.drop.files.bytes~~
           * questions :
               * temporaryfilesharing vs drop ?
                   * drop fait penser à temporaire
                   * drop fait penser à dropbox donc pas si temporaire que ça
               * rajouter dans temporaryfilesharing le sous-prefixe duration ?
               * TODO Angie Flo : valider OK                                                    <------------- ???
               * TODO Antoine : rajouter de nouveaux métriques 
                   * metrics.temporaryfilesharing.shares
                   * metrics.temporaryfilesharing.secured
                   * metrics.temporaryfilesharing.singledownload
                   * metrics.temporaryfilesharing.duration.unlimited
                   * metrics.temporaryfilesharing.duration.annual
                   * metrics.temporaryfilesharing.duration.monthly
                   * metrics.temporaryfilesharing.duration.weekly
                   * metrics.temporaryfilesharing.duration.daily
                   * metrics.temporaryfilesharing.created
                   * metrics.temporaryfilesharing.expired
                   * metrics.temporaryfilesharing.purged
                   * metrics.temporaryfilesharing.deleted


       * [Metrics spécifiques aux progiciels de Gestion associative] (Bénévalibre, Galette, Structura)
           * choix du préfixe : ~~asso~~ vs volonteering vs association vs nonprofitmanagement
               * asso ?
               * association
                   * pour : Antoine, Cpm
           * TODO Antoine : trouver une icône (actuellement default.svg) FAIT
           * + Organisation d'évènements (type mobilizon) n'avait pas non plus d'icône (ou pb ?) FAIT
           * logiciel Structura 
               * TODO : trouver le site web de Structura
                   * [https://structura.associatif.online/](https://structura.associatif.online/)
                   * Antoine : FAIT
               * TODO Antoine : le proposer dans Framalibre ? FAIT
               * (enfin déplacé sur ma todolist car j'ai d'autres choses à faire en rapport avec framalibre)
           * metrics.association.count 
           * metrics.association.actions
           * metrics.association.actions.categories
           * metrics.association.projects
           * metrics.association.members
           * metrics.association.members.contributions
           * metrics.association.members.leaders
           * metrics.association.groups
           * metrics.association.accounts
           * metrics.association.accounts.active
           * metrics.association.database.bytes
           * metrics.association.files.bytes




       * [Metrics spécifiques aux services de présentations en ligne] (Strut)
           * choix du préfixe :
               * presentations vs diapositives vs diaporama vs ~~slides~~ vs ~~slideshow~~
               * presentations ?
           * metrics.presentations.count
           * metrics.presentations.diapositives  vs  slides
           * metrics.presentations.accounts
               * champ générique ?
           * metrics.presentations.accounts.active
               * champ générique ?
           * metrics.presentations.database.bytes
               * champ générique ?
           * metrics.presentations.files.bytes
               * champ générique ?
           * TODO : rajouter en metrics génériques ces quatres là (Antoine) et modifier les autres métriques spécifiques FAIT                 <------------- ???






       * Administration de machines virtuelles // virtual machine administrator (Ganeti)
               * Choix du préfixe : vps (virtual private server)
           * metrics.vps.clusters
           * metrics.vps.nodes
           * metrics.vps.encrypted
           * metrics.vps.sent.bytes
           * metrics.vps.received.bytes
       * 

       * 

       * Mesure de statistiques (Dolomon, Matomo, Open Web Analytics)
               * Choix du préfixe : metrics (actuellement)... , stats, statistics, analytics
                   * metrics.metrics.count ?? ça ne me semble pas une bonne idée / possible
               * Exemples de features (ici pour AWstats) [http://www.awstats.org/](http://www.awstats.org/)
           * metrics.stats.logs
           * metrics.stats.
   * 

   * 

       * Outils de monitoring (Healthchecks, Monitorix)
               * Choix du préfixe : monitoring
           * metrics.monitoring.checks
           * metrics.monitoring.




       * Gestionnaire de marques-pages (Shaarli)
       * et Sauvegarde de contenus web (Wallabag) ?
               * Choix du préfixe : bookmark, saveforlater
           * metrics.bookmark.count
           * metrics.bookmark.




***Catégories de Metrics restant mais qui n'ont pas de services sur stats (ordre alphabétique)***

*Gestionnaire de facturation / paiement*

*Gestionnaire de tâches*

*Lettres d'informations (PHPList, wassup)*

*Outils de prise de décision (Loomio, VotAR)*

*Prise de note (Turtl)*

*Serveurs de jeux vidéos (Minetest, Trivabble)*

*Stockage et partage d'albums photos (Piwigo)*

///

gestionnaires de projet (tracim) (alternative à monday) = filesharing comme nextcloud ? \& group ?

