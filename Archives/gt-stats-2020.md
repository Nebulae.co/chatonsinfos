# Groupe de travail stats.chatons.org



**Objectif du groupe de travail :**

**mettre en place un outil qui permettra de connaître le nombre d’utilisateur⋅ices des services proposés par chaque structure membre du collectif**. La future page stats.chatons.org sera alimentée par des informations fournies par les structures membres du collectif.

   * réfléchir à la solution la plus adaptée permettant d’alimenter la page stats.chatons.org
   * identifier les données à récupérer auprès de chaque structure.




# Première réunion du groupe de travail

### jeudi 10 septembre à 20h

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes : Angie, Flo, Cpm.



**Objectifs**

   * identifier ce qu'il y a à faire
   * quelle méthodologie adopter ?
       * Utiliser un protocole existant ? Lequel ? Faire le nôtre ?
       * Définir le modèle
   * quel calendrier ?


**Liste des participants**

   * Angie (Framasoft)
   * Christian (Cpm) – Chapril
   * mrflos (Colibris)


**Liste des non-participants**

   * nflqt (Syntax) : manque de temps mais aussi de compétence dans le domaine


**Vous le voyez comment vous stats.chatons.org ?**



Christian (Cpm) :

   * ServiceInfos à l'air un standard sympa même si un peu compliqué et abandonné.
   * L'outil de FFDN ne semble pas directement adaptable.
       * [https://db.ffdn.org/](https://db.ffdn.org/)
       * [https://code.ffdn.org/ffdn/ffdn-db](https://code.ffdn.org/ffdn/ffdn-db)
   * Pour le Chapril, je suis en train de mettre au point un outil/protocole nommé StatoolInfos.
   * StatoolInfos reprend tous les concepts du moment mais dans une vue de simplicité :
       * un fichier public téléchargeable pour décrire un chaton :
           * [https://www.chapril.org/.well-known/statoolinfos/chapril.properties](https://www.chapril.org/.well-known/statoolinfos/chapril.properties)
           * contient une liste de liens pour télécharger 1 fichier par service
       * un fichier public téléchargeable par service :
           * [https://www.chapril.org/.well-known/statoolinfos/datechaprilorg.properties](https://www.chapril.org/.well-known/statoolinfos/datechaprilorg.properties)
       * un fichier public téléchargeable pour le CHATONS
           * contient une liste de liens pour télécharger 1 fichier par membre
       * format du fichier : properties (clé/valeur)
           * facile à lire humainement
           * facile à lire (parser) informatiquement
           * encourage à avoir des données simples
           * proposition de convention :
               * des sections définies par le chemin des clés (service.description=bla bla)
               * [https://forge.devinsy.fr/cpm/statoolinfos](https://forge.devinsy.fr/cpm/statoolinfos)
       * des moulinettes pour :
           * récupérer (crawl) les fichiers ;
           * produire des pages HTML statiques (htmlize), 1 pour chaque chaton, 1 par service de chatons
           * aller chercher des stats et alimenter les fichiers publics
   * Proposition de démonstration complète lorsque l'avancement de l'outil le permettra (d'ici 1 mois)
   * [https://statool.agir.april.org/](https://statool.agir.april.org/)


mrflos : 

   * entre chatons : des stats détaillées sur chaque service
   * pour le grand public : des stats générales : nombre de chatons, nombre de personnes impliqués, nombre de gigaoctets libérés, cumul des chiffres (ex : 120.234 pads , 12.543 wikis, etc..)
   * pas indispensable d'avoir les chiffres de tout le monde


Modèle : format / où est le fichier / 

Liste de champs du modèle (à définir entre nous)

Possibilité d'ajouter des champs à la liste avec de nouveaux contributeurs

Intéressant car outil utilisable pour d'autres activités du chaton

Possibilité d'automatiser la récupération des données (metrics)

stats.chatons.org : site web complet avec toutes les données

+ sur chatons.org et entraide.chatons.org : chiffres synthétiques pour communiquer (avec renvoi vers stats.chatons.org pour tout voir)



Sur stats : pages pour chaque logiciel avec regroupement par type d'outil



Cpm nous informe dès qu'il veut avoir nos retours pour le rendre plus beau, plus utilisable

MrFlos intéressé pour avancer sur la mise en forme des données pour le site web



Définition des champs à faire

affiner la liste [https://forge.devinsy.fr/cpm/statoolinfos](https://forge.devinsy.fr/cpm/statoolinfos)

1 petite réunion chaque semaine

annoncer sur le forum la 1ère réunion dédiée à la réflexion sur le nom des champs

mais besoin d'expliciter dans quel langage le modèle et donc le principe de l'expérimentation





# Seconde réunion du groupe de travail

### jeudi 17 septembre à 11h

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes : Angie, Flo, Cpm.



Cpm propose 2 nouveaux fichiers dans le dépôt ChatonsInfos ([https://framagit.org/chatons/chatonsinfos)](https://framagit.org/chatons/chatonsinfos)) : 

[https://framagit.org/chatons/chatonsinfos/-/tree/cpm-ontologie](https://framagit.org/chatons/chatonsinfos/-/tree/cpm-ontologie)



CONCEPTS.md

[https://framagit.org/chatons/chatonsinfos/-/blob/cpm-ontologie/CONCEPTS.md](https://framagit.org/chatons/chatonsinfos/-/blob/cpm-ontologie/CONCEPTS.md)

Explicite les concepts derrière le projet ChatonsInfos

   * remarques d'Angie sur le principe général
       * Angie trouve que ce n'est pas assez clair (besoin d'expliciter ce qu'est la fédération par exemple)
       * peut-être nécessité d'expliciter qui aura la charge de compléter chaque type de fichier ?
           * à mettre dans le README principal : on fait une réunion de temps en temps pour cela
       * "1 fichier web public par service des organisations" : service ? logiciel ? J'ai encore du mal à savoir de quoi on parle... Si je propose 2 logiciels permettant de rendre le même service (Mattermost et Rocket.chat par exemple), est ce qu'il faudra créer 2 fichiers ou 1 seul ?
           * on ne peut pas avoir plusieurs fois une section dans un fichier properties
           * donc nécessité de faire 2 fichiers services avec chacun leur section software
   * autre question : est-ce que tous les champs apparaîtront dans les fichiers properties ? 
       * histoire de savoir s'il s'agira juste de les compléter
       * nécessité de créer des fichiers modele.properties pour chaque type de fichier




ONTOLOGIE1.md

[https://framagit.org/chatons/chatonsinfos/-/blob/cpm-ontologie/ONTOLOGIE1.md](https://framagit.org/chatons/chatonsinfos/-/blob/cpm-ontologie/ONTOLOGIE1.md)

Explicite les différentes sections (et les champs - chemin=valeur - les composant) qui peuvent être utilisées dans les fichiers .properties

   * il y a un truc qui n'est pas clair pour moi : dans la section Files, on indique 
   * # Classe du fichier (valeur parmi Federation/Organization/Service/Device, obligatoire)
   * J'en déduis donc qu'il y a 4 types de fichiers, alors que dans CONCEPTS.md, on ne parle que de 3 types de fichiers
   * Il y a donc ce type de fichier "Device" dont je ne comprends pas ce que c'est et surtout quelles sections il devrait comprendre
   * -> précision de cpm : Device n'a pas de sens dans notre structure, donc il faut l'enlever (du moins pour le moment)
   * 

   * 

   * Définition d'ontologie, y en a beaucoup mais ma préférée est :
       * « En informatique et en science de l'information, une **ontologie** est l'ensemble structuré des termes et concepts représentant le sens d’un champ d'informations, que ce soit par les métadonnées d'un espace de noms, ou les éléments d'un domaine de connaissances.  » [https://fr.wikipedia.org/wiki/Ontologie\_(informatique)](https://fr.wikipedia.org/wiki/Ontologie\_(informatique)) +1


       * fichier CONCEPTS.md en français ;
           * expliciter la notion de fédération
           * préciser qu'un chemin ne peut pas être deux fois dans un fichier
           * notion de section ajouter modèles de fichiers avec champs vides (ajouter au début le lien vers ONTOLOGIE1.md)
       * terme « ontologie »  : validé x3,
       * format ontologie,
           * Angie aime bien cette nouvelle présentation (pas en tableau, mais au format fichier)
           * validé x3,
       * avis sur la différence entre service aux utilisateurs et services autres (infra, institutionnel, autres…) ;
           * Angie : se limiter aux services aux utilisateurs mais possible d'ajouter les autres types de services avec des champs spécifiques
           * question à traiter plus tard,
       * le collectif est-il un chaton ?
           * rend certes des services (2 sites web et 1 forum), mais compliqué de créer des modèles 
           * question à traiter plus tard,
       * ajouter un fichier CHANGELOG
       * expliciter la convention de fonctionnement de la section METRICS, indiquer un autre fichier de convention de chemins
           * Fichier METRICS-ONTOLOGIE.md qui regroupe tous les champs de metrics appliqués à tous les types de services
           * On garde sur ONTOLOGIE.md une explication de comment va se structurer cette section (dont les metrics de type général)




# Troisième réunion du groupe de travail

### jeudi 24 septembre à 11h

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes : Angie, Flo, Cpm.



Notes Cpm :

   * demande de validation de l'un des fichiers MODELES/membres*.properties
       * [https://framagit.org/chatons/chatonsinfos/-/tree/master/MODELES](https://framagit.org/chatons/chatonsinfos/-/tree/master/MODELES)
       * lignes de propriété remplies vs lignes de propriété vidée
           * si on laisse les champs remplis et qu'un membre copie/colle le fichier alors risque d'oublie d'anciennes valeurs
           * décision :
               * laisser vide les champs,
               * mettre l'exemple entre parenthèse dans la ligne de commentaire juste avant,
               * TODO : à propager : FAIT
       * 1 ligne vide entre les propriétés vs propriétés collées
           * décision :
               * 2 lignes vides entre les sections
               * 1 ligne vide entre les propriétés


   * utilité d'avoir l'ontologie des sections en double ? 
       * et dans ONTOLOGIE.md et dans MODELES/membres.properties
       * et dans ONTOLOGIE.md et dans MODELES/service.properties
       * plus simple si on va directement au modèles des fichiers plutôt que de théoriser qu'un fichier est une liste de sections ontologisées ailleurs
       * les doublons de sections sont rares (1 seule)
       * décision :
           * importance d'avoir une référence quelque part
           * pas grave si cette référence est constituée de 3 fichiers (federation.properties, organization.properties, services.properties)
           * moins de reports évitera les oublis
           * validation de la suppression du fichier ONTOLOGY.md après s'être assurés que toutes les informations sont correctement reportées dans les 3 fichiers de références
           * TODO :
               * supprimer le fichier ONTOLOGIE.md
               * vérifier que les 3 fichiers de références sont à jour par rapport au fichier supprimé


   * demande de validation des fichiers modèles :
       * chatons.properties vs federation.properties
       * membre.properties vs organization.properties
       * décision de choisir les noms techniques federation.properties et organization.properties pour les modèles
           * moins de confusion entre modèles et actif
           * plus de cohérence par rapport à l'éditorial de la page CONCEPTS.md
           * TODO : renommer les fichiers : FAIT


   * question API CHATONS :
       * [https://www.chatons.org/api](https://www.chatons.org/api)
       * comment ça fonctionne ? utilisable pour générer automatique un fichier par membre ? (à titre de test)
           * nom, URL, description ?
       * Flo : un seul fichier réponse fourni, très complet mais peut-être pas avec tous les champs, configuré via Drupal (un peu complexe)
           * Cpm : ok, merci :-)


   * revue du proto :
       * [https://demochatons.devinsy.fr/](https://demochatons.devinsy.fr/)
       * reste à faire :
           * vérification fichier properties
               * pour chaque fichier, affichage par ligne des erreurs ou alertes,
               * l'idée est que chacun puisse gérer ses erreurs de remplissage, en autonomie,
           * génération de graphiques pour les métriques
           * génération de graphiques pour les non métriques (valorisation spécifique)
           * compléter l'affichage de champs (pour l'instant, tous n'y sont pas)
           * faire une revue des champs (federation, organisation, service…)
               * certains champs élémentaires sont absents ou à améliorer ou à valider
               * comparer avec les travaux de Bertille et Alain
                   * ([https://framagit.org/bertille/tx-collecte-chatons/-/tree/master/schema](https://framagit.org/bertille/tx-collecte-chatons/-/tree/master/schema)
           * amélioration visuelle (HTML/CSS)
           * ajout de fonction d'export (csv) : utile ?
       * Flo : suggestion d'utiliser datatables.net
           * possibilité de filtre dans les tableaux


   *  notion de catégorie
       * gestion via un fichier properties :
           * category.category1.name = 
           * category.category1.description = 
           * category.category1.softwares =
       * à stocker à la racine du dépôt ChatonsInfos
       * sera utilisé pour déterminé la catégorie d'un service à partir du champ software.name présent dans un fichier properties de classe service
       * cette gestion s'appuiera sur les travaux d'Angie sur le sujet
       * TODO : Angie créé et rempli le fichier categories.properties


   * génération de fichier properties de classe organization pour les membres
       * uniquement en attendant qu'ils fournissent le leur via une URL,
       * non stockage dans le dépôt ChatonsInfos, uniquement sur stats.chatons.org
       * implique de créer les liens dans la section subs du fichier chatons.properties
       * impacte la gestion du fichier chatons.properties mais dans une mesure acceptable
           * quand un membre du collectif veut déclarer ou changer l'URL de son fichier organization, il doit passer par Angie ou Flo via :
               *  une merge request dans le dépôt ChatonsInfos,
               * un canal de communication habituel






# Quatrième réunion du groupe de travail

### jeudi 08 octobre à 11h

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes : Angie, Flo, Cpm.



Notes :    

   * revue du site de démo :
       * page « Propriétés » :
           * faire 4 sous-pages, FAIT
           *  ou des sections ouvrables
           * peut-être séparer les métriques
       * page « Services » :
           * ajouter un cadre pour les légendes des icônes
           * Registration -> Inscription, FAIT
           * rendre la colonne « Logiciel » cliquable, FAIT


   * fichier categories.properties
       * Angie
           * Nettoyage des fichiers service-XXXX.properties
           * créer de nouvelles fiches modèles pour les services
       * Ecriture de categories.properties [https://framagit.org/chatons/chatonsinfos/-/blob/master/categories.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/categories.properties)
           * pas trouvé de code à utiliser pour le nom du champs de la messagerie instantanée = IM
           * il en reste à compléter
           * un logiciel doit pouvoir être dans plusieurs catégories
       * ajout dans le dépôt d'un fichier brouillon pour les tests de dév. :
           *  [https://demochatons.devinsy.fr/categories.xhtml](https://demochatons.devinsy.fr/categories.xhtml)
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/categories.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/categories.properties)
       * Angie, tu peux le modifier à ta guise
       * question : des logos pour les catégories ?
           * Angie : OUI
           * ré-utiliser les icones d'entraide.chatons.org
           * pas grave si pas de logo pour toutes les catégories, prévoir un logo par défaut
           * les poster sur le dépôt de ChatonsInfos
           * taille attendue : 256x256 serait super


   * intégration de DataTables
       * suggestion de Flo
       * possibilité de filtre dans les tableaux
       * intégré pour la page fédération, à regarder pour validation
       * intégration technique lourde. Utilité ? absence de bouton pour la recherche ? rendu visuel ?
           * Angie : pour généraliser l'intégration




   * service.documentation
       * proposition n°1 (actuel) :
           * service.technical.url=
           * service.documentation.url=
       * proposition n°2 :
           * service.documentation.user=
           * service.documentation.technical=
       * proposition n°3 :
           * service.manual.user=
           * service.manual.technical=
       * **proposition n°4** :
           * service.guide.user=
           * service.guide.technical=
           * pour : Angie
           * contre :
       * proposition n°5 :
           * ~~service.guide.user.url=~~
           * ~~service.guide.technical.url=~~
       * décision :
           * Angie, Cpm :  n°4
           * TODO Cpm


   * liste des membres du collectif 
       * en piochant [https://chatons.org/fr/find](https://chatons.org/fr/find), j'ai créé 75 fichiers properties temporaires pour les organisations
       * visible sur   [https://demochatons.devinsy.fr/](https://demochatons.devinsy.fr/) 
       * à la dernière fois réunion, le nombre évoqué était de 79 ? Lesquels manquent ?
       * Angie regarde d'où vient la différence de nombre
       * quid des chatons "en sommeil" ? quid des chatons qui ne sont plus ?
           * permet de réaliser un graph avec l'évolution du nb de chatons
           * besoin d'ajouter un champs spécifique de statut du chaton : organization.status
           * type ENUM (énumération) :  ALIVE/SLEEPY/GONE
           * TOCO Cpm : propager dans service.properties et fichiers exemples demo


   * fichiers modèles Angie :
       * # Lien de la page de support du service (type URL, recommandé).
       * service.contact.url = [https://contact.framasoft.org/fr/faq#framaforms](https://contact.framasoft.org/fr/faq#framaforms)
       * # Courriel du support du service (type EMAIL, recommandé).
       * service.contact.email = [https://contact.framasoft.org/](https://contact.framasoft.org/)
           * une URL pour un email ?
           * car pas d'adresse courriel à diffuser donc plutôt laisser vide
           * vu


   * deathdate ?
       * organization.birthdate vs organization.startdate (idem federation et service)
       * organization.deathdate vs organization.enddate  (idem federation et service)
       * Angie : POUR


   * page FAQ
       * proposition d'ajouter une page FAQ.md à la racine du dépôt pour y centraliser les questions
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/FAQ.md](https://framagit.org/chatons/chatonsinfos/-/blob/master/FAQ.md)
       * Angie : OK, FAIT, à alimenter au fil de l'eau


   * réseaux sociaux
       * plusieurs réseaux sociaux sont des GAFAM, privateurs, etc. (Facebook, Twitter, Instagram, Whatsapp…)
       * des membres du collectifs ont des comptes sur des réseaux sociaux non éthiques
       * le collectif souhaite-t-il relayer les informations vers des réseaux sociaux non éthiques ? non
       * différence entre mettre l'info dans le fichier properties et la valoriser sur stats.chatons.org oui !
       * stats.chatons.org veut-il afficher des liens vers des réseaux sociaux non éthiques ? non
       * champs qui pourra servir à Angie pour lister tous les chatons ayant un compte sur...
           * grep socialnetworkds *.properties
           * grep socialnetworkds *.properties | wc -l
           * grep -c socialnetworks *.properties
       * (hors sujet ChatonsInfos : le collectif a-t-il des comptes sur des réseaux sociaux non éthiques ? oui : Twitter (et même une page facebook non-alimentée)
       * TODO Cpm : vérifier présence dans l'ontologie organization


   * suggestion d'actions Framagit (mineures, pas urgent) :


       * modifier le nom du groupe  [https://framagit.org/chatons](https://framagit.org/chatons) 
           * c'est un champ d'affichage pour les humains
           * settings > General > Group name : de « chaton » en « C.H.A.T.O.N.S. » ou « CHATONS » ou…
           * par contre, conserver le group URL à « chatons » (qui est un nom technique)
           * maj en CHATONS


       * trouver une idée de logo pour le dépôt ChatonsInfos :
           * [https://framagit.org/chatons/chatonsinfos](https://framagit.org/chatons/chatonsinfos)
           * un simple chaton de PeeperCarrot suffira dans un premier temps :
               *   [https://www.peppercarrot.com/extras/html/2016\_cat-generator/](https://www.peppercarrot.com/extras/html/2016\_cat-generator/)
           * possibilité de reprendre celui de StatoolInfos : [https://forge.devinsy.fr/devinsy/statoolinfos/-/blob/master/resources/statoolinfos-logo.jpg](https://forge.devinsy.fr/devinsy/statoolinfos/-/blob/master/resources/statoolinfos-logo.jpg)
           * OK : Angie s'en charge


       * trouver une idée de logo pour le dépôt Entraide Chatons
           * [https://framagit.org/chatons/entraide](https://framagit.org/chatons/entraide)
           * un simple chaton de PeeperCarrot suffira dans un premier temps :
               *   [https://www.peppercarrot.com/extras/html/2016\_cat-generator/](https://www.peppercarrot.com/extras/html/2016\_cat-generator/)
           * OK : Angie s'en charge




Framasky : Angie m’a demandé de voir pour lui choper des stats pour mettre dans les fichiers properties de chaque service. Du coup, je me pose des questions sur les intervalles des stats. Explication :

   * (framaforms) metrics.forms.total.2020 / metrics.forms.total.2020.months / weeks / days : je peux faire des stats sur les formulaires de l’année/mois/semaine/jour en cours mais ça sera toujours en dents de scie selon le jour et l’heure de la relève des informations (si je chope à midi, les stats du jour sont coupées en deux, forcément. Idem pour le mercredi pour la semaine ou le 15 du mois.
       * Il y a la possibilité de faire des statistiques « flottantes » : combien de formulaires créés sur les dernières 24h, les 7 derniers jours, etc. Cela a l’avantage de faire des courbes d’évolution plus lisibles.
       * Mais Angie disait qu’elle voudrait quand même bien des statistiques « statiques » : 2020, janvier, février, etc.
       * On peut avoir les deux types de statistiques, ceci dit, il faut juste expliquer les biais des deux méthodes. Ou alors on peut créer les stats 2020 quand on passe en 2021, celles de janvier quand on passe en février, etc.
       * Bref : faudrait une explication claire et nette des stats attendues 🙂
       * Cpm :
           * le cas d'usage de départ, à titre d'exemple, c'est :
               * la réunion mensuelle Chapril où l'on regarde l'activité du mois précédent ; la réunion pouvant avoir lieu le 06 du mois d'après, les statistiques que l'on regarde portent sur le mois entier précédent et non les 30 derniers jours,
               *  le bilan annuel du Chapril : j'ai bon espoir de pouvoir faire un beau rapport avec des graphiques pour l'année 2020 ; je risque de travailler dessus en mars 2021 et donc j'ai vraiment besoin de l'année entière et non des 365 derniers jours depuis mars,
           * du coup, là,  l'objectif est de mesurer l'activité sur une période de temps donnée (tel jour, tel mois, telle année), exemples : combien de sondages créés le 24/09/2020 ? Au mois de juillet 2020 ? Sur toute l'année 2020 ?
           * du coup, ça répond à la question du passé alors que l'approche « flottante » répond à la question du présent, je suis mercredi et il s'est passé quoi les 7 derniers jours ; les deux approches sont intéressantes et complémentaires ; pour faire simple j'aurais tendance à commencer par une seule approche, celle calendaire, et à revenir vers l'approche « flottante » plus tard.
           * effectivement, bien vu, on obtient souvent des dents de scie mais ça le fait bien quand même (exemple : [https://statool.agir.april.org/)](https://statool.agir.april.org/))
           * cela implique que la donnée soit calculée **une fois la période finie**, oui. Pas grave si ce n'est pas immédiatement après (si le type de donnée le permet).
           * possibilité de calculer la donnée en intermédiaire et donc il faut accepter une grosse dent de scie sur la période en cours et à condition de garantir qu'un ultime calcul sera fait une fois la période finie. Pour la statistique mensuelle, si la valeur est calculée tous les jours, clair que ça va changer tous les jours avant d'arriver à la bonne valeur finale.
           * Vala un début de réponse, faut que je réfléchisse encore :*>


Prochain point :

   * on maintient le rendez-vous du jeudi 




# Cinquième réunion du groupe de travail

### jeudi 15 octobre à 11h

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes : Angie, Flo, Cpm.



Angie n'a absolument rien fait de ce qu'elle devait faire depuis la semaine dernière !

TODO : 

   * vérifier les fiches federation.properties / organization.properties / service.properties
   * créer la fiche organization.properties pour Framasoft
   * categorie.properties : ajouter 
   * préparer des pictos (256x256) pour illustrer les catégories d'outils sur [https://demochatons.devinsy.fr/categories.xhtml](https://demochatons.devinsy.fr/categories.xhtml)
   * ajouter un logo pour le dépôt ChatonsInfos
   * ajouter un logo pour le dépôt Entraide Chatons


Florian a travaillé sur de nouvelles fiches modèles pour les services YesWiki / Scrumblr / PolR / PeerTube



Sur les metrics spécifiques à chaque service, on peut trouver la liste sous : 

[https://demochatons.devinsy.fr/propertyStats.xhtml](https://demochatons.devinsy.fr/propertyStats.xhtml)





Notes :    

   * revue du prototype : [https://demochatons.devinsy.fr/](https://demochatons.devinsy.fr/)
       * menu : ajout de  « Réseaux sociaux »
       * page « Services »  : nouvelle qualité d'image des oiseaux de David Revoy
       * page « Services » : ajout d'un bloc de légendes
       * page « Propriétés » : split de page / ajouter la possibilité de trier sur les colonnes
       * 1 logiciel dans plusieurs catégories : en cours (Cpm)
       * 1 image par catégorie : en cours (Angie)


   * Infos sur l'accès à la machine pour déployer StatoolInfos ? :D
       * besoin de la clé SSH publique de Flo > envoyé le 15 oct 2020 par mail a Angie
       * envoyer les demandes d'installation / configuration à Luc sur tech@framalistes.org


   * réunion virtuelle mensuelle de ce soir : qui présente les avancées ? Cpm
       * montrer la démo ? 
           * éviter de montrer une url qui n'est pas stats.chatons.org
           * comment gérer les nombreux retours ?
           * on montrera cela lors de la réunion de novembre


   * pour le mois prochain : 
       * créer les fichiers framasoft.properties et colibris.properties et ajouter l'url sur chatons.properties
       * lier nos fiches services à notre fiche organization.properties (via subs)


   * service.status : proposition de redéfinition des propriétés
       * 3 possibilités :
           * n°1 : service.status (ON/MAINTENANCE/DOWN/OFF/DRAFT)
           * n°2 : service.status (OK, WARNING, ALERT, ERROR, OVER, VOID) + service.status.description
           * n°3 : service.status.level (OK, WARNING, ALERT, ERROR, OVER, VOID) + service.status.description
           * OK = VERT
           * WARNING = JAUNE
           * ALERT = ORANGE
           * ERROR = ROUGE
           * OVER = NOIR
           * VOID = BLEU
       * décision ?
           * n°3 : Flo, Cpm
           * Angie : du moment qu'il y a une légende !
           * TODO Cpm : propager dans les fichiers du dépôt


   * suggestion de copier/cloner le dépôt [https://framagit.org/framasoft/CHATONS](https://framagit.org/framasoft/CHATONS) vers le groupe [https://framagit.org/chatons](https://framagit.org/chatons) 
       * suggestion d'une **copie/clonage** pour que l'original reste chez Framasoft (après tout, ça a été une activité interne de Framasoft et donc ça  a du sens que le dépôt initial y reste présent).
       * grand moment symbolique ;-)
       * possible de cloner le projet Gitlab par export
       * à prévoir entre 2 portée et surtout juste après une portée
       * attention, le clone/import potentiellement ça perd les id user des tickets
       * à aborder et valider pendant la prochaine réunion mensuelle CHATONS
           * TODO Angie


   * dépôt [https://framagit.org/framasoft/chatons-generator](https://framagit.org/framasoft/chatons-generator) ?
       * suggestion de copie/clonage vers [https://framagit.org/chatons](https://framagit.org/chatons) 
       * Angie n'en voit pas l'intérêt mais possibilité de créer un fichier ReadMe sur le groupe pour y faire un lien
       * [https://wiki.chatons.org/chatons-generator/](https://wiki.chatons.org/chatons-generator/)
       * application déployée sur [https://chatons.org/](https://chatons.org/) ?
       * a priori pertinence à le référencer, mais où ? sur le groupe, mais comment ?
       * TODO Angie : en parler avec Thomas (déplacement du dépôt dans le groupe CHATONS ?)
       * TODO Angie : faire déplacer [https://wiki.chatons.org/chatons-generator/](https://wiki.chatons.org/chatons-generator/) sur chatons.org/chatons-generator


   * application [https://www.peppercarrot.com/extras/html/2016\_cat-generator/](https://www.peppercarrot.com/extras/html/2016\_cat-generator/)
       * ~~suggestion de créer un dépôt pour garder le code sous le coude car y a juste un zip~~
       * forker le projet [https://framagit.org/Deevad/cat-avatar-generator](https://framagit.org/Deevad/cat-avatar-generator)  dans le groupe CHATONS pour conserver un lien vers le projet officiel
       * a priori pertinence à le référencer, mais où ? sur le groupe, mais comment ?
       * sur chatons.org : une page pour accéder à chalogo et chavatar !




# Sixième réunion du groupe de travail

### jeudi 22 octobre à 11h

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes : Angie, Flo, Cpm.



   * divers :
       * ajouter un logo pour le dépôt ChatonsInfos : Angie, FAIT
       * ajouter un logo pour le dépôt Entraide Chatons : Angie, FAIT


   * déplacer le dépôt [https://framagit.org/framasoft/CHATONS](https://framagit.org/framasoft/CHATONS) vers le groupe [https://framagit.org/chatons](https://framagit.org/chatons) 
       * à aborder et valider pendant la prochaine réunion mensuelle CHATONS
           * TODO Angie FAIT


   * déplacer le dépôt [https://framagit.org/framasoft/chatons-generator](https://framagit.org/framasoft/chatons-generator) ?
       * TODO Angie : en parler avec Thomas (déplacement du dépôt dans le groupe CHATONS ?)
           * FAIT
       * TODO Angie : faire déplacer [https://wiki.chatons.org/chatons-generator/](https://wiki.chatons.org/chatons-generator/) sur chatons.org/chatons-generator
       * ajouter un logo pour le dépôt :
           * Angie : FAIT
           * pas fait : à priori, ce sera plutôt un sous-domaine de chatons.org (generator.chatons.org) qu'une page car cela permet de ne pas lier chatons-generator au CMS. Demande à Luc pour son retour de congés.
               * TODO Angie en cours
   * valorisation des applis connexes (générateur de David Revoy + generator.chatons.org)
       * faire des iframes (avec du css pour prendre la largeur)? juste le lien vers les sites?
           * au plus simple pour commencer
           * TODO angie : créer la page
           * TODO pyg : coder les iframes


   * FAQ, enrichissements à relire/compléter/valider :
       * Organisation
           * Qui doit participer ?
           * ChatonsInfos va-t-il remplacer la revue annuelle des fiches adhérents ?
       * Fichiers *properties*
           * Où trouver des modèles ?
           * Où mettre les fichiers *properties* ?
               * ajouter une précision sur l'utilisation de NC dans ce cadre FAIT
           *  J'ai créé le fichier *properties* de mon organisation, j'en fais quoi ?
           * J'ai créé le fichier *properties* d'un service, j'en fais quoi ?
           * Quelles contraintes pour les images ?
           * Je ne veux pas partager les données de mes clients
       * Le site stats.chatons.org
           * Quel est le but de stats.chatons.org
           * Cela ne va-t-il pas créer de la compétition entre membre du collectif CHATONS ?
           * Je ne partage pas encore de ficher *porperties*, suis-je sur stats.chatons.org ?
           * Est-ce que des personnes pourront s'en servir pour trouver un service ?
       * Je ne suis pas certaine de bien avoir compris dans quel sens fonctionne la section Subs : ça vaudrait la peine de faire une section dans la FAQ (ou CONCEPT) pour que ce soit plus explicite
           * on met sur organization.properties dans la section [Subs] les liens des fiches services
           * on met sur chatons.properties le lien vers la fiche organization.properties
           * TODO Angie : expliciter cet aspect dans le fichier CONCEPT
           * TODO Angie : réaliser un schéma de la structuration des fiches pour le fichier CONCEPT
       * TODO Cpm mettre la section stats.chatons.org en n°2


   * revue du prototype : [https://demochatons.devinsy.fr/](https://demochatons.devinsy.fr/)
       * page « Réseaux sociaux » :
           * ajout de Funkwhale, Mobilizon, Peertube et Pixelfed  FAIT
           * gestion de .url ou pas   FAIT
           * ajouter les champs sur les modèles federation.properties et organization.properties FAIT
       * page « Propriétés » :
           * correction du « organizations » FAIT
       * 1 logiciel dans plusieurs catégories : 
           * TODO Cpm  FAIT
       * 1 image par catégorie : en cours (Angie, Cpm)
           * TODO Cpm : coder la possibilité dans StatoolInfos  FAIT
           * TODO Angie : mettre les images dans le dépôt :
               *  [https://framagit.org/chatons/chatonsinfos/-/tree/master/CATEGORIES](https://framagit.org/chatons/chatonsinfos/-/tree/master/CATEGORIES)
               * j'ai pas encore tout fini, mais vous pouvez en voir un certain nombre sous : [https://asso.framasoft.org/nextcloud/s/mRqApH3X5PNbMt2](https://asso.framasoft.org/nextcloud/s/mRqApH3X5PNbMt2)
               * je reprends ces images pour les avoir .svg et avec des noms en minuscule
               * une fois terminé, besoin de vous pour l'upload (parce qu'en version web, je dois le faire fichiers par fichiers) + besoin d'ajouter en lot les méta-données
           * TODO Angie : renseigner le fichier categories.properties :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/CATEGORIES/categories.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/CATEGORIES/categories.properties)
               * ça avance, mais c'est pas terminé !
               * categories.XXX.logo.path=nom du fichier avec extension


   * foncion d'export en JSON/CSV


   * ONTOLOGIE :
       * TODO Cpm propager n°3 dans les fichiers du dépôt service.status.level et service.status.description
           * FAIT
       * TODO Cpm propager n°4 dans les fichiers du dépôt xxxx.guide.user et xxxx.guide.technical
           * FAIT 
       * proposition de supprimer les propriétés suivantes (au début, certaines propriétés paraissaient intéressantes mais maintenant plus vraiment) :
           * organization.favicon
           * organization.owner.favicon
           * décision : oui, on supprime ces 2 champs
               * TODO Cpm
       * proposition d'ajouter sur les fiches organisations 2 champs pour indiquer le statut du chaton
           * # Statut de l'organisation (un parmi {ACTIVE, SLEEPY, CLOSED}, obligatoire) 
                   * plutot IDLE / STANDBY --) ou ONSTANDBY pour SLEEPY?
                   * plutot AWAY que CLOSED ? 
                   * **décision du jour : ACTIVE, IDLE, AWAY**
               * organization.status.level = 
           * # Description du statut de l'organisation (type STRING, optionnel, exemple : en sommeil)
               * organization.status.description = 
           * TODO Angie : ajouter dans le modèle organization.properties : FAIT
           * TODO Cpm : ajouter dans les 75 fiches par défaut 
           * TODO Cpm :  valoriser en entête d'orga  avec picto (voir picto messagerie ou feu tricolore ou…)
       * modifier le champs organization.endate pour qu'il soit optionnel : FAIT
           * pareil pour service.enddate FAIT
           * vérifier que bien propager dans statoolinfos


   * Accès technique à stats.chatons.org
       * TODO Cpm  envoyer les demandes d'installation / configuration à Luc sur tech@framalistes.org  FAIT
           * en attente du retour de congés de Luc
       * TODO Angie : envoyer les clés SSH publique de Cpm et MrFlos à Luc FAIT
           * login : framasoft\_christian
           * login : framasoft\_florian
           * login : framasoft\_angie


   * relire/compléter/valider le README.md de ChatonsInfos :
       *  [https://framagit.org/chatons/chatonsinfos/-/blob/master/README.md](https://framagit.org/chatons/chatonsinfos/-/blob/master/README.md)
       * TODO Licence à compléter dans le Readme: CC-BY-SA : FAIT




   * publier les fichiers properties de Framasoft et Colibri :DDD
       * fiche orga de Framasoft : [https://asso.framasoft.org/nextcloud/s/68QZJJZdCHxpHCz](https://asso.framasoft.org/nextcloud/s/68QZJJZdCHxpHCz)
       * colibri : fait


   * Je me rends compte que je n'ai pas les mêmes champs que les modèles sur mes fiches services (sûrement ajout de champs après que j'y ai travaillé. Est-ce que je les reprends ? Ou j'attends la version définitive ?
       * fiche service pour Framabin : [https://asso.framasoft.org/nextcloud/s/Y87NaDqe63NgARf](https://asso.framasoft.org/nextcloud/s/Y87NaDqe63NgARf)
       * fiche service pour Framaforms : [https://asso.framasoft.org/nextcloud/s/2xDdrxEaWpayggn](https://asso.framasoft.org/nextcloud/s/2xDdrxEaWpayggn)
       * fiche service pour Framagit : [https://asso.framasoft.org/nextcloud/s/qQ5CzKzZyx2Y3gp](https://asso.framasoft.org/nextcloud/s/qQ5CzKzZyx2Y3gp)
       * Fiche service pour Framalistes : [https://asso.framasoft.org/nextcloud/s/4kwyY2aHiYdNdJP](https://asso.framasoft.org/nextcloud/s/4kwyY2aHiYdNdJP)
       * Fiche service pour Framapiaf : [https://asso.framasoft.org/nextcloud/s/WdBXc8g7YFBz6wC](https://asso.framasoft.org/nextcloud/s/WdBXc8g7YFBz6wC)
       * Fiche service pour Framapic : [https://asso.framasoft.org/nextcloud/s/8HzrXpTqKMQiKPD](https://asso.framasoft.org/nextcloud/s/8HzrXpTqKMQiKPD)
       * Fiche service pour Framindmap : [https://asso.framasoft.org/nextcloud/s/46DjtTPkQqgyEYL](https://asso.framasoft.org/nextcloud/s/46DjtTPkQqgyEYL)




# Septième réunion du groupe de travail

### jeudi 29 octobre à 11h

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes : Angie, Flo, Cpm.



   * Accès technique à stats.chatons.org
       * accès framasoft\_christian : OK
       * configuration .well-known/chatonsinfos/ : FAIT OK
       * configuration du mime properties : FAIT OK
       * transfert sftp des fichiers HTML* générés par statoolinfos : FAIT
       * dans le dépôt ChatonsInfos, ajout du dossier StatoolInfos pour stocker l'aborescence de fichies utilisés par statoolinfos : ???
       * well-known/chatonsinfos/default : modification possible des fichiers par défaut des organisations


   * FAQ :
       * TODO Cpm mettre la section stats.chatons.org en n°2 : FAIT
       * TODO Angie : expliciter les liaisons entre les fichiers /sections dans le fichier CONCEPT
       * TODO Angie : réaliser un schéma de la structuration des fiches pour le fichier CONCEPT (en cours)


   * ONTOLOGIE
       * propriétés favicon
           * organization.favicon et organization.owner.favicon
           * décision : oui, on supprime ces 2 champs
           * TODO Cpm FAIT
       * organization.status.* :
           * TODO Cpm : ajouter dans les 75 fiches par défaut  FAIT
           * TODO Cpm :  valoriser en entête d'orga  avec picto (voir picto messagerie ou feu tricolore ou…)
       * organization.socialnetwork.* : 
           * ajouté sur la fiche modèle
       * traitement de la merge-request de MrFlos


   * inscription : adhérent vs membre
       * contexte :
           * dans la page d'un service et dans les tableaux de services, sont présents des pictogrammes indiquant le mode d'inscription : sans compte, compte libre, adhérent et client.
           * certains membres du collectif modèrent la création de compte sans pour autant les associer à la notion d'adhésion.
       * propositions de :
           * modifier « Réservé aux adhérents » en « Réservé aux membres »
           * remplacer le pictogramme « liasse de billets » par [https://fr.wikipedia.org/wiki/Fichier:Circle-icons-rgb.svg](https://fr.wikipedia.org/wiki/Fichier:Circle-icons-rgb.svg), les cercles symbolisant la relation étroite de membre
           * choisir entre les pictogrammes « liasse de billets » et « carte bancaire » pour le mode « client ».
               * Cpm : conserver « carte bancaire » car évoque davantage une relation client
       * vos avis ?
           * OK


   * revue du prototype : [https://stats.chatons.org/](https://stats.chatons.org/) 😍
       * Properties check :
           * améliorations des détections, FAIT
           * ajout des propriétés obligatoires manquantes : FAIT
           * affichage aussi des propriétés WISHED manquantes : FAIT
       * amélioration de la gestion des logos avec détection du format : FAIT
       * pictogrammes en svg
           * [https://asso.framasoft.org/nextcloud/s/mRqApH3X5PNbMt2](https://asso.framasoft.org/nextcloud/s/mRqApH3X5PNbMt2) FAIT
           * tous les circle-icons sont maintenant en svg FAIT
       * Properties File :
           * ajout colonnes attendues, erreur, inconnues FAIT
           * nombre cliquable pour arriver dans la page du vérificateur FAIT


   * revue de fichiers properties de membres (et pour le membre et pour vérifier statoolinfos :p) :
       * Colibri > ya du caca
       * Framasoft


TODO Angie : vérifier que tous les chatons ont une fiche organization.properties par défaut (en ajoutant les chatons en sommeil avec le champs organization.status complété)





# Huitième réunion du groupe de travail

### jeudi 05 octobre à 11h

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes : Angie, Flo, Cpm.



   * gestion technique de stats.chatons.org
       * mise en place cron de génération toutes les heures (regénération des fichiers properties pour le site) : TODO Cpm avec tech@
       * mise en place déclencheur manuel de générateur (via sftp) : TODO Cpm avec tech@
       * gestion du paramétrage
           * fichiers à gérer :
               * chatons.properties : à la racine du dépôt ChatonsInfos + stats.chatons.org:/web/.well-known/chatonsinfos/
               * categories.properties : dans le dépôt ChatonsInfos + stats.chatons.org:/web/.well-known/chatonsinfos/
               * organizations.default : dossier comprenant les fichiers propriétés par défaut des membres du collectif, dans stats.chatons.org:/web/.well-known/chatonsinfos/default/
           * questions :
               * quel est le fichier de référence ? Le dépôt ChatonsInfos ~~ou l'espace SFTP~~ ?
               * dans le dépôt ChatonsInfos, ajout du dossier StatoolInfos pour stocker l'aborescence de fichies utilisés par statoolinfos : ???
               * well-known/chatonsinfos/default : modification possible des fichiers par défaut des organisations
               * ajouter un fichier read.me pour expliciter qu'il ne faut pas modifier les contenus de well-known/chatonsinfos/default mais uniquement chatons.properties et categories.properties


   * ONTOLOGIE :
       * ajout dans CONCEPT le statut d'une organisation : TODO CPM FAIT
       * TODO Angie : expliciter les liaisons entre les fichiers /sections dans le fichier CONCEPT
       * TODO Angie : réaliser un schéma de la structuration des fiches pour le fichier CONCEPT (en cours)
       * champs host.type dans les fiches services.properties
           * VPS / CLOUD / LOCATEDSERVER / HOSTSERVER / HOMESERVER / RASPBERRY
           * besoin d’éclaircir ces différents termes
           * ~~LOCATEDSERVER -> RENTEDSERVER~~
           * bien indiquer que c'est expérimental
           * HOSTSERVER-> HOSTEDSERVER,
           * CLOUD VIRTUALPRIVATESERVER (VPS) HOSTEDSERVER HOSTEDBAY SHAREDHOSTING
           * HOMECLOUD HOMESERVER HOMENANOSERVER
           * et ajouter des lignes de commentaires pour expliciter ces valeurs
               * CLOUD : ensemble de serveur nécessaires pour faire tourner des services lourds
               * VIRTUALPRIVATESERVER (VPS) : machine virtuelle 
               * HOSTEDSERVER : serveur dédié (machine physique)
               * HOSTEDBAY : serveur branché sur un emplacement de baie réseau chez un hébergeur
               * SHAREDHOSTING : hébergement mutualisé (un petit bout de machine)
               * HOMECLOUD : ensemble de serveurs nécessaires pour faire tourner des services lourds, mais hébergés chez soi
               * HOMESERVER : serveur dédié chez soi
               * HOMENANOSERVER : nanomachine (exemple Raspberry Pi) hébergé chez soi
       * TODO mettre à jour la fiche modèle service.properties (et propager sur toutes les fiches services)
           * TODO Cpm




   * automatiser la duplication de certains champs entre fiches service.properties ?
       * service.contact.url
       * service.contact.email
       * service.legal.url
       * ~~service.guide.user~~
       * ~~service.guide.technical~~
       * ~~host.name~~
       * ~~host.description~~
       * ~~host.type ~~
       * ~~host.country.name~~
       * ~~host.country.code~~
       * pas super pertinent pour 3 champs et trop complexe à expliquer (car duplication post-remplissage)
       * intelligence héritage vs copier/coller properties => la rusticité l'emporte


   * Intégrer une page html de présentation de l'outil (petit tutoriel) sur stats.chatons.org
       * page qui apparaîtra dans le dossier input


   * revue du prototype : [https://stats.chatons.org/](https://stats.chatons.org/) 😍
       * Properties check :
           * amélioration du Shrink
       * icônes de catégories \o/
           * [https://stats.chatons.org/categories.xhtml](https://stats.chatons.org/categories.xhtml)
           * si je fais des mises à jour de fichiers .svg, je dois aller modifier où ? dans le futur dossier input du dossier StatoolInfos du dépôt ChatonsInfos
           * gérer le cas de noms de fichier logo non existants TODO Cpm


   * revue de fichiers properties de membres (et pour le membre et pour vérifier statoolinfos :p) :
       * Colibri > ya du caca
       * Framasoft
           * il faut que je reprenne les fiches existantes en ajoutant tous les champs manquants
           * il faut que je créé de nouvelles fiches




Angie ne pourra pas être présente le jeudi 12 novembre



# Neuvième réunion du groupe de travail

### vendredi 13 novembre à 11h

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personne présentes : Flo, Cpm.



   * gestion technique de stats.chatons.org
       * lancement à chaque heure (cron) : à faire
       * déclencheur manuel (fichier SFTP) : à faire
       * arborescence statoolinfos dans le dépôt ChatonsInfos : à faire
       * fichier properties brut en téléchargement au lieu d'être affichés par le navigateur : à faire


   * ONTOLOGIE
       * organization.guide.user : dans le fichier modèle du dépôt, absence de cette propriété
           * décision : 
               * pour : Flo, Cpm
               * TODO Flo : FAIT
       * host.type 
           * voir [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/service.properties#L88](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/service.properties#L88)
           * cas supplémentaire ?
               * service > HOME   > SharedM 
               * service > HOSTED > BAY > SM
               * service > HOSTED > BAY > CLOUD
               * service > HOSTED > PM > VM
               * service > HOSTED > PM > CLOUD
               * service > HOSTED > Shared
           * proposition de différenciation du cas HOMESERVER
               * avant :
                   * service > HOME   > PM =>  HOMESERVER
                   * service > HOME   > VM =>  HOMESERVER  
               * propositions :
                   * HOMEPHYSICALSERVER vs HOMEPHYSICALMACHINE vs HOMESERVER
                   * HOMEVIRTUALSERVER  vs HOMEVIRTUALMACHINE  vs HOMEVIRTUALSERVER
           * proposition de différenciation du cas HOSTEDBAY
               * avant :
                   * service > HOSTED > BAY > PM       HOSTEDBAY
                   * service > HOSTED > BAY > VM       HOSTEDBAY
               * après :
                   * HOSTEDBAYPHYSICALSERVER vs HOSTEDBAYPHYSICALMACHINE vs HOSTEDBAYSERVER
                   * HOSTEDBAYVIRTUALSERVER  vs HOSTEDBAYVIRTUALMACHINE  vs HOSTEDBAYVIRTUALSERVER 
           * étude proposition de scinder le champ :
               * 16 cas en tout :
                   *                  NANO    PHYSICAL    VIRTUAL    SHARED    CLOUD
                   * HOME              pm        pm          vm      shared    cloud
                   * HOSTEDBAY         --        pm          vm      shared    cloud
                   * HOSTEDSERVER      --        pm          vm      shared    cloud
                   * OUTSOURCED           --        --         vps      shared    cloud
               * cela fait-il trop pour s'y retrouver ?
               * possibilité de scinder en un couple de 2 valeurs (1 parmi 5 x 1 parmi 4):
                   * host.server.type      : NANO, PHYSICAL, VIRTUAL, SHARED, CLOUD
                   * host.provider.type  : HOME, HOSTEDBAY, HOSTEDSERVER, OUTSOURCED
               * couvre tous les cas et déterminisme pratique
               * brouillon de rédaction pour mettre dans les fichiers modèles service*.properties
*# Type de serveur (un parmi NANO, PHYSICAL, VIRTUAL, SHARED, CLOUD, obligatoire, exemple : PHYSICAL).*

# *  NANO* : nano-ordinateur (Raspberry Pi, Olimex…)

#   *PHYSICAL* : machine physique

# *  VIRTUAL* : machine virtuelle

#   *SHARED* : hébergement mutualisé

# *  CLOUD* : infrastructure multi-serveurs

host.*server*.type = 



*# Type d'hébergement (un parmi HOME, HOSTEDBAY, HOSTEDSERVER, OUTSOURCED, obligatoire, exemple : HOSTEDSERVER).*

#   HOME : hébergement à domicile

# *  HOSTEDBAY* : serveur personnel hébergé dans une baie d'un fournisseur

#   HOSTEDSERVER : serveur d'un fournisseur

# *  OUTSOURCED* : infrastructure totalement sous-traitée

host.provider.type =



# Si vous avez du mal à remplir les champs précédents, ce tableau pourra vous aider :

*#                  NANO    PHYSICAL    VIRTUAL    SHARED    CLOUD*

*# HOME              pm        pm          vm      shared    cloud*

*# HOSTEDBAY         --        pm          vm      shared    cloud*

*# HOSTEDSERVER      --        pm          vm      shared    cloud*

*# OUTSOURCED        --        --         vps      shared    cloud*

*# Légendes : pm : physical machine* ; vm : virtual machine ; vps : virtual private server.

       * décision :
           * pour : Flo, Cpm
           * contre :
       * TODO flo mettre dans le fichier modèle service.properties : FAIT
       * TODO Cpm propager aux autres fichiers service-foo.properties : à faire


   * revue de [https://stats.chatons.org/](https://stats.chatons.org/) 😍
       * gérer le cas de noms de fichier logo non existants TODO Cpm FAIT
       * réseau sociaux publics d'une organisation :
           * RocketChat ?
           * IRC ?
           * Mattermost ?


   * revue de fichiers properties de membres 
       * revue Colibri  OK




# Dixième réunion du groupe de travail

### jeudi 19 novembre à 11h

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes : Angie, Cpm



   * débrief réunion mensuelle
       * création d'une catégorie sur le forum pour les retours : 
           * [https://forum.chatons.org/c/collectif/stats-chatons-org](https://forum.chatons.org/c/collectif/stats-chatons-org)
           * Angie a répondu ce matin aux questions qui s'étaient déjà posées
           * Angie a créé un nouveau topic concernant la réalisation du fichier *organization.properties*


   * gestion technique de stats.chatons.org
       * arborescence statoolinfos dans le dépôt ChatonsInfos  : FAIT
           * déplacement du fichier /chatons.properties et du dossier /CATEGORIES dans /StatoolInfos
           * dans /StatoolInfos, ajout d'un fichier README
           * dans /StatoolInfos, ajout d'un dossier edito :
               * pour rajouter dans stats.chatons.org une page à contenu libre,
               * edito.xhtml : fichier où mettre le texte libre,
               * edito-uneimage.png : exemple d'image à mettre dans le dossier edito,
       * fichier properties brut en téléchargement au lieu d'être affichés par le navigateur
           * ça fonctionnait mais ça ne fonctionne plus
           * TODO Cpm courriel à tech@  : FAIT
           * à mettre en place par tech@ : FAIT
       * lancement à chaque heure (cron) :
           * TODO Cpm écrire script + courriel à tech@  : FAIT
           * à mettre en place par tech@ : FAIT
       * déclencheur manuel (fichier SFTP) :
           * TODO Cpm écrire script + courriel à tech@  : FAIT
           * à mettre en place par tech@ : FAIT
           * transmettre le nom du fichier déclencheur : REFRESH


   * ONTOLOGIE
       * rappels précédentes réunion :
           * organization.guide.user : ajout dans le fichier modèle du dépôt : FAIT
       * host.type scindé en deux :
               * host.server.type      : NANO, PHYSICAL, VIRTUAL, SHARED, CLOUD
               * host.provider.type  : HOME, HOSTEDBAY, HOSTEDSERVER, OUTSOURCED
           * TODO Flo mettre dans le fichier modèle service.properties : FAIT
           * TODO Cpm propager aux autres fichiers service-foo.properties : FAIT
       * host.description :
           * actuellement en recommandé
           * à l'usage (récent), ne semble pas super nécessaire
           * Proposition de le passer en optionnel :
               * pour : Cpm, Angie
               * contre : 
               * décision : adopté, à faire
               * TODO Cpm fichier modèle service.properties
               * TODO Cpm fichiers modèles services-foo.properties
       * réseau sociaux d'une organisation
           * les suivants sont absents de la liste pré-définie et dans stats.chatons.org :
               * RocketChat et Mattermost : indiquer la "team" si celle-ci est utilisée pour de la diffusion publique d'infos (mais attention à ce que les chatons n'indiquent pas leur instance)
               * IRC : ok
           * proposition de les ajouter :
               * pour :
               * contre :
               * décision :
               * TODO ajouter dans le fichier modèle organization.properties
               * TODO Cpm ajouter dans l'outil statoolinfos
       * information de membre du collectif
           * actuellement, des champs existent pour décrire le membre :
               * # Date de création de l'organisation (type DATE, recommandé, ex. 08/11/2018).
               * organization.startdate = 
               * # Date de fermeture de l'organisation (type DATE, optionnel).
               * organization.enddate =
               * # Statut de l'organisation (un parmi {ACTIVE, IDLE, AWAY}, obligatoire).
               * organization.status.level=
               * # Description du statut de l'organisation (type STRING, optionnel, ex. en sommeil).
               * organization.status.description=
           * proposition d'ajouter des champs pour décrire le membre en tant que membre :
               * # Date d'entrée dans le collectif (type DATE, obligatoire, ex. 08/11/2018).
               * organization.member.startdate
               * # Date de sortie du collectif (type DATE, optionnel, ex. 08/11/2019).
               * organization.member.enddate
               * # Statut de l'organisation en tant que membre (un parmi {ACTIVE, IDLE, AWAY}, obligatoire).
               * organization.member.status.level=
               * # Description du statut de l'organisation en tant que membre (type STRING, optionnel, ex. en sommeil).
               * organization.member.status.description=
           * alors :
               * pour :
               * contre :
               * décision :
               * TODO ? propager dans organization.properties
               * TODO ? propager dans  CHANGELOG.md
               * TODO Cpm propagager statoolinfos
               * TODO ? propager dans le forum


   * revue de [https://stats.chatons.org/](https://stats.chatons.org/) 😍
       * intégration d'une page édito : [https://stats.chatons.org/edito.xhtml](https://stats.chatons.org/edito.xhtml)
           * à modifier sur [https://framagit.org/chatons/chatonsinfos/-/tree/master/StatoolInfos/edito](https://framagit.org/chatons/chatonsinfos/-/tree/master/StatoolInfos/edito) (liens relatifs)
           * texte pour les internautes explicitant où on est et comment on trouve des infos
       * ajout de la page Statistiques (fédération)
           * demande : quels autres statistiques mettre ? TODO Angie : prendre un temps pour y réfléchir
           * utiliser les pourcentages plutôt que les chiffres ?
           * on s'occupera de la mise en forme une fois qu'on aura tous les types de stats
       * ajout de statistiques au niveau des organisations (dans la ligne lien, bouton vers une page dédiée)
           * affichage chronologique du nombre de services par structure
           * nombre d'utilisateurs de l'organisation
       * liste des membres manquants (75 affichés) ?
           * 76 chatons actifs + 6 chatons en sommeil -> 82 chatons
           * TODO Angie : vérifier la liste des organisations (et maj les fiches des chatons en sommeil)


   * revue de fichiers properties de membres  :
       * Framasoft,
           * startdate organization
       * Colibri
       * Chapril
       * Ajouter [https://wtf.roflcopter.fr/.well-known/chatonsinfos/roflcopter.properties](https://wtf.roflcopter.fr/.well-known/chatonsinfos/roflcopter.properties)
           * TODO Angie
       * revue des merge request
           * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
           * celle de filament 
               * TODO Cpm : FAIT
               * TODO Angie : x2
       * revue du forum
           * [https://forum.chatons.org/t/questions-sur-les-properties-avec-stats-chatons-org/](https://forum.chatons.org/t/questions-sur-les-properties-avec-stats-chatons-org/)
           * On fait quoi quand un service est construit avec plusieurs logiciels ?
               * pour l'instant, on ne gère pas dans stats.chatons.org ce type de situation
               * nom de ce type d'outil : suite collaborative




# Onzième réunion du groupe de travail

### jeudi 26 novembre à 11h

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes : Angie, cpm



   * débrief atelier d'accompagnement du 25/11/2020 :
       * annonce forum : [https://forum.chatons.org/t/session-daccompagnement-chatonsinfos-le-mercredi-25-11-2020-19h-20h/1730](https://forum.chatons.org/t/session-daccompagnement-chatonsinfos-le-mercredi-25-11-2020-19h-20h/1730)
       * pad dédié : [https://pad.chapril.org/p/chatonsinfos-20201125](https://pad.chapril.org/p/chatonsinfos-20201125)
       * animation : Cpm et Flo
       * participation : Popi (JY), Zatalyz, ljf
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * TODO Cpm
       * demande d'amélioration de la doc# sur host.name : préciser qu'il ne s'agit pas de l'hébergeur du service mais de l'hébergeur de la machine qui fait tourner le service (et préciser qu'en cas d'autohébergement, c'est le même)
           * TODO Flo ?
       * cas d'un service non logiciel (ljf) :
           * [https://stats.chatons.org/sansnuagearn-housing.xhtml](https://stats.chatons.org/sansnuagearn-housing.xhtml)
           * pose des questions par rapport à certains champs obligatoires dans le fichier service.properties :
               * l'absence de propriété host est catégorisée en erreur /!\/?\
               * colonne vide dans les listes de services
               * software.name, software.license.url, software.license.name
               * host.server.type
               * host.provider.type
           * comment traiter ce cas ?
               * créer un nouveau type de fiche pour traiter ces cas là ?
               * réfléchir aux champs qui y apparaîtraient : demander à ljf et les autres chatons proposant cela
               * comment intégrer cette fiche dans l'interface stats ?
       * cas d'un service s'appuyant sur plusieurs logiciels  (ljf) :
           * [https://stats.chatons.org/software-nextcloudonlyoffice.xhtml](https://stats.chatons.org/software-nextcloudonlyoffice.xhtml)
           * discussion sur la notion de « suite collaborative » avec le logiciel Nextcloud quelque soit les modules installés
           * manque de visibilité sur les modules installés
           * nécessiterait de pouvoir déclarer plusieurs section [Software] dans service.properties /o\
           * décision :
               * pour l'instant, 1 service = 1 logiciel et privilégier la notion de suite collaborative
               * continuer de recenser les cas problématiques
                   * Nextcloud+OnlyOffice
                   * MumbleServer+MumbleWeb
                   * Motrix (Matrix + modules…)
                   * Jabber + Visio
           * -> ajout d'un champ "software.modules" dans la section Software où on indiquerait les modules du logiciel activé


   * gestion technique de stats.chatons.org
       * gestion de /private/chatonsinfos/ via  SFTP :
           * configuration Filezilla
           * fichier chatons.properties dans /private/chatonsinfos/inputs/
           * principe du fichier /private/chatonsinfos/REFRESH


   * ONTOLOGIE
       * rappels précédentes réunion :
           * organization.guide.user : ajout dans le fichier modèle du dépôt : FAIT


       * ajout d'un fichier CHANGELOG :
           * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/CHANGELOG.md](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/CHANGELOG.md)
           * on y indique les modifications / ajouts au regard de l'ontologie de référence
           * mais on n'y indique pas les modifs ortho/typo ou la correction de bugs


       * host.description à passer en « optionnel » dans les fichiers modèles :
               * TODO Cpm fichier modèle service.properties FAIT
               * TODO Cpm fichiers modèles services-foo.properties FAIT
               * TODO Cpm dans l'outil StatoolInfos FAIT


       * propriétés socialnetworks :
           * federation.socialnetworks.* (type URL, recommandé)
           * organization.socialnetworks.* (type URL, recommandé)
           * proposition de les passer à optionnel
               * pour : Angie, Cpm, mrflos
               * contre :
               * commentaire : ça serait bien d'avoir une règle sophistiqué « en avoir au moins un » mais trop sophistiqué et trop contraignant
               * décision : OK
               * TODO Cpm propager dans federation.properties
               * TODO Cpm propager dans organization.properties
               * TODO Cpm propager dans statoolinfos
               * TODO Cpm mettre à jour le CHANGELOG


       * réseau sociaux d'une organisation
           * les suivants sont absents de la liste pré-définie et dans stats.chatons.org :
               * RocketChat et Mattermost : indiquer la "team" si celle-ci est utilisée pour de la diffusion publique d'infos (mais attention à ce que les chatons n'indiquent pas leur instance)
               * IRC : ok
           * proposition de les ajouter :
               * pour :
               * contre :
               * décision :
               * TODO ajouter dans le fichier modèle organization.properties
               * TODO Cpm ajouter dans l'outil statoolinfos
           * faut-il faire pareil pour la fédération et les services ?


       * informations de membre du collectif
           * actuellement, il y a des prorpiétés qui qui donnent des informations sur le membre en tant qu’organisation. :
               * organization.startDate
               * organization.enddate
               * organization.status.level
               * organization.status.description 
           * besoin aussi d’avoir les mêmes informations mais sur le membre en tant que membre
           * proposition d'ajouter de nouvelles propriétés dédiées :
               * # Date d'entrée dans le collectif (type DATE, obligatoire, ex. 08/11/2018). 
               * organization.member.chatons.startdate =
               * # Date de sortie du collectif (type DATE, optionnel, ex. 08/11/2019).
               * organization.member.chatons.enddate =
               * # Statut de l'organisation en tant que membre (un parmi {ACTIVE, IDLE, AWAY}, obligatoire).
               * organization.member.chatons.status.level =
               * # Description du statut de l'organisation en tant que membre (type STRING, optionnel, ex. en sommeil).
               * organization.member.chatons.status.description =
           * problème :
               * les fichiers properties forment une hiérarchie orientée (fédération -> organization -> service), ici c'est mettre une information de fédération dans un fichier d'organization.
               *  Rupture ? Le fichier organization.properties devient dépendant d'une seule fédération
               * obligation de faire un fichier organization.properties par fédération
               * on ne sait pas de quoi les champs de membre parlent : ajouter un champs organization.member.federation ? (pour indiquer à quelle fédération cette fiche organization se relie)
               * organization.members.chatons.
               * **la solution consiste à pouvoir déclarer plusieurs adhésions avec une partie variable dans le chemin de la propriété**
           * délibération proposition :
               * pour :
               * contre :
               * décision :
               * TODO ajouter dans le fichier modèle organization.properties
               * TODO Cpm ajouter dans l'outil statoolinfos


   * revue de [https://stats.chatons.org/](https://stats.chatons.org/) 😍
       * intégration d'une page édito : [https://stats.chatons.org/edito.xhtml](https://stats.chatons.org/edito.xhtml)
           * à modifier sur [https://framagit.org/chatons/chatonsinfos/-/tree/master/StatoolInfos/edito](https://framagit.org/chatons/chatonsinfos/-/tree/master/StatoolInfos/edito) (liens relatifs)
           * texte pour les internautes explicitant où on est et comment on trouve des infos
           * TODO Angie
       * ajout de la page Statistiques (fédération)
           * demande : quels autres statistiques mettre ?
           * TODO Angie : prendre un temps pour y réfléchir
           * utiliser les pourcentages plutôt que les chiffres ?
           * on s'occupera de la mise en forme une fois qu'on aura tous les types de stats
       * ajout de statistiques au niveau des organisations (dans la ligne lien, bouton vers une page dédiée)
           * affichage chronologique du nombre de services par structure
           * nombre d'utilisateurs de l'organisation
       * liste des membres manquants (75 affichés) ?
           * 76 chatons actifs + 6 chatons en sommeil -> 82 chatons
           * TODO Angie : vérifier la liste des organisations (et maj les fiches des chatons en sommeil)


   * revue de fichiers properties de membres  :
       * Framasoft,
           * startdate organization
       * Colibri
       * Chapril
       * Ajouter [https://wtf.roflcopter.fr/.well-known/chatonsinfos/roflcopter.properties](https://wtf.roflcopter.fr/.well-known/chatonsinfos/roflcopter.properties)
           * TODO Angie
       * revue des merge request
           * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
           * celle de filament 
               * TODO Cpm : FAIT
               * TODO Angie : x2
       * revue du forum
           * [https://forum.chatons.org/t/questions-sur-les-properties-avec-stats-chatons-org/](https://forum.chatons.org/t/questions-sur-les-properties-avec-stats-chatons-org/)
           * On fait quoi quand un service est construit avec plusieurs logiciels ?
               * pour l'instant, on ne gère pas dans stats.chatons.org ce type de situation
               * nom de ce type d'outil : suite collaborative






# Douzième réunion du groupe de travail

### jeudi 03 décembre à 11h

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes :  Cpm, Flo



   * divers précédents :
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * TODO Cpm
       * demande d'amélioration de la doc# sur host.name : préciser qu'il ne s'agit pas de l'hébergeur du service mais de l'hébergeur de la machine qui fait tourner le service (et préciser qu'en cas d'autohébergement, c'est le même)
           * TODO Flo ? ok
       * cas d'un service non logiciel (ljf) :
           * [https://stats.chatons.org/sansnuagearn-housing.xhtml](https://stats.chatons.org/sansnuagearn-housing.xhtml)
           * pose des questions par rapport à certains champs obligatoires dans le fichier service.properties :
               * l'absence de propriété host est catégorisée en erreur /!\/?\
               * colonne vide dans les listes de services
               * software.name, software.license.url, software.license.name
               * host.server.type
               * host.provider.type
           * comment traiter ce cas ?
               * créer un nouveau type de fiche pour traiter ces cas là ?
               * réfléchir aux champs qui y apparaîtraient : demander à ljf et les autres chatons proposant cela
               * comment intégrer cette fiche dans l'interface stats ?
           * délibération :
               * pour : Cpm, mrflos
               * contre :
               * décision : ok pour faire un nouveau fichier properties dédié aux offres non logicielles mais le faire plus tard, une fois que les service.properties seront totalement intégrés (métriques).
           * TODO Flo : créer ticket dans ChatonsInfos


   * ONTOLOGIE


       * software.modules (services.properties)
           * ajout d'un champs "modules" dans la section Software où on indiquerait les modules du logiciel activé
           * TODO Cpm : rajouter dans l'ontologie le nouveau champ + docs
           * TODO Cpm : coder et valoriser le champ dans StatoolInfos


       * propriétés socialnetworks :
           * proposition de les passer à optionnel
           * TODO Cpm propager dans federation.properties
           * TODO Cpm propager dans organization.properties
           * TODO Cpm propager dans statoolinfos
           * TODO Cpm mettre à jour le CHANGELOG


       * réseau sociaux d'une organisation
           * les suivants sont absents de la liste pré-définie et dans stats.chatons.org :
               * RocketChat et Mattermost : indiquer la "team" si celle-ci est utilisée pour de la diffusion publique d'infos (mais attention à ce que les chatons n'indiquent pas leur instance)
               * IRC : ok
           * proposition de les ajouter :
               * pour :
               * contre :
               * décision :
               * TODO ajouter dans le fichier modèle organization.properties
               * TODO Cpm ajouter dans l'outil statoolinfos
           * faut-il faire pareil pour la fédération et les services ?


       * informations de membre du collectif
           * actuellement, il y a des propriétés qui qui donnent des informations sur le membre en tant qu’organisation. :
           * besoin aussi d’avoir les mêmes informations mais sur le membre en tant que membre
           * proposition d'ajouter de nouvelles propriétés dédiées avec un élément variable (le 3e) :
               * # Nom d'une structure dont est membre l'organisation  (type STRING, obligatoire, ex. chatons). 
               * organization.member.chatons.name =
               * # Description d'une structure dont est membre l'organisation  (type STRING, optionnel, ex. chatons). 
               * organization.member.chatons.description =
               * # Date d'entrée dans le collectif (type DATE, obligatoire, ex. 08/11/2018). 
               * organization.member.chatons.startdate =
               * # Date de sortie du collectif (type DATE, optionnel, ex. 08/11/2019).
               * organization.member.chatons.enddate =
               * # Statut de l'organisation en tant que membre (un parmi {ACTIVE, IDLE, AWAY}, obligatoire).
               * organization.member.chatons.status.level =
               * # Description du statut de l'organisation en tant que membre (type STRING, optionnel, ex. en sommeil).
               * organization.member.chatons.status.description =
               * alternative de nommage :
                   * organization.memberof.chatons.startdate
                   * organization.memberships.chatons.startdate
           * délibération proposition :
               * pour : 
               * contre :
               * décision :
               * TODO ajouter dans le fichier modèle organization.properties
               * TODO Cpm ajouter dans l'outil statoolinfos
       * En l'état, pas de grosses inspiration, TODO Flo : trouver un terme qui se mette au pluriel en anglais


   * revue de [https://stats.chatons.org/](https://stats.chatons.org/) 😍
       * intégration d'une page édito : [https://stats.chatons.org/edito.xhtml](https://stats.chatons.org/edito.xhtml)
           * à modifier sur [https://framagit.org/chatons/chatonsinfos/-/tree/master/StatoolInfos/edito](https://framagit.org/chatons/chatonsinfos/-/tree/master/StatoolInfos/edito) (liens relatifs)
           * texte pour les internautes explicitant où on est et comment on trouve des infos
           * TODO Angie
       * ajout de la page Statistiques (fédération)
           * demande : quels autres statistiques mettre ? 
           * TODO Angie et Flo : prendre un temps pour y réfléchir
           * utiliser les pourcentages plutôt que les chiffres ?
           * on s'occupera de la mise en forme une fois qu'on aura tous les types de stats
       * ajout de statistiques au niveau des organisations (dans la ligne lien, bouton vers une page dédiée)
           * affichage chronologique du nombre de services par structure
           * nombre d'utilisateurs de l'organisation
       * liste des membres manquants (75 affichés) ?
           * 76 chatons actifs + 6 chatons en sommeil -> 82 chatons
           * TODO Angie : vérifier la liste des organisations (et maj les fiches des chatons en sommeil)


   * revue de fichiers properties de membres  :
       * Framasoft,
           * startdate organization
       * Colibri
       * Chapril
       * revue des merge request
           * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * revue du forum
           * [https://forum.chatons.org/t/questions-sur-les-properties-avec-stats-chatons-org/](https://forum.chatons.org/t/questions-sur-les-properties-avec-stats-chatons-org/)






# Treizième réunion du groupe de travail

### jeudi 10 décembre à 11h

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes :  Cpm, Angie, mrflos



   * divers précédents :
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * TODO Cpm
       * demande d'amélioration de la doc# sur host.name : préciser qu'il ne s'agit pas de l'hébergeur du service mais de l'hébergeur de la machine qui fait tourner le service (et préciser qu'en cas d'autohébergement, c'est le même)
           * TODO Flo FAIT \o/
       * cas d'un service non logiciel (ljf) :
           * [https://stats.chatons.org/sansnuagearn-housing.xhtml](https://stats.chatons.org/sansnuagearn-housing.xhtml)
           * décision : ok pour faire un nouveau fichier properties dédié aux offres non logicielles mais le faire plus tard, une fois que les service.properties seront totalement intégrés (métriques).
           * TODO Flo : créer ticket dans ChatonsInfos
               * Flo : FAIT [https://framagit.org/chatons/chatonsinfos/-/issues/4](https://framagit.org/chatons/chatonsinfos/-/issues/4) 
           * TODO Angie : 
               * préciser partout que service.properties est fait pour les services **logiciels (readme et modeles/services.properties)**
               * prévenir LJF par mail ou pendant la réunion mensuelle CHATONS
           * TODO Angie : poser la question en réunion mensuelle CHATONS si les services non logiciels font parties du périmètre du collectif


   * ONTOLOGIE
       * software.modules (services.properties)
           * ajout d'un champs "modules" dans la section Software où on indiquerait les modules du logiciel activé
           * TODO Cpm : rajouter dans l'ontologie le nouveau champ + docs
           * TODO Cpm : coder et valoriser le champ dans StatoolInfos 
           * TODO Angie  : créer modèles des différents cas
               * nextcloud.properties : le nextcloud de base, avec ses modules actifs
               * nextcloud-calendar.properties: le nextcloud bridé (custom?) avec un seul module d'actif (ici les agendas, comme sur framagenda.org)
           * 



       * propriétés socialnetworks :
           * proposition de les passer à optionnel
           * TODO Cpm propager dans federation.properties (c'est mis en recommandé)
           * TODO Cpm propager dans organization.properties (c'est mis en recommandé)
           * TODO Cpm propager dans statoolinfos (c'est plus en erreur)
           * TODO Cpm mettre à jour le CHANGELOG (pas fait)


       * réseau sociaux d'une organisation
           * les suivants sont absents de la liste pré-définie et dans stats.chatons.org :
               * RocketChat et Mattermost : indiquer la "team" si celle-ci est utilisée pour de la diffusion publique d'infos (mais attention à ce que les chatons n'indiquent pas leur instance)
               * IRC : ok
               * Matrix : mais quoi ? un salon en particulier ? un compte ?
               * XMPP/Jabber :
               * un salon de discussion est-il un réseau social
               * 

           * proposition d'ajouter pour les organisations
               * ~~organization.contact.chatrooms~~ ou 
               * organization.chatrooms (plusieurs url avec virgules)
                   * pour :
                   * contre : Angie
                   * décision :
               * organization.chatrooms.{xmpp / irc / rocketchat / mattermost / matrix …} 
                   * pour : Cpm, Angie, mrflos
                   * contre :
                   * décision : adopté, ATTENTION À METTRE UN BON COMMENTAIRE
               * TODO Angie : ajouter dans le fichier modèle organization.properties
               * TODO Cpm ajouter dans l'outil statoolinfos
           * faut-il faire pareil pour la fédération ?
               * utilité faible
               * on ne va pas sur stats.chatons.org pour avoir des infos sur le collectif
               * attendons d'avoir vraiment le besoin pour l'ajouter
               * décision : NON
           * faut-il faire pareil pour les services ?
               * NON, pas ressenti le besoin pour socialnetworks donc pas utile


       * informations de membre du collectif
           * actuellement, il y a des propriétés qui donnent des informations sur le membre en tant qu’organisation. :
           * besoin aussi d’avoir les mêmes informations mais sur le membre en tant que membre
           * ajouter de nouvelles propriétés dédiées avec un élément variable (le 3e) :
               * alternative de nommage :
                   * ~~organization.members.chatons.startdate~~ (ambiiguitéentre membre de et membre)
                   * organization.memberof.chatons.startdate
                   * ~~organization.memberships.chatons.startdate~~
                   * ~~organization.affiliations.chatons.startdate~~
           * délibération proposition :
               * pour : Cpm , mrflos consent
               * contre :
               * décision : adoption de « memberof », faut avancer !
               * TODO Angie ajouter dans le fichier modèle organization.properties
               * TODO Cpm ajouter dans l'outil statoolinfos
       * En l'état, pas de grosses inspiration, TODO Flo : trouver un terme qui se mette au pluriel en anglais FAIL


   * revue de [https://stats.chatons.org/](https://stats.chatons.org/) 😍
       * intégration d'une page édito : [https://stats.chatons.org/edito.xhtml](https://stats.chatons.org/edito.xhtml)
           * à modifier sur [https://framagit.org/chatons/chatonsinfos/-/tree/master/StatoolInfos/edito](https://framagit.org/chatons/chatonsinfos/-/tree/master/StatoolInfos/edito) (liens relatifs)
           * texte pour les internautes explicitant où on est et comment on trouve des infos
           * TODO Angie
       * ajout à page Statistiques (fédération)
           * demande : quels autres statistiques mettre ? 
           * TODO Angie et Flo : prendre un temps pour y réfléchir
               * ajout d'un camembert par catégories de services (47 entrées à ce jour) ? 
                   * les types de services les plus proposés / les moins proposés
               * ajout d'une courbe de progression du nombre de services ?
               * ajout d'une courbe de progression du nombre de membres de la fédération ?
               * les entrées des chatons dans le collectif (nb de chatons a l'année)
                   * metrics.members.count.2016
           * utiliser les pourcentages plutôt que les chiffres ?
               * ca dépend des metrics
               * les ajustements, la mise en forme pourra etre faite plutot à la fin
           * on s'occupera de la mise en forme une fois qu'on aura tous les types de stats
       * ajout de statistiques au niveau des organisations (dans la ligne lien, bouton vers une page dédiée)
           * affichage chronologique du nombre de services par structure
           * nombre d'utilisateurs de l'organisation
       * liste des membres manquants (76 affichés) ?
           * 76 chatons actifs + 6 chatons en sommeil -> 82 chatons
           * TODO Angie : c'est bon pour les actifs ?, vérifier la liste des organisations (et maj les fiches des chatons en sommeil)
       * liste des catégories :
           * [https://stats.chatons.org/category-autres.xhtml](https://stats.chatons.org/category-autres.xhtml)
           * actuels : 32
           * TODO Angie : enrichir les catégories :D ou demander aux chatons des modifs de leurs fichiers service.properties


   * revue de fichiers properties de membres  :
       * Framasoft,
           * startdate organization
       * Colibri
       * Chapril
       * revue des merge request
           * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * revue du forum
           * [https://forum.chatons.org/t/questions-sur-les-properties-avec-stats-chatons-org/](https://forum.chatons.org/t/questions-sur-les-properties-avec-stats-chatons-org/)


   * avancer avec le collectif sur la complétion des metrics ?
       * metrics génériques à renommer : http / visitors
       * metrics spécifiques à chaque service à penser
           * besoin de repasser dessus pour le nommage avant de propager
           * besoin de coder leur affichage pour stats.chatons.org
           * besoin de paramétrer des moulinettes pour les récupérations automatisées de moulinettes






# Quatorzième réunion du groupe de travail

### jeudi 17 décembre 2020 à 11h

*L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur *[https://wiki.april.org/w/Mumble](*https://wiki.april.org/w/Mumble*)

*Rendez-vous sur la **terrasse Est . ****[]Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.[]*



Personnes présentes :  Cpm Angie Flo



   * objectif du jour :
       * traiter les TODO en attente
       * vérifier qu'on a pas oublié de TODO des compte-rendus précédents


   * divers précédents :
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * TODO Cpm
       * demande d'amélioration de la doc# sur host.name : préciser qu'il ne s'agit pas de l'hébergeur du service mais de l'hébergeur de la machine qui fait tourner le service (et préciser qu'en cas d'autohébergement, c'est le même)
           * TODO Flo FAIT \o/
       * cas d'un service non logiciel (ljf) :
           * [https://stats.chatons.org/sansnuagearn-housing.xhtml](https://stats.chatons.org/sansnuagearn-housing.xhtml)
           * décision : ok pour faire un nouveau fichier properties dédié aux offres non logicielles mais le faire plus tard, une fois que les service.properties seront totalement intégrés (métriques).
           * TODO Flo : créer ticket dans ChatonsInfos
               * Flo : FAIT [https://framagit.org/chatons/chatonsinfos/-/issues/4](https://framagit.org/chatons/chatonsinfos/-/issues/4) 
           * TODO Angie : 
               * préciser partout que service.properties est fait pour les services **logiciels (readme et modeles/services.properties)** OK
               * prévenir LJF par mail OK
           * TODO Angie : poser la question en réunion mensuelle CHATONS si les services non logiciels font parties du périmètre du collectif


   * ONTOLOGIE
       * software.modules (services.properties)
           * ajout d'un champs "modules" dans la section Software où on indiquerait les modules du logiciel activé
           * TODO Cpm : rajouter dans l'ontologie le nouveau champ + docs      FAIT
           * TODO Cpm : coder et valoriser le champ dans StatoolInfos 
           * TODO Angie  : créer modèles des différents cas
               * nextcloud.properties : le nextcloud de base, avec ses modules actifs OK
               * nextcloud-calendar.properties: le nextcloud bridé (custom?) avec un seul module d'actif (ici les agendas, comme sur framagenda.org) OK
           * modules actifs par défault
               * Files
               * Photos
           * modules courants mis en avant pas activés
               * Big Blue Button
               * Bookmark
               * Calendar
               * Carnet
               * Collabora Online
               * Onlyoffice
               * Contacts
               * Deck
               * Forms
               * Mail
               * Maps
               * News
               * Notes
               * Polls
               * Spreed (discourse)
               * Talk
               * Task


       * propriétés socialnetworks :
           * proposition de les passer à optionnel
           * TODO Cpm propager dans federation.properties (c'est mis en recommandé)
           * TODO Cpm propager dans organization.properties (c'est mis en recommandé)
           * TODO Cpm propager dans statoolinfos (c'est plus en erreur)
           * TODO Cpm mettre à jour le CHANGELOG (pas fait)


       * réseau sociaux d'une organisation
           * les suivants sont absents de la liste pré-définie et dans stats.chatons.org :
               * RocketChat et Mattermost : indiquer la "team" si celle-ci est utilisée pour de la diffusion publique d'infos (mais attention à ce que les chatons n'indiquent pas leur instance)
               * IRC : ok
               * Matrix : mais quoi ? un salon en particulier ? un compte ?
               * XMPP/Jabber :
               * un salon de discussion est-il un réseau social
           * proposition d'ajouter pour les organisations
               * ~~organization.contact.chatrooms~~ ou 
               * ~~organization.chatrooms (plusieurs url avec virgules)~~
               * organization.chatrooms.{xmpp / irc / rocketchat / mattermost / matrix …} 
                   * pour : Cpm, Angie, mrflos
                   * contre :
                   * décision : adopté, ATTENTION À METTRE UN BON COMMENTAIRE
               * TODO Angie : ajouter dans le fichier modèle organization.properties OK
               * TODO Cpm ajouter dans l'outil statoolinfos
           * faut-il faire pareil pour la fédération ?
               * utilité faible
               * on ne va pas sur stats.chatons.org pour avoir des infos sur le collectif
               * attendons d'avoir vraiment le besoin pour l'ajouter
               * décision : NON
           * faut-il faire pareil pour les services ?
               * NON, pas ressenti le besoin pour socialnetworks donc pas utile


       * informations de membre du collectif « memberof »
           * organization.memberof.chatons.startdate
           * TODO Angie ajouter dans le fichier modèle organization.properties OK
           * TODO Cpm ajouter dans l'outil statoolinfos


   * revue de [https://stats.chatons.org/](https://stats.chatons.org/) 😍
       * intégration d'une page édito : [https://stats.chatons.org/edito.xhtml](https://stats.chatons.org/edito.xhtml)
           * à modifier sur [https://framagit.org/chatons/chatonsinfos/-/tree/master/StatoolInfos/edito](https://framagit.org/chatons/chatonsinfos/-/tree/master/StatoolInfos/edito) (liens relatifs)
           * texte pour les internautes explicitant où on est et comment on trouve des infos
           * TODO Angie fait !
       * ajout à page Statistiques (fédération)
           * demande : quels autres statistiques mettre ? 
           * TODO Angie et Flo : prendre un temps pour y réfléchir
               * ajout d'un camembert par catégories de services (47 entrées à ce jour) ? 
                   * les types de services les plus proposés / les moins proposés
               * ajout d'une courbe de progression du nombre de services ?
               * ajout d'une courbe de progression du nombre de membres de la fédération ?
               * les entrées des chatons dans le collectif (nb de chatons a l'année)
                   * metrics.members.count.2016
           * utiliser les pourcentages plutôt que les chiffres ?
               * ca dépend des metrics
               * les ajustements, la mise en forme pourra etre faite plutot à la fin
           * on s'occupera de la mise en forme une fois qu'on aura tous les types de stats
       * ajout de statistiques au niveau des organisations (dans la ligne lien, bouton vers une page dédiée)
           * affichage chronologique du nombre de services par structure
           * nombre d'utilisateurs de l'organisation
       * liste des membres manquants (76 affichés) ?
           * 76 chatons actifs + 6 chatons en sommeil -> 82 chatons
           * TODO Angie : c'est bon pour les actifs ?, vérifier la liste des organisations (et maj les fiches des chatons en sommeil)
               * sous /web/.well-known/chatonsinfos/organizations.default, j'ai ajouté les fiches de :
                   * boblecodeur (en sommeil)
                   * devosi (en sommeil)
                   * duchesse (en sommeil)
                   * enough
                   * facil
                   * gozmail (en sommeil)
                   * octodome (en sommeil)
               * j'ai supprimé la fiche pour ravion.org
               * j'ai modifié le statut sur les fiches de :
                   * Le Samarien (en sommeil)
                   * Nebulae (en sommeil)
               * et j'ai mis à jour chatons.properties avec les URL des nouveaux fichiers
       * liste des catégories :
           * [https://stats.chatons.org/category-autres.xhtml](https://stats.chatons.org/category-autres.xhtml)
           * actuels : 32
           * TODO Angie : enrichir les catégories :D ou demander aux chatons des modifs de leurs fichiers service.properties
               * nouvelles catégories : 
                   * #Générateur de flux RSS : categories.rssgen.xxx
                   * #Mesure de statistiques (alternative à Google Analytics) : categories.metrics.xxx
                   * #VPN - Réseau privé virtuel : categories.vpn.xxx
                   * #Organisation d'évènements : categories.events.xxx
                   * #Gestion de groupes : categories.groups.xxx
                   * #Générateur de QR code : categories.qrcode.xxx
               * TODO Angie : ajout des images .svg pour ces nouvelles catégories
               * pour certains outils, je ne sais pas quel nom donner à la catégorie : 
                   * Bénévalibre :
                       * suggestion : gestion association, avec Galette et Structura ([https://structura.associatif.online/)](https://structura.associatif.online/))
                   * TODO mrflos :
                       *  CyberChef : outil d'administration systeme / automatisation (icone tapis roulant de l'usine [https://thenounproject.com/search/?q=automate\&i=2362268](https://thenounproject.com/search/?q=automate\&i=2362268) )
                       * Healthchecks : outil d'administration systeme / monitoring (icone moniteur avec oscilloscope / oeil [https://thenounproject.com/search/?q=monitor\&i=165895](https://thenounproject.com/search/?q=monitor\&i=165895) )
                       * Ganeti (VPS) : outils d'administration systeme / administration de machine virtuelle (icone serveur? [https://thenounproject.com/search/?q=server\&i=1286442](https://thenounproject.com/search/?q=server\&i=1286442) )
           * autres :
               * [https://stats.chatons.org/softwares.xhtml](https://stats.chatons.org/softwares.xhtml)
                   * jitsimeet         n/a : JitsiMeet à rajouter dans la catégorie Visio-conférence
                   * whiteboard : Whiteboard (pfff le nom ;*>)




   * revue de fichiers properties de membres  :
       * Framasoft,
           * startdate organization
       * Colibri
       * Chapril
       * revue des merge request
           * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * revue du forum
           * [https://forum.chatons.org/t/questions-sur-les-properties-avec-stats-chatons-org/](https://forum.chatons.org/t/questions-sur-les-properties-avec-stats-chatons-org/)


   * avancer avec le collectif sur la complétion des metrics ?
       * metrics génériques à renommer : http / visitors
       * metrics spécifiques à chaque service à penser
           * besoin de repasser dessus pour le nommage avant de propager
           * besoin de coder leur affichage pour stats.chatons.org
           * besoin de paramétrer des moulinettes pour les récupérations automatisées de moulinettes
